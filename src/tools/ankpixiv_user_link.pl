#!/bin/perl

use strict;

use encoding 'utf8';

use Encode;
use DBI;

sub main () {

	my $from = $ARGV[0] || "ankpixiv.sqlite";

	if ( ! -f $from ) {
		my $winappfolder = $ENV{'APPDATA'} || "<USER FOLDER>\\AppData\\Roaming";
		print STDERR "Usage: $0 <FIREFOX PROFILE FOLDER>/ankpixiv.sqlite\n";
		print STDERR "\n";
		print STDERR "ex.\n";
		print STDERR "\n";
		print STDERR " WINDOWS: $0 $ENV{'APPDATA'}\\Mozilla\\Firefox\\Profiles\\<RANDOM NAME>\\ankpixiv.sqlite\n";
		print STDERR " LINUX:   $0 ~/.mozilla/firefox/<RANDOM NAME>/ankpixiv.sqlite\n";
		exit 1;
	}

	my $dbfrom = "dbi:SQLite:dbname=$from";
	my $dbhfrom = DBI->connect($dbfrom,undef,undef,{PrintError => 0,AutoCommit => 0}) || die "connect $!";

	my $sthfrom_select_members = $dbhfrom->prepare("SELECT id,pixiv_id,service_id FROM members;") || die $dbhfrom->errstr;

	if ( $sthfrom_select_members->execute() ) {
		while ( my $ref = $sthfrom_select_members->fetchrow_arrayref ) {
			my ( $id, $pixiv_id, $service_id ) = @$ref;
			
			if ( $service_id eq 'PXV' ) {
				printf("http://www.pixiv.net/member.php?id=%s\thttp://pixiv.me/%s\n", $id, $pixiv_id);
			}
			elsif ( $service_id eq 'TWT' ) {
				printf("https://twitter.com/%s\n", $pixiv_id);
			}
			elsif ( $service_id eq 'NCS' ) {
				printf("http://www.nicovideo.jp/user/%s\n", $id);
			}
			elsif ( $service_id eq 'NJE' ) {
				printf("http://nijie.info/members.php?id=%s\n", $id);
			}
			elsif ( $service_id eq 'TNM' ) {
				printf("http://www.tinami.com/creator/profile/%s\n", $id);
			}
			elsif ( $service_id eq 'TBR' ) {
				printf("http://%s.tumblr.com/\n", $id);
			}
			else {
				#print "$id, $pixiv_id, $service_id\n";
			}
		}
		$sthfrom_select_members->finish;
	}

	$dbhfrom->disconnect;

}

&main;
exit 0;
