# coding: utf-8

import os
import os.path
import sys
import sqlite3

def Main ():
    source = "ankpixiv.sqlite"
    if len(sys.argv) > 1:
        source = sys.argv[1]
    if not os.path.exists(source):
        winappfolder = os.environ.get("APPDATA")
        if not winappfolder:
            winappfolder = "<USER FOLDER>\\AppData\\Roaming"
        print >> sys.stderr, "Usage: %s <FIREFOX PROFILE FOLDER>/ankpixiv.sqlite" % (sys.argv[0])
        print >> sys.stderr, ""
        print >> sys.stderr, "ex."
        print >> sys.stderr, ""
        print >> sys.stderr, "WINDOWS: %s %s\\Mozilla\\Firefox\\Profiles\\<RANDOM NAME>\\ankpixiv.sqlite" % (sys.argv[0],winappfolder)
        print >> sys.stderr, "LINUX  : %s ~/.mozilla/firefox/<RANDOM NAME>/ankpixiv.sqlite" % (sys.argv[0])
        return 1
    
    conn = sqlite3.connect(source)
    cursor = conn.cursor()
    for row in cursor.execute("SELECT id,pixiv_id,service_id FROM members;"):
        try:
            if row[2] == "PXV":
                print "http://www.pixiv.net/member.php?id=%s\thttp://pixiv.me/%s" % (row[0],row[1])
            elif row[2] == "NCS":
                print "http://www.nicovideo.jp/user/%s" % (row[0])
            elif row[2] == "NJE":
                print "http://nijie.info/members.php?id=%s" % (row[0])
            elif row[2] == "TNM":
                print "http://www.tinami.com/creator/profile/%s" % (row[0])
            elif row[2] == "TBR":
                print "http://%s.tumblr.com/" % (row[0])
        except:
            print >> sys.stderr, row[0]
    cursor.close()

Main()