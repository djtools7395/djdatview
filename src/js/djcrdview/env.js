(function (global) {

  var Env = {
    selectedEventId: 0,
    selectedDateId: 1,
    selectedMapId: 1,
    selectedBlockId: 1,
    selectedCircleId: 0,

    selectedViewId: 'map',

    showGenreBitmap: true,
    transparentMapBitmap: true,
    cutColumnsPortrait: 6,
    cutColumnsLandscape: 12,
    detailLinkColumns: 4,
    mapFitToHeight: 1,

    useKeydown: true,

    useAnimation: true,

    circleCacheSize: 300,
    circlesInBlockCacheSize: 20,
    favCircleCacheSize: 300,
    cutImageCacheSize: 300,

    uncheckedCutOpacity: undefined
  };

  //---

  global.Env = Env;

})(DjCRDView);
