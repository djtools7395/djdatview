// 状況テーブル

(function (global) {

  var Condition = {};

  // キャッシュ
  var conditionCache = {};

  Condition.clearCache = function () {
    conditionCache = {
      data: {},
      sortedKeys: []
    };

    return Promise.resolve(conditionCache).then(function (res) {

      var query = {
        sql: 'SELECT condId,label,alarm,enabled FROM DjdatCondition WHERE djdatNo=? ORDER BY djdatNo,condId',
        get params () {
          return [global.Env.selectedEventId];
        },
        onSuccess: function (r) {
          for (var i=0; i<r.rows.length; i++) {
            var item = r.rows.item(i);
            if (isNaN(item.condId)) {
              continue;
            }

            var obj = {};
            for (var p in item) {
              obj[p] = item[p];
            }

            res.data[obj.condId] = obj;
          }

          return res;
        },
        onError: function (e) {
          return e;
        }
      };

      return SandboxDB.transactionQuery(query);

    }).then(function (res) {

      var cached = Object.keys(res.data).length;

      global.Const.Conditions.forEach(function (cond,index) {
        var condId = index+1;
        if (res.data[condId]) {
          return;
        }

        // DB上に存在しないものは既定値を使う
        var obj = {};
        for (var p in cond) {
          obj[p] = cond[p];
        }
        obj['condId'] = condId;

        res.data[condId] = obj;
      });

      res.sortedKeys = global.Utils.getSortedKeys(res.data);

      // 短縮表示
      res.sortedKeys.forEach(function (condId) {
        var item = res.data[condId];
        item.shortlabel = item.label.slice(0,2);
      });

      console.debug('CACHE condition table length='+Object.keys(res.data).length+' cached='+cached);

      return Promise.resolve(res);

    });
  };

  //---

  /**
   *
   * @param condId
   * @param enabled
   * @returns {Promise}
   */
  Condition.setConditionEnabled = function (condId, enabled) {

    var cond = conditionCache.data[condId];
    if (!cond) {
      return Promise.reject(new Error('wrong cond id condId='+condId));
    }

    cond.enabled = enabled;

    var query = {
      sql: 'INSERT OR REPLACE INTO DjdatCondition (djdatNo,condId,label,alarm,enabled) VALUES (?,?,?,?,?)',
      get params () {
        return [global.Env.selectedEventId, condId, cond.label, cond.alarm, cond.enabled];
      },
      onSuccess: function (r) {
        return r;
      },
      onError: function (e) {
        return e;
      }
    };

    return SandboxDB.transactionQuery(query);
  };

  /**
   *
   * @param condId
   * @returns {*}
   */
  Condition.findCondition = function (condId) {
    return conditionCache.data[condId];
  };

  /**
   *
   * @returns {*}
   */
  Condition.getSortedKeys = function () {
    return conditionCache.sortedKeys;
  };

  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.Condition = Condition;

})(DjCRDView);