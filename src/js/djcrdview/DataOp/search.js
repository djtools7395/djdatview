(function (global) {

  var Search = {};

  //---

  /**
   * URLリストでサークルリストを検索する
   * @param list
   * @param progress
   * @returns {*}
   */
  Search.searchCirclesByURLList = function (list,progress) {

    if (list.length == 0) {
      return Promise.reject(new Error('empty url list.'));
    }

    return SandboxDB.beginTransaction()
      .then(function (res) {
        return Search.executeFetchCirclesByURLListQuery(res.transaction,list,progress);
      });

  };

  /**
   * URLリストでサークルリストをクエリを実行する
   * @param transaction
   * @param list
   * @param onprogress
   * @returns {*}
   */
  Search.executeFetchCirclesByURLListQuery = function (transaction, list, onprogress) {

    // 一行分のURLを一個ずつ順に検索にかけてマッチしたら戻る
    function matchURLsOneByOne (t, urls) {
      return spawn(function* () {
        for (var i=0; i<urls.length; i++) {
          var url = urls[i];

          var r = yield global.DataOp.Circle.executeMatchURLQuery(t, url);
          if (r) {
            return r;
          }
        }
      });
    }

    var pStep = Math.ceil(list.length/100);

    return spawn(function* () {

      var results = null;

      for (var i=0; i<list.length; i++) {
        var urls = list[i];

        if (onprogress && (i+1) % pStep == 0) {
          onprogress({value:i+1});
        }

        // 一行に複数のURLがある場合はどれか一つがマッチしたらOK
        var r = yield matchURLsOneByOne(transaction, urls);

        // ヒットした検索結果をキャッシュにのせる
        if (r && r.result) {
          var rows = r.result.rows;
          rows.forEach(function (item) {
            results = results || {};
            if (!results[item.circleId]) {
              results[item.circleId] = {
                Circle: item,
                matched: r.result.url
              };
            }
          });
        }
      }

      return results;
    });
  };

  // TODO json出力と同時に、DBにも保存して、再起動時に再利用できるようにする

  /**
   * サークル名＆作家名リストでサークルリストを検索する
   * @param keys
   * @param progress
   * @returns {*}
   */
  Search.searchCirclesByNameList = function (keys, progress) {

    if (keys.length == 0) {
      return Promise.reject(new Error('empty name list.'));
    }

    return SandboxDB.beginTransaction()
      .then(function (res) {
        return Search.executeFetchCirclesByNameListQuery(res.transaction,keys,progress);
      });
  };

  /**
   * サークル名＆作家名リストでサークルリストをクエリを実行する
   * @param transaction
   * @param keys
   * @returns {*}
   */
  Search.executeFetchCirclesByNameListQuery = function (transaction, keys, onprogress) {

    function modKeyValue (str,min) {
      var val = str && str.trim();
      if (!val)
        return;

      var length = val.length;
      val = global.Utils.strNormalize(str);
      val = val.replace(/%/,'%%');
      if (isNaN(min) || length >= min) {
        // ３文字以上なら部分一致、２文字以下なら完全一致
        val = '%'+val+'%';
      }

      return val;
    }

    //

    var pStep = Math.ceil(keys.length/100);

    return spawn(function* () {
      var results = null;

      for (var i=0; i<keys.length; i++) {
        var key = keys[i];

        if (onprogress && (i+1) % pStep == 0) {
          onprogress({value:i+1});
        }

        // 一行分のサークル名＆作家名を一個ずつ順に検索にかけてマッチしたら戻る
        var name = modKeyValue(key.name);
        var author = modKeyValue(key.author);

        if (!name && !author) {
          continue;
        }

        var r = yield global.DataOp.Circle.executeMatchNameQuery(transaction,{name:name,author:author});

        // ヒットした検索結果をキャッシュにのせる
        if (r && r.result) {
          var rows = r.result.rows;
          rows.forEach(function (item) {
            results = results || {};
            if (!results[item.circleId]) {
              results[item.circleId] = {
                Circle: item,
                matched: r.result.key
              };
            }
          });
        }
      }

      return results;
    });
  };

  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.Search = Search;

})(DjCRDView);