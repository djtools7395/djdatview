(function (global) {

  var EnvOp = {};

  //---

  /**
   * イベント選択・削除時に選択中イベントをリセットする
   * @param eventId
   */
  EnvOp.setSelectedEvent = function (eventId) {
    EnvOp.save({
      selectedEventId: eventId,
      selectedDateId: 1,
      selectedMapId: 1,
      selectedBlockId: 1,
      selectedCircleId: 0
    });
  };

  /**
   *
   * @param json_str
   * @returns {Promise}
   */
  EnvOp.save = function (config) {

    // 書き換えて
    var modified = false;

    for (var p in config) {
      if (p in global.Env) {
        if (global.Env[p] != config[p]) {
          modified = true;
          global.Env[p] = config[p];
        }
      }
      else {
        console.error('ENV: bad key='+p);
      }
    }

    if ( ! modified) {
      // 変更ないなら保存しない
      return Promise.resolve();
    }

    var json_str = JSON.stringify(global.Env);

    var query = {
      sql: 'INSERT OR REPLACE INTO DjdatEnv (id,env) VALUES (?,?)',
      params: [0, json_str]
    };

    return SandboxDB.transactionQuery(query);
  };

  /**
   * 設定を読み込む
   */
  EnvOp.init = function () {

    function decodeEnvJson (r) {
      if (r.rows.length == 0) {
        // 初回起動かなにか
        return null;
      }

      // 関数以外だけ復元
      try {
        var obj = JSON.parse(r.rows.item(0).env);
        for (var p in obj) {
          if (p in global.Env) {
            global.Env[p] = obj[p];
          }
        }

        // 正常終了
        return global.Env;
      }
      catch (e) {
        if (e)
          console.error(e.stack || e);
      }

      // 不正
      return;
    }

    var query = {
      sql: 'SELECT * FROM DjdatEnv WHERE id=?',
      params: [0],
      onSuccess: decodeEnvJson
    };

    return SandboxDB.transactionQuery(query);
  };

  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.EnvOp = EnvOp;

})(DjCRDView);