//------------------
//
// お気に入りの追加・更新・削除
//
//------------------

// FIXME 他イベントお気に入り取り込み時のUnknownを捨てているのを直す
// FIXME getFavDetails を clearCacheに合わせる
// FIXME condId の最新の値は FavCircleテーブル にも保存しておくべき。お気に入り件数が増えると初期化コストが高い
// FIXME 読み込み直後は表示・非表示の設定が反映されてない？

(function (global) {

  var FavCircle = {};

  // キャッシュ
  var favCircleCache = {};

  /**
   * キャッシュのクリア
   */
  FavCircle.clearCache = function () {

    favCircleCache = {
      data: {},
      // FIXME 追加・削除があるのでリストにする
      sortedKeys: []
    };

    //
    function fetchFavCircle (res) {
      var query = {
        sql: 'SELECT c.rowid as rid, c.*, f.userId AS f_userId, F.memo AS f_memo, F.colorId AS f_colorId, F.urls AS f_urls, F.timestamp AS f_timestamp FROM DjdatCircle c, (SELECT * from DjdatFavCircle WHERE userId=? AND djdatNo=? ORDER BY circleId) f WHERE c.djdatNo=f.djdatNo AND c.circleId=f.circleId',
        get params() {
          return [1, global.Env.selectedEventId];
        },
        onSuccess: function (r) {
          if (r.rows.length == 0) {
            // スキップ
            console.debug('CACHE no fav circles eventId=' + global.Env.selectedEventId);
            return res.result;
          }

          for (var i = 0; i < r.rows.length; i++) {
            var item = r.rows.item(i);
            if (isNaN(item.circleId)) {
              continue;
            }

            var circle = global.DataOp.Circle.getCircleObject(item);

            var fav = {};
            for (var p in item) {
              var m = /^f_(.+)$/.exec(p);
              if (m) {
                var q = m[1];
                if (q == 'urls') {
                  fav[q] = (function () {
                    // FIXME
                    /*
                     try {
                     return JSON.parse(item[p]);
                     }
                     catch (e) {}
                     */

                    return [];
                  })();
                }
                else {
                  fav[q] = item[p];
                }
              }
            }

            res.result.data[circle.circleId] = {
              Circle: circle,
              Fav: FavCircle.genFavData(1, circle, fav),
              Cond: FavCircle.genCondData()
            };

            res.result.sortedKeys.push(circle.circleId);
          }

          tat.lap('fav entry loaded length='+r.rows.length);
          return res.result;
        }
      };

      return SandboxDB.executeQuery(res.transaction, query);
    }

    //
    function fetchFavLayoutInfo (res) {
      var query = {
        sql: 'SELECT circleId,xpos,ypos,layout,mapId FROM DjdatLayout l, (SELECT circleId,blockID,spaceNo FROM DjdatCircle WHERE djdatNo=? AND circleId IN (SELECT circleId FROM DjdatFavCircle WHERE djdatNo=?)) c WHERE l.djdatNo=? AND l.blockID=c.blockID AND l.spaceNo=c.spaceNo',
        get params() {
          return [global.Env.selectedEventId, global.Env.selectedEventId, global.Env.selectedEventId];
        },
        onSuccess: function (r) {
          if (r.rows.length == 0) {
            // スキップ
            return res.result;
          }

          for (var i = 0; i < r.rows.length; i++) {
            var item = r.rows.item(i);
            var c = res.result.data[item.circleId].Circle;
            if (c) {
              var obj = global.DataOp.Circle.getLayoutInfoObject(item);
              for (var p in obj) {
                if (p !== 'circleId') {
                  c[p] = obj[p];
                }
              }
            }
          }

          tat.lap('fav layout loaded length='+r.rows.length);
          return res.result;
        }
      };

      return SandboxDB.executeQuery(res.transaction, query);
    }

    //
    function fetchFavPubsInfo (res) {
      var query = {
        sql: 'SELECT * FROM DjdatPublication WHERE djdatNo=? AND circleId IN (SELECT circleId from DjdatFavCircle WHERE userId=? AND djdatNo=?)',
        get params() {
          return [global.Env.selectedEventId, 1, global.Env.selectedEventId];
        },
        onSuccess: function (r) {
          if (r.rows.length == 0) {
            // スキップ
            return res.result;
          }

          for (var i = 0; i < r.rows.length; i++) {
            var item = r.rows.item(i);
            var c = res.result.data[item.circleId] && res.result.data[item.circleId].Circle;
            if (c) {
              var obj = global.DataOp.Circle.getPubInfoOjbect(item);
              c.Pubs = c.Pubs || [];
              c.Pubs.push(obj);
            }
          }

          tat.lap('fav pubs loaded length='+r.rows.length);
          return res.result;
        }
      };

      return SandboxDB.executeQuery(res.transaction, query);
    }

    //
    function fetchFavConditionHistory (res) {
      var query = {
        sql: 'SELECT circleId,timestamp,condId FROM DjdatConditionHistory WHERE djdatNo=? AND circleId IN (SELECT circleId FROM DjdatFavCircle WHERE djdatNo=?)',
        get params() {
          return [global.Env.selectedEventId, global.Env.selectedEventId];
        },
        onSuccess: function (r) {
          if (r.rows.length == 0) {
            // スキップ
            return res.result;
          }

          for (var i = 0; i < r.rows.length; i++) {
            var item = r.rows.item(i);
            var c = res.result.data[item.circleId];
            if (c) {
              var obj = {};
              for (var p in item) {
                if (p !== 'circleId') {
                  obj[p] = item[p];
                }
              }
              if (!c.Cond.timestamp || c.Cond.timestamp < obj.timestamp) {
                c.Cond = obj;
              }
            }
          }

          tat.lap('fav condition loaded length='+r.rows.length);
          return res.result;
        }
      };

      return SandboxDB.executeQuery(res.transaction, query);
    }

    //

    var tat = global.Utils.TatCount('CACHE fav circles');

    return SandboxDB.beginTransaction(favCircleCache)
      .then(fetchFavCircle)
      .then(fetchFavLayoutInfo)
      .then(fetchFavPubsInfo)
      .then(fetchFavConditionHistory)
      .then(function (res) {
        tat.end();
        return res.result;
      });
  };

  /**
   * お気に入りキャッシュにエントリーを追加
   * @param fav
   */
  function appendFavCircleCache (fav) {
    favCircleCache.data[fav.Fav.circleId] = fav;
    favCircleCache.sortedKeys = global.Utils.getSortedKeys(favCircleCache.data);
  }

  /**
   * お気に入りキャッシュからエントリーを削除
   * @param fav
   */
  function deleteFavCircleCache (fav) {
    delete favCircleCache.data[fav.Fav.circleId];
    favCircleCache.sortedKeys = global.Utils.getSortedKeys(favCircleCache.data);
  }

  /**
   * お気に入りキャッシュのエントリーをリフレッシュ
   * @param circleId
   */
  FavCircle.refreshFavCircleCache = function (circleId) {
    var fav = FavCircle.findFavCircle(circleId);
    if (!fav) {
      return;
    }

    // 削除して
    delete favCircleCache.data[circleId];

    // 追加
    FavCircle.getFavCircle(circleId)
      .then(function (fav) {
        appendFavCircleCache(fav);
      });
  };

  /**
   *
   * @param circleId
   * @returns {Promise}
   */
  function getFavCircleEntry (circleId) {

    var query = {
      sql: 'SELECT * FROM DjdatFavCircle WHERE userId=? AND djdatNo=? AND circleId=?',
      get params() {
        return [1, global.Env.selectedEventId, circleId];
      },
      onSuccess: function (r) {
        if (r.rows.length == 0) {
          return null;
        }

        var item = r.rows.item(0);
        var obj = {};
        for (var p in item) {
          if (p == 'urls') {
            obj[p] = (function () {
              /*
                 try {
                 return JSON.parse(item[p]);
                 }
                 catch (e) {}
                 */

              return [];
            })();
          }
          else {
            obj[p] = item[p];
          }
        }

        return obj;
      }
    };

    return SandboxDB.transactionQuery(query);
  }

  /**
   *
   * @param res
   * @param circleId
   * @returns {*}
   */
  function getFavDetails (circleId) {
    // サークル情報＆レイアウト情報
    return global.DataOp.Circle.getCircle(circleId)
      .then(function (data) {
        if (!data) {
          return Promise.reject();
        }

        return data;
      }).then(function (data) {
        // お気に入り色
        return getFavCircleEntry(circleId)
          .then(function (fav) {
            if (fav) {
              data.Fav = fav;
            }
            return data;
          });
      }).then(function (data) {
        // 状況履歴
        return global.DataOp.FavCondition.getFavConditionHistory(circleId)
          .then(function (cond) {
            data.Cond = (function () {
              if (cond) {
                return FavCircle.genCondData(cond.condId, cond.timestamp);
              }

              return FavCircle.genCondData();
            })();

            return data;
          });
      })
      .catch(function (e) {
        if (!e) {
          // 対象が見つからなければエントリーを無効にする
          return Promise.resolve();
        }

        return Promise.reject(e);
      });
  }

  /**
   * キャッシュを検索する（見つからなければnullを返す）
   * @param circleId
   * @returns {*}
   */
  FavCircle.findFavCircle = function (circleId) {
    return favCircleCache.data[circleId];
  };

  /**
   * キャッシュを検索する（見つからなければ一時的なデータを作る）
   * @param circleId
   * @returns {*}
   */
  FavCircle.getFavCircle = function (circleId) {
    var cache = FavCircle.findFavCircle(circleId);
    if (cache) {
      return Promise.resolve(cache);
    }

    return getFavDetails(circleId)
      .then(function (data) {
        if (data) {
          data.Fav = FavCircle.genFavData(0, data.Circle, data.Fav);
        }
        return Promise.resolve(data);
      });
  };

  /**
   *
   * @param userId
   * @param circle
   * @param fav
   * @returns {{userId: *, circleId: (*|query.onSuccess.circleId|circleId|defs.tables.DjdatCircle.columns.circleId|defs.tables.DjdatCutImages.columns.circleId|defs.tables.DjdatFavCircle.columns.circleId), colorId: (*|colorId|defs.tables.DjdatFavCircle.columns.colorId|defs.tables.DjdatFavUnknown.columns.colorId|defs.tables.DjdatFavColor.columns.colorId|number), name: *, kana: (*|defs.tables.DjdatFavCircle.columns.kana|defs.tables.DjdatFavUnknown.columns.kana|kana), author: (*|author|names.author|defs.tables.DjdatFavCircle.columns.author|defs.tables.DjdatFavUnknown.columns.author|convertFavCircleEntries.fetchMatchedCircles.foo.author), urls: Array.<T>, memo: (*|defs.tables.DjdatCircle.columns.memo|defs.tables.DjdatFavCircle.columns.memo|defs.tables.DjdatFavUnknown.columns.memo|memo|string)}}
   */
  FavCircle.genFavData = function (userId, circle, fav) {
    return {
      userId: userId,
      circleId: circle.circleId,
      colorId: fav ? fav.colorId : 0,
      name: circle.name,
      kana: circle.kana,
      author: circle.author,
      urls: [circle.url, circle.circlems, circle.rss, circle.extraUrl1, circle.extraUrl2, circle.extraUrl3].filter(function (e) {
        return !!e
      }),
      memo: fav ? fav.memo : ''
    };
  };

  /**
   *
   * @param condId
   * @param timestamp
   * @returns {{condId: *, timestamp: *}}
   */
  FavCircle.genCondData = function (condId, timestamp) {
    return {
      condId: condId ? condId : 1, // 状況情報が存在しない場合は「未」とする
      timestamp: timestamp ? timestamp : 0
    };
  };

    /**
   *
   * @returns {*}
   */
  FavCircle.getSortedKeys = function () {
    return favCircleCache.sortedKeys;
  };

  //---

  /**
   * お気に入りの削除
   * @param fav
   */
  function removeFavCircle (fav) {

    return Promise.resolve().then(function () {
      var query = {
        sql: 'DELETE FROM DjdatFavCircle WHERE userId=? AND djdatNo=? AND circleId=?',
        get params () {
          return [1, global.Env.selectedEventId, fav.Fav.circleId];
        },
        onSuccess: function (r) {
          return fav;
        },
        onError: function (e) {
          return e;
        }
      };

      // お気に入り情報を更新
      fav.Fav.colorId = -1;

      // キャッシュから削除
      deleteFavCircleCache(fav);

      console.debug('fav removed',fav.Fav.circleId);

      return SandboxDB.transactionQuery(query);
    });
  }

  /**
   * お気に入りの更新
   * @param fav
   * @param colorId
   */
  function updateFavCircle (fav, colorId) {

    var circleId = fav.Fav.circleId;

    return Promise.resolve().then(function () {

      var query = {
        sql: 'UPDATE DjdatFavCircle SET colorId=? WHERE userId=? AND djdatNo=? AND circleId=?',
        get params () {
          return [colorId, 1, global.Env.selectedEventId, fav.Fav.circleId];
        },
        onSuccess: function (r) {
          return fav;
        },
        onError: function (e) {
          return e;
        }
      };

      // お気に入り情報を更新
      fav.Fav.colorId = colorId;

      console.debug('fav updated',fav.Fav.circleId,fav.Fav.colorId);

      return SandboxDB.transactionQuery(query);
    });
  }

  /**
   * お気に入りの追加
   * @param circleId
   * @param colorId
   */
  function addFavCircle (circleId, colorId) {

    return FavCircle.getFavCircle(circleId).then(function (fav) {
      // お気に入り情報を追加する
      var query = {
        sql: 'INSERT INTO DjdatFavCircle (djdatNo,userId,circleId,colorId,name,kana,author,urls,memo) VALUES (?,?,?,?,?,?,?,?,?)',
        get params () {
          return [global.Env.selectedEventId, 1, fav.Fav.circleId, fav.Fav.colorId, fav.Circle.name, fav.Circle.kana, fav.Circle.author, fav.Fav.urls, fav.Fav.memo];
        },
        onSuccess: function (r) {
          return fav;
        },
        onError: function (e) {
          return e;
        }
      };

      // お気に入り情報を作成
      fav.Fav.colorId = colorId;
      fav.Fav.memo = '';
      // FIXME urlsの上手な使い方
      fav.Fav.urls = [];//JSON.stringify(fav.Fav.urls);

      // キャッシュに追加
      appendFavCircleCache(fav);

      console.debug('fav added',fav.Fav.circleId,fav.Fav.colorId);

      return SandboxDB.transactionQuery(query);
    });
  }

  //---

  /**
   * お気に入りの追加・更新・削除
   * @param colorId
   */
  FavCircle.updateColor = function (colorId) {

    var circleId = global.Env.selectedCircleId;
    if (!circleId) {
      console.error(new Error('circle not selected'));
      return;
    }

    if (colorId != -1 && colorId != 0 && ! global.DataOp.FavColor.findFavColor(colorId) ) {
      console.error(new Error('invalid color id: '+circleId+' , '+colorId));
      return;
    }

    var fav = FavCircle.findFavCircle(circleId);

    Promise.resolve().then(function () {
      if (colorId == -1) {
        if (fav) {
          return removeFavCircle(fav);
        }
      }
      else if (colorId == 0) {
        if (fav) {
          return updateFavCircle(fav, colorId);
        }
      }
      else {
        if (fav) {
          return updateFavCircle(fav, colorId);
        }
        else {
          return addFavCircle(circleId, colorId);
        }
      }

      // 無効な操作（お気に入りにないのに削除実行とか）
      return Promise.reject();

    }).then(function (fav) {

      global.GuiParts.updateFavItem(fav);

    }).catch(function (e) {
      if (e)
        console.error(e.stack || e);
    });
  };

  /**
   *
   * @param condId
   */
  FavCircle.updateCondition = function (condId) {

    var circleId = global.Env.selectedCircleId;
    if (!circleId) {
      console.error(new Error('circle not selected'));
      return;
    }

    // TODO お気に入りに入っていなくても状況更新ができるようにする
    var fav = global.DataOp.FavCircle.findFavCircle(circleId);
    if (!fav) {
      console.log('not in favlist: '+circleId);
      return;
    }

    global.DataOp.FavCondition.setFavCondition(circleId, condId)
      .then(function (obj) {
        fav.Cond.condId = obj ? obj.condId : 1;
      })
      .then(function () {
        global.GuiParts.updateFavItem(fav);
      })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      });
  };

  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.FavCircle = FavCircle;

})(DjCRDView);