(function (global) {

  var URLList = {};

  //---

  URLList.load = function (file) {

    return Promise.resolve().then(function () {
      // お気に入りの読み込み
      return new Promise(function (resolve, reject) {

        var list = [];

        var config = {
          delimiter: "\t",
          newline: "",
          header: false,
          dynamicTyping: false,
          preview: 0,
          encoding: "",
          worker: false,
          comments: false,
          step: function (result) {
            result.data.forEach(function (data) {
              if (data.some(function (v) { ! /^https?:\/\//.test(v)})) {
                return;
              }

              list.push(data);
            });
          },
          complete: function () {
            resolve(list);
          },
          error: function (e) {
            reject(e);
          },
          download: false,
          skipEmptyLines: true,
          chunk: undefined,
          fastMode: false
        };

        Papa.parse(file, config);
      });
    });
  };

  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.URLList = URLList;

})(DjCRDView);