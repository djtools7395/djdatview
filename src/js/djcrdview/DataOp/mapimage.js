(function (global) {

  var MapImage = {};

  // キャッシュ
  var mapImageCache = {};

  /**
   * キャッシュのクリア
   */
  MapImage.clearCache = function () {

    mapImageCache = {};

    // マップ画像は全部読み出し（標準解像度のみ）
    var query = {
      sql: 'SELECT mapId,dateId,type,image FROM DjdatMapImages WHERE djdatNo=? AND type NOT LIKE ?',
      get params () {
        return [global.Env.selectedEventId, 'H%'];
      },
      onSuccess: function (r) {
        if (r.rows.length == 0) {
          // スキップ
          console.debug('CACHE no map images eventId='+global.Env.selectedEventId);
          return null;
        }

        for (var i=0; i<r.rows.length; i++) {
          var item = r.rows.item(i);

          var obj = {};
          for (var p in item) {
            obj[p] = item[p];
          }

          var mapCacheId = getIndex(obj.type,obj.dateId,obj.mapId);

          console.debug('CACHE map bitmap ' + mapCacheId + ' , length=' + obj.image.length);

          mapImageCache[mapCacheId] = obj;
        }

        return mapImageCache;
      },
      onError: function (e) {
        return e;
      }
    };

    //
    function addMapImagesToCache (cache) {
      if (!cache) {
        return Promise.resolve(cache);
      }

      return spawn(function* () {
        var keys = Object.keys(cache);
        for (var i=0; i<keys.length; i++) {
          var cacheId = keys[i];

          var imageURL = cache[cacheId].image;
          if (/^data:/.test(imageURL)) {
            // (旧)dataURL形式でDBに登録している場合
            // no proc.
          }
          else {
            // ローカルファイルに保存している場合
            imageURL = yield SandboxFS.openFileEntry(imageURL)
              .then(function (fileEntry) {
                return fileEntry.toURL();
              });
          }

          cache[cacheId].image = yield global.GuiParts.getImageBuffer(imageURL);
        }
      });
    }

    return SandboxDB.transactionQuery(query).then(addMapImagesToCache);
  };

  //---

  /**
   * キャッシュのインデックスを求める
   * @param type
   * @param dateId
   * @param mapId
   * @returns {string}
   */
  function getIndex (type,dateId,mapId) {
    return [type,dateId,mapId].join("/");
  }

  /**
   * キャッシュ検索
   * @param type
   * @param dateId
   * @param mapId
   * @returns {*}
   */
  MapImage.findMapImage = function (type,dateId,mapId) {
    return mapImageCache[getIndex(type,dateId,mapId)];
  };

  // イベント追加

  /**
   * イベントのマップ画像情報の追加
   * @param info
   * @param images
   * @param onprogress
   * @returns {*}
   */
  MapImage.storeMapImages = function (info, images, onprogress) {

    return SandboxDB.beginTransaction().then(function (res) {
      return spawn(function* () {
        for (var i=0; i<images.length; i++) {
          var image = images[i];

          var baseName = (function () {
            for (var p in info.mapIdMapForBaseName) {
              if (image.file.basename.slice(image.file.basename.length-p.length) == p) {
                return p;
              }
            }
          })();

          if (!baseName) {
            return Promise.resolve();
          }

          var m = /^(.+?)(\d)$/.exec(image.file.basename.slice(0,image.file.basename.length-baseName.length));
          if (!m) {
            return Promise.resolve();
          }

          var bitmapType = m[1];
          var dateId = m[2];

          var query = {
            sql: 'INSERT OR REPLACE INTO DjdatMapImages (djdatNo,dateId,mapId,type,image) VALUES (?,?,?,?,?)',
            get params () {
              return [
                info.Djdat.id,
                dateId,
                info.mapIdMapForBaseName[baseName],
                bitmapType,
                image.file.path
              ];
            },
            onSuccess: function (r) {
              if (onprogress) {
                onprogress(i+1);
              }
              return r;
            }
          };

          yield SandboxDB.executeQuery(res.transaction, query);
        }
      });
    });
  };

  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.MapImage = MapImage;

})(DjCRDView);