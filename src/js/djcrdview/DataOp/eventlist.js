
// FIXME selectedEventInfoみたいなのが必要

(function (global) {

  var EventList = {
    get Selected () {
      return selectedEvent;
    }
  };

  const CLEAR_TABLE_TARGETS = [
    'DjdatDate',
    'DjdatGenre',
    'DjdatMap',
    'DjdatArea',
    'DjdatBlock',
    'DjdatLayout',
    'DjdatCircle',
    'DjdatMapImages',
    'DjdatCutImages',
    'DjdatEtcImages',
    'DjdatInfo'
  ];

  //
  var selectedEvent;

  // キャッシュ
  var eventCache = {};

  /**
   * キャッシュのリセット
   * @returns {*}
   */
  EventList.clearCache = function () {

    eventCache = {
      data: {},
      sortedKeys: []
    };

    // イベント情報
    function getEventAll (res) {

      var query = {
        sql: 'SELECT * FROM DjdatInfo',
        get params () {
          return [];
        },
        onSuccess: function (r) {
          if (r.rows.length == 0) {
            console.debug('CACHE no event table');
          }

          // 入れ物
          res.result.data = {};

          for (var i=0; i<r.rows.length; i++) {
            var item = r.rows.item(i);
            var obj = {};
            for (var p in item) {
              obj[p] = item[p];
            }

            res.result.data[obj.djdatNo] = {};
            res.result.data[obj.djdatNo].Event = obj;
          }

          console.debug('CACHE Event length='+Object.keys(res.result.data).length);

          return res.result;
        },
        onError: function (e) {
          return e;
        }
      };

      return SandboxDB.executeQuery(res.transaction, query);
    }

    // 開催日情報
    function getEventDateAll (res) {
      return spawn(function* () {
        var keys = Object.keys(res.result.data);
        for (var i=0; i<keys.length; i++) {
          var eventId = keys[i];

          var query = {
            sql: 'SELECT dateId,year,month,day,weekday FROM DjdatDate WHERE djdatNo=? ORDER BY dateId',
            params: [eventId],
            onSuccess: function (r) {
              function onRow (obj) {
                obj['ymd'] = [global.Utils.zeroPad(obj.year,4),global.Utils.zeroPad(obj.month,2),global.Utils.zeroPad(obj.day,2)].join('/');
              }

              res.result.data[eventId].Date = {};
              copyObjectMembers('Date','dateId',res.result.data[eventId],r.rows,onRow);

              return res.result;
            },
            onError: function (e) {
              return e;
            }
          };

          yield SandboxDB.executeQuery(res.transaction, query);
        }

        return res;
      });
    }

    //

    return SandboxDB.beginTransaction(eventCache)
      .then(getEventAll)
      .then(getEventDateAll)
      .then(function (res) {
        sortEventListByDate(res.result);
        return Promise.resolve(res);
      });
  };

  /**
   * キャッシュからイベント情報を削除
   * @param djdatNo
   */
  function removeEventFromCache (djdatNo) {
    delete eventCache.data[djdatNo];
    sortEventListByDate(eventCache);
  }

  /**
   * キャッシュのキーを開催日順に並べる
   * @param res
   */
  function sortEventListByDate (cache) {

    function getIndex (eventId) {
      var ymd = cache.data[eventId].Date && cache.data[eventId].Date[1] && cache.data[eventId].Date[1].ymd || '19700101';
      return [ymd, eventId].join(',');
    }

    function getEventId (index) {
      return index.split(/,/)[1];
    }

    // 開催日付降順に並べる
    cache.sortedKeys = (function () {
      var keys = [];
      Object.keys(cache.data).forEach(function (eventId) {
        keys.push(getIndex(eventId));
      });

      return keys.sort().map(function (index) { return getEventId(index) }).reverse();
    })();
  }

  /**
   * DBから取得した値をオブジェクトのメンバーとしてコピーする等
   * @param key
   * @param id
   * @param result
   * @param rows
   * @param onRow
   * @returns {*}
   */
  function copyObjectMembers (key, id, result, rows, onRow) {
    for (var i=0; i<rows.length; i++) {
      var item = rows.item(i);
      var obj = {};
      for (var p in item) {
        obj[p] = item[p];
      }

      if (onRow) {
        onRow(obj);
      }

      result[key][obj[id]] = obj;
    }

    result[key].sortedKeys = global.Utils.getSortedKeys(result[key]);
    console.debug('CACHE '+key+' length='+result[key].sortedKeys.length);
    return result;
  }



  //---

  var getBlockListId = function (dateId, blockId) {
    return [dateId, blockId].join("-");
  };

  var getNextBlockList = function (dateId, blockId, toNext) {
    var id = getBlockListId(dateId, blockId);
    var index = this._map[id];
    index = index && index.index || 0;
    if (toNext) {
      if (++index >= this.list.length) {
        index = 0;
      }
    }
    else {
      if (--index < 0) {
        index = this.list.length-1;
      }
    }

    return this.list[index];
  };

  var getBlockList = function (dateId, blockId) {
    return this._map[getBlockListId(dateId, blockId)];
  };

  /**
   * 選択中のイベントを変更する
   * @param eventId
   * @returns {*}
   */
  EventList.setSelectedEvent = function (eventId) {

    //
    function getEventCircleRange (res) {
      var query = {
        sql: 'SELECT MIN(rowid) as min,MAX(rowid) as max FROM DjdatCircle WHERE djdatNo=?',
        params: [eventId],
        onSuccess: function (r) {
          var item = r.rows.item(0);
          var obj = {
            first: item.min,
            last: item.max,
            count: item.max - item.min + 1
          };

          res.result.Event.CircleRange = obj;

          console.debug('CACHE Circle Range first='+obj.first+' , last='+obj.last);
          return res.result;
        }
      };

      return SandboxDB.executeQuery(res.transaction, query);
    }

    //
    function getEventMap (res) {
      var query = {
        sql: 'SELECT * FROM DjdatMap WHERE djdatNo=?',
        params: [eventId],
        onSuccess: function (r) {
          return copyObjectMembers('Map','mapId',res.result,r.rows);
        }
      };

      return SandboxDB.executeQuery(res.transaction, query);
    }

    //
    function getEventArea (res) {
      var query = {
        sql: 'SELECT * FROM DjdatArea WHERE djdatNo=?',
        params: [eventId],
        onSuccess: function (r) {
          return copyObjectMembers('Area','areaId',res.result,r.rows);
        }
      };

      return SandboxDB.executeQuery(res.transaction, query);
    }

    //
    function getEventBlock (res) {
      var query = {
        sql: 'SELECT * FROM DjdatBlock WHERE djdatNo=?',
        params: [eventId],
        onSuccess: function (r) {
          function onRow (obj) {
            obj.mapId = res.result.Area[obj.areaId].mapId;
            obj.isInMap = function (mapId) {
              return mapId == this.mapId;
            }
          }

          return copyObjectMembers('Block','blockId',res.result,r.rows,onRow);
        }
      };

      return SandboxDB.executeQuery(res.transaction, query);
    }

    //
    function getEventBlockList (res) {
      var query = {
        sql: 'SELECT day as dateId,blockId,count(circleId) as count FROM DjdatCircle WHERE djdatNo=? GROUP BY day,blockId ORDER BY day,blockId',
        params: [eventId],
        onSuccess: function (r) {
          res.result.BlockList.getNext = getNextBlockList;
          res.result.BlockList.getBlockList = getBlockList;
          res.result.BlockList.list = [];
          res.result.BlockList._map = {};
          for (var i=0; i<r.rows.length; i++) {
            var item = r.rows.item(i);
            if (item.count > 0) {
              var obj = {
                dateId: item.dateId,
                blockId: item.blockId,
                count: item.count,
                index: res.result.BlockList.list.length
              };

              res.result.BlockList.list.push(obj);
              res.result.BlockList._map[getBlockListId(obj.dateId, obj.blockId)] = obj;
            }
          }

          console.debug('CACHE BlockList length='+res.result.BlockList.list.length);
          return res.result;
        }
      };

      return SandboxDB.executeQuery(res.transaction, query);
    }

    //
    function getEventGenre (res) {
      var query = {
        sql: 'SELECT * FROM DjdatGenre WHERE djdatNo=?',
        params: [eventId],
        onSuccess: function (r) {
          return copyObjectMembers('Genre','genreId',res.result,r.rows);
        }
      };

      return SandboxDB.executeQuery(res.transaction, query);
    }

    function openCutCCZ (res) {
      var path = global.DataOp.RomList.getCutCCZPath(eventId);
      return SandboxFS.openFile(path)
        .then(function (file) {
          console.debug('OPENED CutCCZ file='+file.name);
          selectedEvent.CutCCZ = file;
        })
        .catch(function (e) {
          if (e.code != 1) {
            return Promise.reject(e);
          }

          return Promise.resolve(res);
        });
    }

    //

    // 選択中イベントキャッシュのクリア
    selectedEvent = {
      Event: {},
      Date: {},
      Map: {},
      Area: {},
      Block: {},
      BlockList: {},
      Genre: {},
      CutCCZ: null
    };

    // Event,Date はキャッシュから引き継ぎ
    var event = EventList.findEvent(eventId);
    if (!event) {
      return Promise.resolve();
    }

    ['Event','Date'].forEach(function (p) {
      selectedEvent[p] = event[p];
    });

    // Map,Area,Block,Genre はDBから
    return SandboxDB.beginTransaction(selectedEvent)
      .then(getEventCircleRange)
      .then(getEventMap)
      .then(getEventArea)
      .then(getEventBlock)
      .then(getEventBlockList)
      .then(getEventGenre)
      .then(openCutCCZ);
  };

  /**
   * イベントを検索する
   * @param eventId
   * @returns {*}
   */
  EventList.findEvent = function (eventId) {
    return eventCache.data[eventId];
  };

  /**
   * キャッシュのキーの配列を取得する
   * @returns {*}
   */
  EventList.getSortedKeys = function () {
    return eventCache.sortedKeys;
  };

  // 削除

  /**
   * イベント情報の削除（カット画像は残せる）
   * @param eventId
   * @param dontOverwriteCutImage
   * @param onprogress
   * @returns {*}
   */
  EventList.removeEvent = function (eventId, dontOverwriteCutImage, onprogress) {

    return SandboxDB.beginTransaction().then(function (res) {
      return spawn(function* () {
        var tables = CLEAR_TABLE_TARGETS.filter(function (n) {
          return !dontOverwriteCutImage && (n == 'DjdatCutImages' || n == 'DjdatEtcImages');
        });

        for (var i=0; i<tables.length; i++) {
          var table = tables[i];

          var query = {
            sql: 'DELETE from '+table+' WHERE djdatNo=?',
            get params () {
              return [eventId];
            },
            onSuccess: function (r) {
              if (onprogress) {
                onprogress(i+1);
              }

              return r;
            },
            onError: function (e) {
              return e;
            }
          };

          console.debug('REMOVED: table eventId='+eventId+' table='+table);
          yield SandboxDB.executeQuery(res.transaction, query);
        }

        console.debug('CLEARED: table cache eventId='+eventId);
        removeEventFromCache(eventId);
      });
    });
  };

  // イベント追加

  /**
   * DBにイベント情報を追加する
   * @param info
   * @param successCallback
   * @param errorCallback
   */
  EventList.storeEvent = function (info) {

    //
    function insertDjdatInfo (res) {
      var query = {
        sql: 'INSERT OR REPLACE INTO DjdatInfo (djdatNo,djdatName,cutSizeW,cutSizeH,cutOriginX,cutOriginY,cutOffsetX,cutOffsetY,mapSizeW,mapSizeH,mapOriginX,mapOriginY) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)',
        get params () {
          return [
            info.Djdat.id,
            info.Djdat.name,
            info.cutInfo.width,
            info.cutInfo.height,
            info.cutInfo.topX,
            info.cutInfo.topY,
            info.cutInfo.offsetX,
            info.cutInfo.offsetX,
            info.mapTableInfo.width,
            info.mapTableInfo.height,
            info.mapTableInfo.topX,
            info.mapTableInfo.topY
          ];
        },
        onSuccess: function (r) {
          return r;
        },
        onError: function (e) {
          return e;
        }
      };

      return SandboxDB.executeQuery(res.transaction, query);
    }

    //
    function insertDjdatDate (res) {
      return spawn(function* () {
        for (var i=0; i<info.DjdatDate.length; i++) {
          var date = info.DjdatDate[i];

          var dateId = i + 1;
          dateIdMapForWday[date.wday] = dateId;

          var query = {
            sql: 'INSERT OR REPLACE INTO DjdatDate (djdatNo,dateId,year,month,day,weekday) VALUES (?,?,?,?,?,?)',
            get params() {
              return [
                info.Djdat.id,
                dateId,
                date.year,
                date.month,
                date.day,
                global.Utils.strToWday(date.wday) + 1
              ];
            },
            onSuccess: function (r) {
              return r;
            },
            onError: function (e) {
              return e;
            }
          };

          yield SandboxDB.executeQuery(res.transaction, query);
        }

        return res;
      });
    }

    //
    function insertDjdatMap (res) {
      return spawn(function* () {
        for (var i=0; i<info.DjdatMap.length; i++) {
          var cmap = info.DjdatMap[i];

          var mapId = i+1;
          mapIdMapForMapName[cmap.name] = mapId;
          mapIdMapForBaseName[cmap.baseName] = mapId;

          var query = {
            sql: 'INSERT OR REPLACE INTO DjdatMap (djdatNo,mapId,name,filename,x,y,w,h,allFilename,x2,y2,w2,h2,rotate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            get params() {
              return [
                info.Djdat.id,
                mapId,
                cmap.name,
                cmap.baseName,
                cmap.x,
                cmap.y,
                cmap.width,
                cmap.height,
                cmap.outlineBaseName,
                cmap.highX,
                cmap.highY,
                cmap.highWidth,
                cmap.highHeight,
                cmap.rotate
              ];
            },
            onSuccess: function (r) {
              return r;
            },
            onError: function (e) {
              return e;
            }
          };

          yield SandboxDB.executeQuery(res.transaction, query);
        }

        return res;
      });
    }

    //
    function insertDjdatArea (res) {
      return spawn(function* () {
        for (var i = 0; i < info.DjdatArea.length; i++) {
          var carea = info.DjdatArea[i];

          var areaId = i + 1;
          carea.mapId = mapIdMapForMapName[carea.mapName] || 1;

          carea.blockNames.split('').forEach(function (b, i) {

            var blockId = Object.keys(blockIdMap).length + 1;

            blockIdMap[blockId] = {
              name: b,
              areaId: areaId
            };

            blockIdMapForBlockName[b] = blockId;

            mapIdMapForBlockName[b] = carea.mapId;
          });

          var query = {
            sql: 'INSERT OR REPLACE INTO DjdatArea (djdatNo,areaId,name,simpleName,mapId,x,y,w,h,allFilename,x2,y2,w2,h2) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            get params() {
              return [
                info.Djdat.id,
                areaId,
                carea.areaName,
                carea.mapName,
                carea.mapId,
                carea.x,
                carea.y,
                carea.width,
                carea.height,
                carea.outlineBaseName,
                carea.highX,
                carea.highY,
                carea.highWidth,
                carea.highHeight
              ];
            },
            onSuccess: function (r) {
              return r;
            },
            onError: function (e) {
              return e;
            }
          };

          yield SandboxDB.executeQuery(res.transaction, query);
        }

        return res;
      });
    }

    //
    function insertDjdatBlock (res) {
      return spawn(function* () {
        var keys = Object.keys(blockIdMap).sort();
        for (var i = 0; i < keys.length; i++) {
          var blockId = keys[i];
          var cblock = blockIdMap[blockId];

          var query = {
            sql: 'INSERT OR REPLACE INTO DjdatBlock (djdatNo,blockId,name,areaId) VALUES (?,?,?,?)',
            get params() {
              return [
                info.Djdat.id,
                blockId,
                cblock.name,
                cblock.areaId
              ];
            },
            onSuccess: function (r) {
              return r;
            },
            onError: function (e) {
              return e;
            }
          };

          yield SandboxDB.executeQuery(res.transaction, query);
        }

        return res;
      });
    }

    //
    function insertDjdatGenre (res) {
      return spawn(function* () {
        for (var i = 0; i < info.DjdatGenre.length; i++) {
          var cgenre = info.DjdatGenre[i];

          var genreId = i+1;
          genreIdMapForCode[cgenre.code] = genreId;

          var query = {
            sql: 'INSERT OR REPLACE INTO DjdatGenre (djdatNo,genreId,name,code) VALUES (?,?,?,?)',
            get params() {
              return [
                info.Djdat.id,
                genreId,
                cgenre.name,
                cgenre.code
              ];
            },
            onSuccess: function (r) {
              return r;
            },
            onError: function (e) {
              return e;
            }
          };

          yield SandboxDB.executeQuery(res.transaction, query);
        }

        return res;
      });
    }

    //
    function insertDjdatLayout (res) {
      return spawn(function* () {
        for (var i = 0; i < info.MapLayout.length; i++) {
          var layout = info.MapLayout[i];

          var query = {
            sql: 'INSERT OR REPLACE INTO DjdatLayout (djdatNo,blockId,spaceNo,xpos,ypos,layout,mapId) VALUES (?,?,?,?,?,?,?)',
            get params() {
              return [
                info.Djdat.id,
                blockIdMapForBlockName[layout.block],
                layout.space,
                layout.x,
                layout.y,
                layout.layout,
                mapIdMapForBlockName[layout.block]
              ];
            },
            onSuccess: function (r) {
              return r;
            },
            onError: function (e) {
              return e;
            }
          };

          yield SandboxDB.executeQuery(res.transaction, query);
        }

        return res;
      });
    }

    //
    function insertDjdatCircle (res) {
      return spawn(function* () {
        for (var i=0; i<info.CircleList.length; i++) {
          var clist = info.CircleList[i];

          var blockId = blockIdMapForBlockName[clist.block];
          if (!blockId) {
            // FIXME 抽選漏れへの対応は保留
            continue;
          }

          noabMap[blockId] = noabMap[blockId] || (clist.spaceNo % 2 == 0);

          var query = {
            sql: 'INSERT OR REPLACE INTO DjdatCircle (djdatNo,circleId,pageNo,cutIndex,day,blockId,spaceNo,spaceNoSub,genreId,circleName,circleNameNrm,circleKana,penName,penNameNrm,bookName,url,mailAddr,description,memo,updateId,updateData,circlems,rss,updateFlag) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            get params() {
              return [
                info.Djdat.id,
                clist.id,
                clist.pageNo,
                clist.cutIndex,
                dateIdMapForWday[clist.wday],
                blockId,
                clist.spaceNo,
                clist.spaceNoSub,
                genreIdMapForCode[clist.genreCode],
                clist.name,
                global.Utils.strNormalize(clist.name),
                clist.kana,
                clist.author,
                global.Utils.strNormalize(clist.author),
                clist.book,
                clist.url,
                clist.mail,
                clist.desc,
                null,
                null,
                clist.updateData,
                clist.circlems,
                clist.rss,
                null
              ];
            },
            onSuccess: function (r) {
              return r;
            },
            onError: function (e) {
              return e;
            }
          };

          yield SandboxDB.executeQuery(res.transaction, query);
        }

        return res;
      });
    }

    //
    function updateDjdatBlock (res) {
      return spawn(function* () {
        var keys = Object.keys(noabMap).sort();
        for (var i=0; i<keys.length; i++) {
          var blockId = noabMap[keys[i]];

          var query = {
            sql: 'UPDATE DjdatBlock SET noab=? WHERE djdatNo=? AND blockId=?',
            get params() {
              return [
                noabMap[blockId] ? 0 : 1,
                info.Djdat.id,
                blockId
              ];
            },
            onSuccess: function (r) {
              return r;
            },
            onError: function (e) {
              return e;
            }
          };

          yield SandboxDB.executeQuery(res.transaction, query);
        }

        return res;
      });
    }

    //

    var dateIdMapForWday = {};
    var mapIdMapForMapName = {};
    var mapIdMapForBaseName = {};
    var mapIdMapForBlockName = {};
    var blockIdMap = {};
    var blockIdMapForBlockName = {};
    var genreIdMapForCode = {};
    var noabMap = {};

    return Promise.resolve()
      .then(function () {
        // 既存のイベント情報をDBから削除
        return EventList.removeEvent(info.Djdat.id, info.dontOverwriteCutImage);
      })
      .then(function () {
        // トランザクション開始
        return SandboxDB.beginTransaction()
          .then(insertDjdatInfo)
          .then(insertDjdatDate)
          .then(insertDjdatMap)
          .then(insertDjdatArea)
          .then(insertDjdatBlock)
          .then(insertDjdatGenre)
          .then(insertDjdatLayout)
          .then(insertDjdatCircle)
          .then(updateDjdatBlock);
      })
      .then(function () {
        // 配置図画像の登録に必要なマップID情報
        info.mapIdMapForBaseName = mapIdMapForBaseName;
        // イベント情報キャッシュのリセット
        global.DataOp.EventList.clearCache();
      });
  };

  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.EventList = EventList;

})(DjCRDView);