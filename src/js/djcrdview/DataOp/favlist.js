// FIXME UnknownエントリーのDBへの保存

(function (global) {

  var FavList = {
    MODE_MERGE: 'merge',
    MODE_OVERWRITE: 'overwrite',

    MATCH_EQUAL: 'equal',
    MATCH_LIKE: 'like'
  };

  //---

  const ENCODING_SHIFT_JIS = 'Shift_JIS';
  const ENCODING_JIS = 'ISO-2022-JP';
  const ENCODING_EUC_JP = 'EUC-JP';
  const ENCODING_UTF_8 = 'UTF-8';

  const ENCODINGS = [
    ENCODING_SHIFT_JIS,
    ENCODING_JIS,
    ENCODING_EUC_JP,
    ENCODING_UTF_8
  ];

  const FAV_CONFIG = {
    sign: 'ComicMarketCD-ROMCatalog',
    app: 'Windows 1.84.1',
    encoding: ENCODING_UTF_8,
    get newline () {
      return this.encoding == ENCODING_SHIFT_JIS || this.encoding == ENCODING_JIS ? "\r\n" : "\n";
    },
    get event () {
     return  'ComicMarket'+global.Env.selectedEventId;
    }
  };

  //---

  /*
   * 読み込み
   */

  /**
   * お気に入りファイルを読み込む
   * @param file
   */
  FavList.loadFavList = function (file, config) {

    // 進捗状況表示を進める
    function progress (obj) {
      if (config.progress) {
        config.progress(++progressValue);
      }
      return Promise.resolve(obj);
    }

    // 既存のイベントなら上書き確認する
    function confirmOverwrite (favlist) {
      return config.confirmOverwrite(favlist.eventId)
        .then(function (obj) {
          favlist.eventId = obj.eventId;
          favlist.name = obj.name;
          favlist.match = obj.match;
          favlist.mode = obj.mode;

          return Promise.resolve(favlist);
        })
        .then(function () {
          if (favlist.match != FavList.MATCH_LIKE) {
            return Promise.resolve(favlist);
          }

          return convertFavCircleEntries(favlist);
        })
        .then(function () {
          if (favlist.mode != FavList.MODE_OVERWRITE) {
            return Promise.resolve(favlist);
          }

          return removeFavEntry(favlist);
        });
    }

    //

    if ( ! global.DataOp.EventList.Selected.Event.djdatNo) {
      // 選択中のイベントがないのでキャンセル
      console.error(new Error('no event selected'));
      return Promise.reject();
    }

    var progressValue = 0;

    return Promise.resolve(file)
      .then(loadFavCSVText)
      .then(confirmOverwrite)
      .then(insertFavEventName)
      .then(progress)
      .then(insertFavCircleEntries)
      .then(progress)
      .then(insertFavColorEntries)
      .then(progress)
      .then(insertFavUnknownEntries);
  };

  /**
   * CSVファイルを読み込む
   * @param file
   * @returns {Promise}
   */
  function loadFavCSVText (file) {
    // テキストファイルを読み込む
    function readCSVText () {
      return global.Utils.readAsText(file.slice(0, 100, 'text/plain'));
    }

    // お気に入りファイルのエンコーディングの確認
    function detectFileEncoding (text) {
      var v = text.split(',')[3];
      return Promise.resolve(ENCODINGS.indexOf(v) != -1 ? v : ENCODING_SHIFT_JIS);
    }

    // CSVテキストをオブジェクトに変換
    function decodeCSVText (encoding) {
      function onRow (result) {

        function getData(i) {
          return i < length && data[i];
        }

        function getURLs(a) {
          return a.map(function (i) {
            return getData(i - 1)
          }).filter(function (v) {
            return v && /^https?:\/\//.test(v);
          });
        }

        var data = result.data[0];
        var length = data.length;
        if (length < 2) {
          return;
        }

        var key = getData(0);
        if (/^header$/i.test(key)) {
          favlist.eventId = parseInt(/(\d+)$/.exec(data[2])[1]);
        }
        else if (/^color$/i.test(key)) {
          favlist.colors[getData(1)] = {
            checkColor: getData(2).replace(/^(..)(..)(..)$/, '$3$2$1'),
            printColor: getData(3).replace(/^(..)(..)(..)$/, '$3$2$1'),
            label: getData(4)
          };
        }
        else if (/^circle$/i.test(key)) {
          favlist.circles[getData(1)] = {
            colorId: getData(2),
            name: getData(10),
            kana: getData(11),
            author: getData(12),
            mail: getData(15),
            memo: getData(17),
            urls: getURLs([15, 24, 25])
          };
        }
        else if (/^unknown$/i.test(key)) {
          favlist.unknowns.push({
              name: getData(1),
              kana: getData(2),
              author: getData(3),
              memo: getData(4),
              colorId: getData(5),
              mail: getData(8),
              urls: getURLs([8, 12, 13])
            }
          );
        }
      }

      var favlist = {
        user: (file.name.match(/_([^_\.]+)\.csv$/i) || [])[1],
        userId: 0,
        eventId: 0,
        name: null,
        colors: {},
        circles: {},
        unknowns: []
      };

      return new Promise(function (resolve, reject) {
        var config = {
          delimiter: ",",
          newline: "",
          header: false,
          dynamicTyping: false,
          preview: 0,
          encoding: encoding,
          worker: false,
          comments: false,
          step: onRow,
          complete: function () {
            resolve(favlist);
          },
          error: function (e) {
            reject(e);
          },
          download: false,
          keepEmptyRows: false,
          chunk: undefined,
          fastMode: false
        };

        Papa.parse(file, config);
      });
    }

    //

    return Promise.resolve()
      .then(readCSVText)
      .then(detectFileEncoding)
      .then(decodeCSVText);
  }

  /**
   * お気に入りリストの削除
   * @param fav
   * @returns {Promise}
   */
  function removeFavEntry (favlist) {
    var removeQuerySet = [
      {sql: 'DELETE FROM DjdatFavUnknown WHERE djdatNo=?', params: [favlist.eventId]},
      {sql: 'DELETE FROM DjdatFavColor WHERE djdatNo=?', params: [favlist.eventId]},
      {sql: 'DELETE FROM DjdatFavCircle WHERE djdatNo=?', params: [favlist.eventId]},
      {sql: 'DELETE FROM DjdatFav WHERE djdatNo=?', params: [favlist.eventId]}
    ];

    return spawn(function* () {
      for (var i=0; i<removeQuerySet.length; i++) {
        var query = removeQuerySet[i];

        yield SandboxDB.transactionQuery(query);
      }

      console.debug('fav entries removed');
      return favlist;
    });
  }

  /**
   * お気に入り用のイベント名を登録する
   * @param favlist
   * @returns {Promise}
   */
  function insertFavEventName (favlist) {
    var query = {
      sql: 'INSERT OR IGNORE INTO DjdatFav (djdatNo,djdatName) VALUES (?,?)',
      params: [favlist.eventId, favlist.name]
    };

    return SandboxDB.transactionQuery(query)
      .then(function () {
        return Promise.resolve(favlist);
      });
  }

  /**
   * お気に入りのサークルを保存する
   * @param favlist
   * @returns {Promise}
   */
  function insertFavCircleEntries (favlist) {

    function storeFavCircles (res) {
      return spawn(function* () {
        var keys = Object.keys(circles);
        for (var i=0; i<keys.length; i++) {
          var circleId = keys[i];

          var query = {
            sql: 'INSERT OR IGNORE INTO DjdatFavCircle (djdatNo,userId,circleId,colorId,name,kana,author,urls,memo) VALUES (?,?,?,?,?,?,?,?,?)',
            get params () {
              var c = circles[circleId];
              return [favlist.eventId, 1, circleId, c.colorId, c.name, c.kana, c.author, JSON.stringify(c.urls), c.memo];
            },
            onSuccess: function (r) {
              ++requested;
              if (r.rowsAffected) {
                ++stored;
              }
              return r;
            }
          };

          yield SandboxDB.executeQuery(res.transaction, query);
        }
      });
    }

    //

    var circles = favlist.circles;
    var requested = 0;
    var stored = 0;

    return SandboxDB.beginTransaction()
      .then(storeFavCircles)
      .then(function () {
        console.debug('fav(circle) inserted requested='+requested+' stored='+stored);
        return Promise.resolve(favlist);
      });
  }

  /**
   * 未登録サークルを保存する
   * @param favlist
   * @returns {Promise}
   */
  function insertFavUnknownEntries (favlist) {
    function storeFavUnknowns (res) {
      return spawn(function* () {
        for (var i=0; i<unknowns.length; i++) {
          var c = unknowns[i];

          var query = {
            sql: 'INSERT OR IGNORE INTO DjdatFavUnknown (djdatNo,userId,name,kana,author,memo,colorId,urls) VALUES (?,?,?,?,?,?,?,?)',
            params: [favlist.eventId, 1, c.name, c.kana, c.author, c.memo, c.colorId, JSON.stringify(c.urls)],
            onSuccess: function (r) {
              ++requested;
              if (r.rowsAffected) {
                ++stored;
              }
              return r;
            }
          };

          yield SandboxDB.executeQuery(res.transaction, query);
        }
      });
    }

    //

    var unknowns = favlist.unknowns;
    var requested = 0;
    var stored = 0;

    return SandboxDB.beginTransaction()
      .then(storeFavUnknowns)
      .then(function () {
        console.debug('fav(unknown) inserted requested='+requested+' stored='+stored);
        return Promise.resolve(favlist);
      });
  }

  /**
   * お気に入り色テーブルを保存する
   * @param favlist
   * @returns {*}
   */
  function insertFavColorEntries (favlist) {
    function storeFavColors (res) {
      return spawn(function* () {
        var keys = Object.keys(colors);
        for (var i=0; i<keys.length; i++) {
          var colorId = keys[i];

          var query = {
            sql: 'INSERT OR IGNORE INTO DjdatFavColor (djdatNo,userId,colorId,checkColor,printColor,label,enabled) VALUES (?,?,?,?,?,?,?)',
            get params () {
              var c = colors[colorId];
              return [favlist.eventId, 1, colorId, c.checkColor, c.printColor, c.label, 1];
            },
            onSuccess: function (r) {
              ++requested;
              if (r.rowsAffected) {
                ++stored;
              }
              return r;
            }
          };

          yield SandboxDB.executeQuery(res.transaction, query);
        }
      });
    }

    //

    var colors = favlist.colors;
    var requested = 0;
    var stored = 0;

    return SandboxDB.beginTransaction()
      .then(storeFavColors)
      .then(function () {
        console.debug('fav(color) inserted requested='+requested+' stored='+stored);
        return Promise.resolve(favlist);
      });
  }

  /**
   * 別のイベントのお気に入りリストからサークルリストを検索して新しいお気に入りを作成
   * @param favlist
   * @returns {Promise}
   */
  function convertFavCircleEntries (favlist) {

    // お気に入りにエントリを追加する
    function addFavEntry (circles,item,fav) {
      if (circles[item.circleId]) {
        // 上書きはしない
        return;
      }

      return circles[item.circleId] = global.DataOp.FavCircle.genFavData(favlist.userId, item, fav);
    }

    function fetchMatchedCircles (res) {
      return spawn(function* () {
        for (var i=0; i<obj.length; i++) {
          var fav = obj[i];

          ++requested;

          // サークル名 or 作家名での検索
          var c = yield global.DataOp.Search.executeFetchCirclesByNameListQuery(res.transaction, [{name:fav.name,author:fav.author}])
          if (c) {
            ++matched;
            Object.keys(c).forEach(function (circleId) {
              if (addFavEntry(favlist.circles,c[circleId].Circle,fav)) {
                ++stored;
              }
            });
          }

          // URLでの検索
          if (fav.urls.length == 0) {
            continue;
          }

          var c = yield global.DataOp.Search.executeFetchCirclesByURLListQuery(res.transaction, [fav.urls], null);
          if (c) {
            // URLでヒットした
            ++matchedURL;
            Object.keys(c).forEach(function (circleId) {
              if (addFavEntry(favlist.circles,c[circleId].Circle,fav)) {
                ++storedURL;
              }
            });
          }
        }
      });
    }

    //

    // CSVのエントリからキーを集める
    var obj = [];
    for (var p in favlist.circles) {
      obj.push(favlist.circles[p]);
    }
    favlist.unknowns.forEach(function (e) {
      obj.push(e);
    });

    // 集めたら結果を入れるので空にする
    favlist.circles = {};
    favlist.unknowns = [];

    var requested = 0;
    var matched = 0;
    var matchedURL = 0;
    var stored = 0;
    var storedURL = 0;

    return SandboxDB.beginTransaction()
      .then(fetchMatchedCircles)
      .then(function () {
        console.debug('fav(circle) converted requested='+requested+' matched='+matched+'/'+stored+' matchedURL='+matchedURL+'/'+storedURL);
        return Promise.resolve(favlist);
      });
  }

  /*
   * 保存
   */

  /**
   * お気に入りをCSVテキストに変換する
   * @returns {*}
   */
  FavList.getFavCSVText = function () {

    // サークル詳細を埋める
    function setFavCircleDetails (res) {
      return spawn(function* () {
        for (var i=0; i<data.length; i++) {
          var o = data[i];

          if (o[0] != 'Circle') {
            continue;
          }

          var r = yield global.DataOp.Circle.fetchCircle(o[1], {transaction:res.transaction, result:{}});

          var c = r.Circle;
          o[3] = c.pageNo;
          o[4] = c.cutIndex;
          o[5] = global.Utils.wdayToStr(global.DataOp.EventList.Selected.Date[c.dateId].weekday - 1);
          o[6] = global.DataOp.EventList.Selected.Map[global.DataOp.EventList.Selected.Block[c.blockId].mapId].name.slice(0, 1);
          o[7] = global.DataOp.EventList.Selected.Block[c.blockId].name;
          o[8] = c.spaceNo;
          o[9] = global.DataOp.EventList.Selected.Genre[c.genreId].code;
          o[10] = c.name;
          o[11] = c.kana;
          o[12] = c.author;
          o[13] = c.bookName;
          o[14] = c.url || c.circlems || c.rss;
          o[15] = c.mailAddr;
          o[16] = c.description;

          o[18] = c.xpos;
          o[19] = c.ypos;
          o[20] = c.layout;
          o[21] = c.spaceNoSub;
        }

        return res;
      });
    }

    // CSVに変換する
    function encodeCSV () {
      var escapeFunction = (function () {
        if (FAV_CONFIG.encoding == ENCODING_SHIFT_JIS) {
          return EscapeSJIS;
        }
        if (FAV_CONFIG.encoding == ENCODING_JIS) {
          return EscapeJIS8;
        }
        if (FAV_CONFIG.encoding == ENCODING_EUC_JP) {
          return EscapeEUCJP;
        }
        return EscapeUTF8;
      })();

      var text = Papa.unparse(data, {
        quotes: false,
        delimiter: ",",
        newline: FAV_CONFIG.newline
      });

      return Promise.resolve(unescape(escapeFunction(text+FAV_CONFIG.newline)));
    }

    //

    var data = [];

    // ヘッダ
    data.push(['Header', FAV_CONFIG.sign, FAV_CONFIG.event, FAV_CONFIG.encoding, FAV_CONFIG.app]);

    // お気に入り色テーブル
    global.DataOp.FavColor.getSortedKeys().forEach(function (colorId) {
      var c = global.DataOp.FavColor.findFavColor(colorId);
      data.push([
        'Color',
        c.colorId,
        c.checkColor.replace(/^(..)(..)(..)$/,'$3$2$1'),
        c.printColor.replace(/^(..)(..)(..)$/,'$3$2$1'),
        c.label
      ]);
    });

    // サークルエントリー
    global.DataOp.FavCircle.getSortedKeys().forEach(function (circleId) {
      var fav = global.DataOp.FavCircle.findFavCircle(circleId);
      if (fav.Fav.colorId < 0) {
        return;
      }

      data.push([
        'Circle',
        circleId,
        fav.Fav.colorId,
        1,
        1,
        fav.Circle.dateId,
        1,
        fav.Circle.blockId,
        fav.Circle.spaceNo,
        1,
        fav.Circle.name,
        '',
        fav.Circle.author,
        '',
        '',
        '',
        '',
        fav.Fav.memo,
        0,
        0,
        0,
        0
      ]);
    });

    return SandboxDB.beginTransaction()
      .then(setFavCircleDetails)
      .then(encodeCSV);
  };

  /*
   * リスト管理
   */

  /**
   * 存在するお気に入りのイベント名リストを取得
   * @returns {Promise}
   */
  FavList.getFavListEntries = function () {
    var query = {
      sql: 'SELECT i.djdatNo,i.djdatName,MIN(d.year) year,MIN(d.month) month,MIN(d.day) day FROM DjdatFav i LEFT OUTER JOIN DjdatDate d ON i.djdatNo=d.djdatNo GROUP BY i.djdatNo ORDER BY d.year DESC,d.month DESC,d.day DESC;',
      get params () {
        return [];
      },
      onSuccess: function (r) {
        if (r.rows.length == 0) {
          return;
        }

        var  obj = [];
        for (var i=0; i<r.rows.length; i++) {
          obj.push(r.rows.item(i));
        }

        return obj;
      },
      onError: function (e) {
        return e;
      }
    };

    return SandboxDB.transactionQuery(query);
  };

  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.FavList = FavList;

})(DjCRDView);
