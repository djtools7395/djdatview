// FIXME Layout情報

(function (global) {

  var Database = {};

  //---

  Database.DEFS = {
    shortName: "DjCRDView",
    version: "1",
    displayName: "DjCRDView Storage",
    maxSize: 2*1024*1024*1024
  };

  const TABLE_DEF = {
    table: (function () {
      const defs = {
        tables: {
          DjdatEnv: {
            columns: {
              id: {type:"INTEGER", constraint: "unique"},
              env: {type: "TEXT"}
            }
          },

          DjdatInfo: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              djdatName: {type: "TEXT"},
              cutSizeW: {type: "INTEGER"},
              cutSizeH: {type: "INTEGER"},
              cutOriginX: {type: "INTEGER"},
              cutOriginY: {type: "INTEGER"},
              cutOffsetX: {type: "INTEGER"},
              cutOffsetY: {type: "INTEGER"},
              mapSizeW: {type: "INTEGER"},
              mapSizeH: {type: "INTEGER"},
              mapOriginX: {type: "INTEGER"},
              mapOriginY: {type: "INTEGER"}
            },

            primaryKey: ["djdatNo"]
          },

          DjdatDate: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              dateId: {type: "INTEGER", constraint: "not null"},
              year: {type: "INTEGER"},
              month: {type: "INTEGER"},
              day: {type: "INTEGER"},
              // weekdayは 1-7 で、 0-6 ではない
              weekday: {type: "INTEGER"}
            },

            primaryKey: ["djdatNo", "dateId"]
          },

          DjdatMap: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              mapId: {type: "INTEGER", constraint: "not null"},
              name: {type: "TEXT"},
              filename: {type: "TEXT"},
              x: {type: "INTEGER"},
              y: {type: "INTEGER"},
              w: {type: "INTEGER"},
              h: {type: "INTEGER"},
              allFilename: {type: "TEXT"},
              x2: {type: "INTEGER"},
              y2: {type: "INTEGER"},
              w2: {type: "INTEGER"},
              h2: {type: "INTEGER"},
              rotate: {type: "INTEGER"}
            },

            primaryKey: ["djdatNo", "mapId"]
          },

          DjdatArea: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              areaId: {type: "INTEGER", constraint: "not null"},
              name: {type: "TEXT"},
              simpleName: {type: "TEXT"},
              mapId: {type: "INTEGER", constraint: "not null"},
              x: {type: "INTEGER"},
              y: {type: "INTEGER"},
              w: {type: "INTEGER"},
              h: {type: "INTEGER"},
              allFilename: {type: "TEXT"},
              x2: {type: "INTEGER"},
              y2: {type: "INTEGER"},
              w2: {type: "INTEGER"},
              h2: {type: "INTEGER"}
            },

            primaryKey: ["djdatNo", "areaId"]
          },

          DjdatBlock: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              blockId: {type: "INTEGER", constraint: "not null"},
              name: {type: "TEXT"},
              areaId: {type: "INTEGER", constraint: "not null"},
              // ブロック内に偶数番号のスペースが存在しない場合は、01a,01b,02a,02b,...ではなく01,02,03,04,...という配置であると判定する
              noab: {type: "INTEGER"}
            },

            primaryKey: ["djdatNo", "blockId"]
          },

          DjdatGenre: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              genreId: {type: "INTEGER", constraint: "not null"},
              name: {type: "TEXT"},
              code: {type: "INTEGER"}
            },

            primaryKey: ["djdatNo", "genreId"]
          },

          DjdatLayout: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              blockId: {type: "INTEGER"},
              spaceNo: {type: "INTEGER"},
              xpos: {type: "INTEGER"},
              ypos: {type: "INTEGER"},
              layout: {type: "INTEGER"},
              mapId: {type: "INTEGER"}
            },

            primaryKey: ["djdatNo", "blockID", "spaceNo"],

            indexes: [
              ["djdatNo", "mapId"],
              ["djdatNo", "xpos"],
              ["djdatNo", "ypos"]
            ]
          },

          DjdatCircle: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              circleId: {type: "INTEGER", constraint: "not null"},
              pageNo: {type: "INTEGER"},
              cutIndex: {type: "INTEGER"},
              // FIXME day -> dateId
              day: {type: "INTEGER"},
              blockId: {type: "INTEGER"},
              spaceNo: {type: "INTEGER"},
              spaceNoSub: {type: "INTEGER"},
              genreId: {type: "INTEGER"},
              // FIXME circleName -> name
              circleName: {type: "TEXT"},
              circleNameNrm: {type: "TEXT"},
              // FIXME circleKana -> kana
              circleKana: {type: "TEXT"},
              // FIXME penName -> author
              penName: {type: "TEXT"},
              penNameNrm: {type: "TEXT"},
              bookName: {type: "TEXT"},
              url: {type: "TEXT"},
              mailAddr: {type: "TEXT"},
              description: {type: "TEXT"},
              memo: {type: "TEXT"},
              updateId: {type: "INTEGER"},
              updateData: {type: "TEXT"},
              circlems: {type: "TEXT"},
              rss: {type: "TEXT"},
              updateFlag: {type: "INTEGER"},
              extraUrl1: {type: "TEXT"},
              extraUrl2: {type: "TEXT"},
              extraUrl3: {type: "TEXT"}
            },

            primaryKey: ["djdatNo", "circleId"],

            indexes: [
              ["djdatNo", "day", "blockId", "spaceNo"],
              ["djdatNo", "day", "blockId"],
              ["pageNo", "djdatNo"],
              ["circleName", "djdatNo"],
              ["circleNameNrm", "djdatNo"],
              ["circleKana", "djdatNo"],
              ["penName", "djdatNo"],
              ["penNameNrm", "djdatNo"],
              ["updateId", "djdatNo"],
              ["djdatNo", "url"],
              ["djdatNo", "circlems"],
              ["djdatNo", "rss"],
              ["djdatNo", "extraUrl1"],
              ["djdatNo", "extraUrl2"],
              ["djdatNo", "extraUrl3"]
            ]
          },

          // 発行物情報
          DjdatPublication: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              circleId: {type: "INTEGER", constraint: "not null"},
              id: {type: "INTEGER"},
              class: {type: "INTEGER"},
              category: {type: "INTEGER"},
              type: {type: "INTEGER"},
              method: {type: "INTEGER"},
              terms: {type: "INTEGER"},
              price: {type: "INTEGER"},
              limitation: {type: "INTEGER"},
              num: {type: "INTEGER"},
              title: {type: "TEXT"},
              desc: {type: "TEXT"},
              urls: {type: "TEXT"},
              shops: {type: "TEXT"},
              image: {type: "BLOB"},
            },

            primaryKey: ["djdatNo", "circleId", "id"]
          },

          DjdatCutImages: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              circleId: {type: "INTEGER", constraint: "not null"},
              image: {type: "BLOB"},
              altImage: {type: "BLOB"}
            },

            primaryKey: ["djdatNo", "circleId"]
          },

          DjdatEtcImages: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              imageId: {type: "INTEGER", constraint: "not null"},
              image: {type: "BLOB"},
              altImage: {type: "BLOB"}
            },

            primaryKey: ["djdatNo", "imageId"]
          },

          DjdatMapImages: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              mapId:  {type: "INTEGER", constraint: "not null"},
              dateId:  {type: "INTEGER", constraint: "not null"},
              type: {type: "TEXT", constraint: "not null"},
              image: {type: "BLOB", constraint: "not null"},
              altImage: {type: "BLOB"}
            },

            primaryKey: ["djdatNo", "mapId", "dateId", "type"],

            indexes:[
              ["djdatNo"]
            ]
          },

          // お気に入りイベント名（イベント情報が削除されてもお気に入り情報は残るので）
          DjdatFav: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              djdatName: {type: "TEXT"}
            },

            primaryKey: ["djdatNo"]
          },

          // お気に入りサークル
          DjdatFavCircle: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              userId: {type: "INTEGER", constraint: "not null"},
              circleId: {type: "INTEGER", constraint: "not null"},
              name: {type: "TEXT"},
              kana: {type: "TEXT"},
              author: {type: "TEXT"},
              memo: {type: "TEXT"},
              colorId: {type: "INTEGER", constraint: "not null"},
              urls: {type: "TEXT"},
              timestamp:  {type: "TEXT"}
            },

            primaryKey: ["djdatNo", "userId", "circleId"],

            indexes: [
              ["djdatNo"],
              ["djdatNo", "userId"],
              ["djdatNo", "userId", "circleId"]
            ]
          },

          DjdatFavUnknown: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              userId: {type: "INTEGER", constraint: "not null"},
              name: {type: "TEXT"},
              kana: {type: "TEXT"},
              author: {type: "TEXT"},
              memo: {type: "TEXT"},
              colorId: {type: "INTEGER", constraint: "not null"},
              urls: {type: "TEXT"},
              timestamp:  {type: "TEXT"}
            },

            primaryKey: ["djdatNo", "name", "author"],

            indexes: [
              ["djdatNo"],
              ["djdatNo", "userId"],
            ]
          },


          // お気に入り配色
          DjdatFavColor: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              userId: {type: "INTEGER", constraint: "not null"},
              colorId: {type: "INTEGER", constraint: "not null"},
              checkColor: {type: "TEXT"},
              printColor: {type: "TEXT"},
              label: {type: "TEXT"},
              enabled: {type: "INTEGER"}
            },

            primaryKey: ["djdatNo", "userId", "colorId"],

            indexes: [
              ["djdatNo"],
              ["djdatNo", "userId"]
            ]
          },

          // お気に入り所有者
          DjdatFavUser: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              userId: {type: "INTEGER", constraint: "not null"},
              name: {type: "TEXT", constraint: "not null"}
            },

            primaryKey: ["djdatNo", "userId"],

            indexes: [
              ["djdatNo"]
            ]
          },

          DjdatCondition: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              condId: {type: "INTEGER", constraint: "not null"},
              label: {type: "TEXT"},
              alarm: {type: "INTEGER"},
              enabled: {type: "INTEGER"}
            },

            primaryKey: ["djdatNo", "condId"],

            indexes: [
              ["djdatNo"],
              ["djdatNo", "condId"]
            ]
          },

          // 行動履歴
          DjdatConditionHistory: {
            columns: {
              djdatNo: {type: "INTEGER", constraint: "not null"},
              circleId: {type: "INTEGER", constraint: "not null"},
              condId: {type: "INTEGER", constraint: "not null"},
              timestamp: {type: "INTEGER", constraint: "not null"}
            },

            indexes: [
              ["djdatNo","circleId"],
              ["djdatNo"]
            ]
          }

        }
      };

      var sqls = [];
      var names = [];
      for (var tableName in defs.tables) {
        names.push(tableName);
        var tableDef = defs.tables[tableName];

        var colDef = tableDef.columns;
        var fs = [];
        for (var colName in colDef) {
          fs.push(colName + ' ' + colDef[colName].type + (colDef[colName].constraint ? ' '+colDef[colName].constraint : ''));
        }
        sqls.push('create table if not exists '+tableName+' ('+fs.join()+(tableDef.primaryKey ? ',PRIMARY KEY ('+tableDef.primaryKey.join()+')' : '')+')');

        if (tableDef.indexes) {
          tableDef.indexes.forEach(function (columns) {
            var indexName = (tableName+'_index_'+columns.join()).replace( /,/g,'_');
            sqls.push('create index if not exists '+indexName+' on '+tableName+' ('+columns.join()+')');
          });
        }
      }

      return {
        createSQLs: sqls,
        names: names
      };
    })()

  };

  //

  /**
   *
   * @returns {{shortName: string, version: string, displayName: string, maxSize: number}}
   */
  Database.getDbDef = function () {
    return DB_DEF;
  };

  /**
   *
   * @returns {*}
   */
  Database.createTables = function () {

    return SandboxDB.beginTransaction().then(function (res) {
      return spawn(function* () {
        for (var i=0; i<TABLE_DEF; i++) {
          var query = {
            sql: sql,
            params: []
          };

          yield SandboxDB.executeQuery(res.transaction, query);
        }
      });
    });

  };

  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.Database = Database;

})(DjCRDView);