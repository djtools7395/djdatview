// お気に入り色テーブル

(function (global) {

  var FavColor = {};

  // キャッシュ
  var favColorCache = {};

  FavColor.clearCache = function () {
    favColorCache = {
      data: {},
      sortedKeys: []
    };

    return Promise.resolve(favColorCache).then(function (res) {

      var query = {
        sql: 'SELECT colorId,checkColor,printColor,label,enabled FROM DjdatFavColor WHERE djdatNo=? AND userId=?',
        get params () {
          return [global.Env.selectedEventId, 1];
        },
        onSuccess: function (r) {
          for (var i=0; i<r.rows.length; i++) {
            var item = r.rows.item(i);
            if (isNaN(item.colorId)) {
              continue;
            }

            var obj = {};
            for (var p in item) {
              obj[p] = item[p];
            }

            res.data[obj.colorId] = obj;
          }

          return res;
        },
        onError: function (e) {
          return e;
        }
      };

      return SandboxDB.transactionQuery(query);

    }).then(function (res) {

      var cached = Object.keys(res.data).length;

      global.Const.FavColors.forEach(function (color,index) {
        var colorId = index+1;
        if (res.data[colorId]) {
          return;
        }

        // DB上に存在しないものは既定値を使う
        res.data[colorId] = {
          colorId: colorId,
          checkColor: color,
          printColor: color,
          label: null,
          enabled: 1
        };
      });

      res.sortedKeys = global.Utils.getSortedKeys(res.data);

      console.debug('CACHE fav color table length='+Object.keys(res.data).length+' cached='+cached);

      return Promise.resolve(res);

    });
  };

  //---

  /**
   *
   * @param colorId
   * @param enabled
   */
  FavColor.setFavColorEnabled = function (colorId, enabled) {

    var color = favColorCache.data[colorId];
    if (!color) {
      return Promise.reject(new Error('wrong color id colorId='+colorId));
    }

    color.enabled = enabled;

    var query = {
      sql: 'INSERT OR REPLACE INTO DjdatFavColor (djdatNo,userId,colorId,checkColor,printColor,label,enabled) VALUES (?,?,?,?,?,?,?)',
      get params () {
        return [global.Env.selectedEventId, 1, colorId, color.checkColor, color.printColor, color.label, color.enabled];
      }
    };

    return SandboxDB.transactionQuery(query);
  };

  /**
   *
   * @param condId
   * @returns {*}
   */
  FavColor.findFavColor = function (condId) {
    return favColorCache.data[condId];
  };

  /**
   *
   * @returns {*}
   */
  FavColor.getSortedKeys = function () {
    return favColorCache.sortedKeys;
  };


  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.FavColor = FavColor;

})(DjCRDView);