
// FIXME FavCondition に変更があったら FavCircleCache も更新する必要がある

(function (global) {

  var FavCondition = {};

  // キャッシュ
  var favCondCache = new LRUCache(global.Env.favCircleCacheSize > 0 ? global.Env.favCircleCacheSize : 120);

  /**
   * キャッシュのクリア
   */
  FavCondition.clearCache = function () {
    favCondCache.removeAll();

    console.debug('CACHE fav condition cleared');

    return Promise.resolve();
  };

  //---

  /**
   * 状況を更新する
   * @param circleId
   * @param condId
   */
  function updateFavCondition (circleId, condId) {
    return Promise.resolve()
      .then(function () {

        // 新しい状況情報の生成
        var newCond = global.DataOp.FavCircle.genCondData(condId, Date.now());

        var query = {
          sql: 'INSERT INTO DjdatConditionHistory (djdatNo,circleId,condId,timestamp) VALUES (?,?,?,?)',
          get params () {
            return [global.Env.selectedEventId, circleId, newCond.condId, newCond.timestamp];
          },
          onSuccess: function (r) {
            return newCond;
          },
          onError: function (e) {
            return e;
          }
        };

        return SandboxDB.transactionQuery(query);

      })
      .then(function (obj) {

        // キャッシュに保存
        favCondCache.put(circleId, obj);

        console.debug('cond updated', circleId, obj.condId);
        return Promise.resolve(obj);

      })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      });
  }

  /**
   * 状況を一つ戻す
   * @param circleId
   */
  function revertFavCondition (circleId) {
    return Promise.resolve().then(function () {
      // 最新の状況を２つ分さがす
      var query = {
        sql: 'SELECT timestamp,condId FROM DjdatConditionHistory WHERE djdatNo=? AND circleId=? ORDER BY timestamp DESC LIMIT 2',
        get params () {
          return [global.Env.selectedEventId, circleId];
        },
        onSuccess: function (r) {
          if (r.rows.length == 0) {
            // 状況履歴がないのでなにもしない
            console.debug('no conditions', circleId);
            return;
          }

          var cond = (function () {
            if (r.rows.length == 1) {
              // 状況履歴が空になるので、新しい状況は'未'
              return null;
            }

            // 新しい状況は一つ前のもの
            var item = r.rows.item(1);
            var obj = {};
            for (var p in item) {
              obj[p] = item[p];
              if (p === 'timestamp') {
                obj[p] = parseInt(obj[p]);
              }
            }
            return obj;
          })();

          return {removeTo:r.rows.item(0).timestamp, cond:cond};
        },
        onError: function (e) {
          return e;
        }
      };

      return SandboxDB.transactionQuery(query);

    }).then(function (obj) {
      // 最新の状況を削除する
      var query = {
        sql: 'DELETE FROM DjdatConditionHistory WHERE djdatNo=? AND circleId=? AND timestamp=?',
        get params () {
          return [global.Env.selectedEventId, circleId, obj.removeTo];
        },
        onSuccess: function (r) {
          return obj.cond;
        },
        onError: function (e) {
          return e;
        }
      };

      return SandboxDB.transactionQuery(query);

    }).then(function (obj) {
      // キャッシュに保存
      favCondCache.put(circleId, obj);

      if (obj) {
        console.debug('cond reverted', circleId, obj.condId);
      }
      else {
        console.debug('cond reverted', circleId, 'empty');
      }

      return Promise.resolve(obj);
    }).catch(function (e) {
      if (e)
        console.error(e.stack || e);
    });
  }

  /**
   *
   * @param condId
   */
  FavCondition.setFavCondition = function (circleId, condId) {

    if (condId == -1) {
      // 一個戻す
      return revertFavCondition(circleId);
    }

    var cond = global.DataOp.Condition.findCondition(condId);
    if (!cond) {
      console.error(new Error('invalid condition id: '+circleId+' , '+condId));
      return;
    }

    // 更新する
    return updateFavCondition(circleId, condId);
  };

  /**
   *
   * @param onRow
   * @returns {Promise}
   */
  FavCondition.getFavConditionHistoriesAll = function () {

    var query = {
      sql: 'SELECT timestamp,circleId,condId FROM DjdatConditionHistory WHERE djdatNo=? ORDER BY timestamp',
      get params () {
        return [global.Env.selectedEventId];
      },
      onSuccess: function (r) {
        if (r.rows.length == 0) {
          // 状況履歴がないのでなにもしない
          console.debug('no conditions', global.Env.selectedEventId);
          return;
        }

        var cache = [];
        for (var i=0; i< r.rows.length; i++) {
          var item = r.rows.item(i);
          var obj = {};
          for (var p in item) {
            obj[p] = item[p];
          }
          cache.push(obj);
        }

        return cache;
      },
      onError: function (e) {
        return e;
      }
    };

    return SandboxDB.transactionQuery(query);
  };

  /**
   *
   * @param json
   * @param progress
   * @returns {*}
   */
  FavCondition.storeFavConditionHistoriesAll = function (json, progress) {
    return SandboxDB.beginTransaction()
      .then(function (r) {
        // 古い状況情報の削除
        var query = {
          sql: 'DELETE FROM DjdatConditionHistory WHERE djdatNo=?',
          get params() {
            return [global.Env.selectedEventId];
          }
        };

        return SandboxDB.executeQuery(r.transaction, query);
      })
      .then(function (r) {
        // 新しい状況情報の生成
        return spawn(function* () {
          for (var i=0; i<json.histories.length; i++) {
            var hist = json.histories[i];

            var query = {
              sql: 'INSERT INTO DjdatConditionHistory (djdatNo,circleId,condId,timestamp) VALUES (?,?,?,?)',
              get params () {
                return [global.Env.selectedEventId, hist.circleId, hist.condId, parseInt(hist.timestamp,10)];
              },
              onSuccess: function (r) {
                //console.debug('o', (i+1), hist.circleId, r);
                progress({value: i+1});
                return r;
              },
              onError: function (e) {
                return e;
              }
            };

            yield SandboxDB.executeQuery(r.transaction, query);
          }
        });
      });
  };

  /**
   * 状況履歴の検索
   * @param circleId
   * @returns {Promise}
   */
  FavCondition.getFavConditionHistory = function (circleId) {

    var cache = favCondCache.get(circleId);
    if (cache !== undefined) {
      // キャッシュにヒット
      return Promise.resolve(cache);
    }

    return SandboxDB.beginTransaction()
      .then(function (res) {
        return FavCondition.fetchFavConditionHistory(circleId,res);
      });
  };

  /**
   *
   * @param circleId
   * @param res
   * @returns {Promise}
   */
  FavCondition.fetchFavConditionHistory = function (circleId, res) {

    var query = {
      sql: 'SELECT timestamp,condId FROM DjdatConditionHistory WHERE djdatNo=? AND circleId=? ORDER BY timestamp DESC LIMIT 1',
      get params () {
        return [global.Env.selectedEventId, circleId];
      },
      onSuccess: function (r) {
        if (r.rows.length == 0) {
          return null;
        }

        var item = r.rows.item(0);

        var obj = {};
        for (var p in item) {
          obj[p] = item[p];
        }

        return obj;
      },
      onError: function (e) {
        return e;
      }
    };

    return SandboxDB.executeQuery(res.transaction, query)
      .then(function (res) {

        // obj == null もキャッシュ
        favCondCache.put(circleId,res.result);

        return Promise.resolve(res.result);
      });
  };

  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.FavCondition = FavCondition;

})(DjCRDView);