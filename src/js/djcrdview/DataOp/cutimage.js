(function (global) {

  var CutImage = {};

  // キャッシュ
  var cutImageCache = new LRUCache(global.Env.cutImageCacheSize > 0 ? global.Env.cutImageCacheSize : 120);

  var etcImageCache = {};

  const JIKO_IMAGE_ID = 'JIKO';

  const ALTCUT_IS_JPEG = 1;

  /**
   * キャッシュのクリア
   */
  CutImage.clearCache = function () {

    cutImageCache.removeAll();
    etcImageCache = {};

    // ブロック名画像等は全部読み出し
    return clearEtcImageCache();
  };

  /**
   * その他の画像を全部読みだしてリセット
   * @returns {*}
   */
  function clearEtcImageCache () {
    //
    function addImagesToCache (obj) {
      return spawn(function* () {
        var keys = Object.keys(obj);
        for (var i=0; i<keys.length; i++) {
          var key = keys[i];
          obj[key].image = yield decodeStoredImage(obj[key].image);
        }

        return obj;
      });
    }

    //
    var query = {
      sql: 'SELECT imageId,image,altImage FROM DjdatEtcImages WHERE djdatNo=?',
      params: [global.Env.selectedEventId],
      onSuccess: function (r) {
        if (r.rows.length == 0) {
          // スキップ
          console.debug('CACHE no block images eventId='+global.Env.selectedEventId);
          return null;
        }

        for (var i=0; i<r.rows.length; i++) {
          var item = r.rows.item(i);

          var obj = {};
          for (var p in item) {
            obj[p] = item[p];
          }

          etcImageCache[obj.imageId] = obj;
        }

        return etcImageCache;
      },
      onError: function (e) {
        return e;
      }
    };

    return SandboxDB.transactionQuery(query)
      .then(addImagesToCache)
      .then(function (obj) {
        console.debug('CACHE etc images length='+Object.keys(obj).length);
      });
  }

  //---

  // TODO 引数のpageIdは削除の方向で

  /**
   *
   * @param pageId
   * @param circleId
   * @param image
   * @returns {*}
   */
  CutImage.addCutImageAlt = function (pageId, circleId, image) {

    return Promise.resolve().then(function () {
      // 既に登録済みか確認して
      return CutImage.getCutImageEntry(circleId);
    })
      .then(function (obj) {
        if (!obj) {
          // FIXME カット画像のエントリーが無い場合はUPDATEではなくINSERT
          return Promise.reject();
        }

        if (!obj.altImage) {
          return Promise.resolve();
        }

        var config = {
          title: '警告',
          content: '既に差し替えカット画像が存在しています。',
          message: '上書きしますか？',
          buttons: [
            {label: '上書きする', value:true},
            {label: 'キャンセル'}
          ]
        };

        return global.GuiParts.DynDialog.show(config).then(function (r) {
          if (!r.value) {
            // キャンセル
            return Promise.reject();
          }

          return Promise.resolve();
        });
      }
    ).then(function () {
        // 画像サイズを加工して
        return global.GuiParts.Bitmap.fitBitmapToCutSize(image);
      }
    ).then(function (image) {
        // DBに登録
        return replaceCutImageAlt(circleId, image);
      }
    );
  };

  /**
   * DBに差し替えカット画像を登録する
   * @param circleId
   * @param image
   * @returns {Promise}
   */
  function replaceCutImageAlt (circleId, image) {
    var query = {
      sql: 'UPDATE DjdatCutImages SET altImage=? WHERE djdatNo=? AND circleId=?',
      get params() {
        return [path, global.Env.selectedEventId, circleId];
      },
      onSuccess: function (r) {
        var cache = cutImageCache.get(circleId);
        if (cache) {
          cache.altImage = image;
        }
        return image;
      },
      onError: function (e) {
        return e;
      }
    };

    var fileName = [circleId, ALTCUT_IS_JPEG ? 'JPG' : 'PNG'].join('.');
    var mimeType = ALTCUT_IS_JPEG ? 'image/jpeg' : 'image/png';
    var path = global.DataOp.RomList.getAltCutImagePath(global.Env.selectedEventId, fileName);

    return SandboxFS.openFileWriter(path, true)
      .then(function (fileWriter) {
        var obj = global.Utils.dataURItoUint8Array(image.toDataURL(mimeType));
        return SandboxFS.writeUint8Array(fileWriter, obj.uint8a);
      })
      .then(function () {
        return SandboxDB.transactionQuery(query);
      });
  }

  /**
   *
   * @param circleId
   * @returns {*}
   */
  function findCutImageCache (circleId) {
    return cutImageCache.get(circleId);
  }

  /**
   * DB上のカット画像と差し替え画像を取得する（単品）
   * @param circleId
   * @returns {Promise}
   */
  CutImage.getCutImageEntry = function (circleId) {
    return CutImage.getCutImageEntryMultiple([circleId],false)
      .then(function (images) {
        return images[circleId];
      });
  };

  /**
   * DB上のカット画像と差し替え画像を取得する（複数まとめて）
   * @param cutKeys
   * @returns {*}
   */
  CutImage.getCutImageEntryMultiple = function (cutKeys, isUseJiko) {

    var images = {};

    // キャッシュにヒットしないサークルIDを抽出する
    var fetchKeys = cutKeys.filter(function (circleId) {
      var img = findCutImageCache(circleId);
      images[circleId] = img;
      return !img;
    });

    if (fetchKeys.length == 0) {
      // キャッシュで全部そろった
      return Promise.resolve(images);
    }

    // SQLのバインド変数分'?'を作る
    var binds = fetchKeys.map(function () {return '?'}).join(',');

    var query = {
      sql: 'SELECT circleId,image,altImage FROM DjdatCutImages WHERE djdatNo=? AND circleId IN ('+binds+')',
      params: [global.Env.selectedEventId].concat(fetchKeys),
      onSuccess: function (r) {
        var results = {};
        for (var i=0; i<r.rows.length; i++) {
          var item = r.rows.item(i);
          var obj = {};
          var circleId = item['circleId'];
          for (var p in item) {
            if (p != 'circleId') {
              obj[p] = item[p];
            }
          }

          results[circleId] = obj;
        }

        if (cutKeys.length > 1) {
          console.debug('cut images fetched requested='+cutKeys.length+' specified='+fetchKeys.length+' length='+r.rows.length);
        }

        return results;
      }
    };

    return xyz(images, query, isUseJiko);
  };

  /**
   *
   * @param day
   * @param blockId
   * @param isUseJiko
   * @returns {*}
   */
  CutImage.getCutImageInBlock = function (day, blockId, isUseJiko) {

    var query = {
      sql: 'SELECT circleId,image,altImage FROM DjdatCutImages WHERE djdatNo=? AND circleId IN (SELECT circleId FROM DjdatCircle WHERE djdatNo=? AND day=? AND blockId=?)',
      params: [global.Env.selectedEventId, global.Env.selectedEventId, day, blockId],
      onSuccess: function (r) {
        var results = {};
        for (var i=0; i<r.rows.length; i++) {
          var item = r.rows.item(i);
          var obj = {};
          var circleId = item['circleId'];
          if (findCutImageCache(circleId)) {
            // すでにある
            continue;
          }

          for (var p in item) {
            if (p != 'circleId') {
              obj[p] = item[p];
            }
          }

          results[circleId] = obj;
        }

        return results;
      }
    };

    return xyz({}, query, isUseJiko);
  };

  // FIXME xyzって…
  /**
   *
   * @param query
   * @returns {Promise.<T>}
   */
  function xyz (images, query, isUseJiko) {

    var cutImageDim = {
      w: global.DataOp.EventList.Selected.Event.cutSizeW * 2,
      h: global.DataOp.EventList.Selected.Event.cutSizeH * 2
    };

    var bg = null; //'#000000';

    return SandboxDB.transactionQuery(query)
      .then(function (results) {
        // DataURLをcanvasに変換
        return spawn(function* () {
          images = images || {};

          var keys = Object.keys(results);
          for (var i=0; i<keys.length; i++) {
            var circleId = keys[i];
            var obj = results[circleId];
            yield Promise.all([
                // 通常画像
                obj.altImage ? null : decodeStoredImage(obj.image, cutImageDim.w, cutImageDim.h, bg),
                // 差し替え画像
                decodeStoredImage(obj.altImage, cutImageDim.w, cutImageDim.h, bg)
              ])
              .then(function (r) {

                obj.image = r[0];
                obj.altImage = r[1];

                if (!obj.image && !obj.altImage) {
                  if (isUseJiko) {
                    images[circleId] = CutImage.findJikoImageEntry();
                  }
                  return;
                }

                // キャッシュに保存
                cutImageCache.put(circleId, obj);

                // 戻り値にも保存
                images[circleId] = obj;
              });
          }

          return images;
        });
      });
  }

  /**
   *
   * @param image
   * @param w
   * @param h
   * @param bg
   * @returns {*}
   */
  function decodeStoredImage (image, w, h, bg) {

    if (!image) {
      return Promise.resolve();
    }

    if (/^data:/.test(image)) {
      // (旧)dataURL形式でDBに登録している場合
      return global.GuiParts.getImageBuffer(image, w, h, bg);
    }

    // ローカルのアーカイブファイルに保存している場合
    var m = /^(\d+),(\d+)$/.exec(image);
    if (m) {
      var top = parseInt(m[1], 10);
      var end = top + parseInt(m[2], 10);
      var url = URL.createObjectURL(global.DataOp.EventList.Selected.CutCCZ.slice(top, end));
      return global.GuiParts.getImageBuffer(url, w, h, bg)
        .then(function (image) {
          URL.revokeObjectURL(url);
          return image;
        })
        .catch(function (e) {
          URL.revokeObjectURL(url);
          return Promise.reject(e);
        });
    }

    // ローカルの個別ファイルに保存している場合
    return SandboxFS.openFileEntry(image)
      .then(function (fileEntry) {
        return fileEntry.toURL();
      })
      .then(function (url) {
        return global.GuiParts.getImageBuffer(url, w, h, bg);
      })
      .catch(function (e) {
        if (e.code == 1) {
          // FILE NOT FOUND
          console.error(new Error('FILE_NOT_FOUND: cut path='+url));
          return;
        }

        return Promise.reject(e);
      })
      ;
  }

  /**
   *
   * @param circleId
   * @param data
   * @returns {*}
   */
  function getStoredImageBinary (circleId, data) {
    if (/^data:/.test(data)) {
      // (旧)dataURL形式でDBに登録している場合
      var obj = global.Utils.dataURItoUint8Array(data);
      return Promise.resolve({
        blob: new Blob([obj.uint8a], {type: obj.mimeType}),
        filename: [circleId, obj.mimeType == 'image/jpeg' ? 'JPG' : 'PNG'].join('.')
      });
    }

    return SandboxFS.openFile(data)
      .then(function (file) {
        return Promise.resolve({
          blob: file,
          filename: file.name
        });
      })
      .catch(function (e) {
        if (e.code == 1) {
          // FILE NOT FOUND
          console.error(new Error('FILE_NOT_FOUND: alt cut path='+data));
          return;
        }

        return Promise.reject(e);
      });
  }

  /**
   * 表示用の画像を取得する
   * @param circleId
   * @returns {Promise}
   */
  CutImage.getCutImage = function (circleId) {
    return CutImage.getCutImageEntryMultiple([circleId],true)
      .then(function (images) {
        var img = images[circleId];
        return img.altImage || img.image;
      });
  };

  /**
   *
   * @param imageId
   * @returns {*}
   */
  function findEtcImageEntry (imageId) {
    return etcImageCache[imageId];
  }

  /**
   *
   * @returns {*}
   */
  CutImage.findJikoImageEntry = function () {
    return findEtcImageEntry(JIKO_IMAGE_ID);
  };

  /**
   *
   * @param imageId
   * @returns {*}
   */
  CutImage.findEtcImage = function (imageId) {
    var cache = findEtcImageEntry(imageId);
    return cache && cache.image;
  };

  /**
   * 差し替えカット画像を全部抽出
   * @returns {Promise}
   */
  CutImage.getCutImageAltObjects = function () {

    function openZipBlob () {
      return new Promise(function (resolve, reject) {
        zip.createWriter(new zip.BlobWriter("application/zip"), function(writer) {
          zipWriter = writer;
          resolve();
        }, function (e) {
          reject(e);
        },true);
      });
    }

    function addInitTxt() {
      return new Promise(function (resolve) {
        var text = "cmnum\t"+global.Env.selectedEventId+"\n" +
          "width\t210\n" +
          "height\t300\n";
        var blob = new Blob([text], {type: "text/plain"});
        zipWriter.add('INIT.TXT', new zip.BlobReader(blob), function() {
          resolve();
        });
      });
    }

    function queryAltImages () {
      var query = {
        sql: 'SELECT circleId,altImage FROM DjdatCutImages WHERE djdatNo=? AND altImage IS NOT NULL',
        get params () {
          return [global.Env.selectedEventId];
        }
      };

      return SandboxDB.transactionQuery(query);
    }

    function addAltImages (r) {
      return spawn(function* () {
        for (var i=0; i<r.rows.length; i++) {
          var item = r.rows.item(i);

          var obj = yield getStoredImageBinary(item.circleId, item.altImage);

          if (!obj) {
            continue;
          }

          yield new Promise(function (resolve) {
            zipWriter.add(obj.filename, new zip.BlobReader(obj.blob), function() {
              console.debug('ADDED: alt cut to zip file='+obj.filename);
              resolve();
            });
          });
        }
      });
    }

    function onSuccess () {
      return new Promise(function (resolve) {
        zipWriter.close(function (blob) {
          resolve(blob);
        });
      });
    }

    function onError (e) {
      if (zipWriter) {
        zipWriter.close(function () {
        });
      }

      return Promise.reject(e);
    }

    //

    var zipWriter = null;

    return Promise.resolve()
      .then(openZipBlob)
      .then(addInitTxt)
      .then(queryAltImages)
      .then(addAltImages)
      .then(onSuccess)
      .catch(onError);
  };

  /**
   * 差し替えカット画像をまとめて書き込み
   * @param json
   */
  CutImage.storeCutImageAltObjects = function (file, progress) {

    function openZipReader (reader) {
      return new Promise(function (resolve, reject) {
        zip.createReader(reader, function (r) {
          zipReader = r;
          resolve();
        },function (e) {
          reject(e);
        });
      });
    }

    function getZipEntryAll () {
      return new Promise(function (resolve) {
        zipReader.getEntries(function (entries) {
          resolve(entries);
        });
      });
    }

    function checkEventId (entries) {
      var entry = (function () {
        for (var i=0; i<entries.length; i++) {
          var entry = entries[i];
          if (/^INIT\.TXT$/i.test(entry.filename)) {
            return entry;
          }
        }
      })();

      if (!entry) {
        return Promise.reject(new Error('not found INIT.TXT'));
      }

      return new Promise(function (resolve) {
        return entry.getData(new zip.TextWriter(), function (text) {
          resolve(text);
        });
      })
        .then(function (text) {
          var m = /cmnum\s+(\d+)/.exec(text);
          if (!m || parseInt(m[1],10) != global.Env.selectedEventId) {
            return Promise.reject(new Error('unmatch event id: current='+global.Env.selectedEventId+' ccz='+m[1]));
          }

          return Promise.resolve(entries);
        });
    }

    function storeAltImage (entries) {
      // FIXME ファイルを一つずつ COPY->DB登録 とやろうとすると何故かトランザクションエラーになるのをなんとかしたい
      function copyImages () {
        return spawn(function* () {
          var paths = [];

          for (var i=0; i<entries.length; i++) {
            var entry = entries[i];

            var circleId = (function () {
              var m = /^(\d+)\.(?:JPE?G|PNG)$/i.exec(entry.filename);
              if (!m) {
                return;
              }

              if (!isCutImage(m[1])) {
                return;
              }

              return parseInt(m[1], 10);
            })();

            if (!circleId) {
              continue;
            }

            var path = global.DataOp.RomList.getAltCutImagePath(global.Env.selectedEventId, entry.filename);

            yield new Promise(function (resolve) {
              entry.getData(new zip.BlobWriter(), function (blob) {
                resolve(blob);
              })
            })
              .then(function (blob) {
                return SandboxFS.openFileWriter(path, true)
                  .then(function (fileWriter) {
                    return SandboxFS.writeBlob(fileWriter, blob);
                  })
              })
              .then(function () {
                console.debug('COPIED: alt cut ' + i + ' , path=' + path);

                paths.push({
                  circleId: circleId,
                  path: path
                });
              });
          }

          return paths;
        });
      }

      function storePaths (paths) {
        return SandboxDB.beginTransaction().then(function (r) {
          return spawn(function* () {
            for (var i = 0; i < paths.length; i++) {
              var path = paths[i];

              var query = {
                sql: 'UPDATE DjdatCutImages SET altImage=? WHERE djdatNo=? AND circleId=?',
                params: [path.path, global.Env.selectedEventId, path.circleId],
                onSuccess: function (r) {
                  console.debug('STORED: alt cut ' + i + ' , path=' + path.path);
                  return r;
                }
              };

              yield SandboxDB.executeQuery(r.transaction, query);
            }
          });
        });
      }

      return Promise.resolve()
        .then(copyImages)
        .then(storePaths);
    }

    function onSuccess () {
      zipReader.close(function () {
      });
    }

    function onError (e) {
      if (zipReader) {
        zipReader.close(function () {
        });
      }

      return Promise.reject(e);
    }

    //
    var zipReader = null;

    return Promise.resolve(new zip.BlobReader(file))
      .then(openZipReader)
      .then(getZipEntryAll)
      .then(checkEventId)
      .then(storeAltImage)
      .then(onSuccess)
      .catch(onError);
  };

  // イベント追加

  /**
   * イベントのカット画像情報の追加
   * @param info
   * @param entries
   * @param progress
   * @returns {*}
   */
  CutImage.storeCutImageEntry = function (eventId, entries) {

    return SandboxDB.beginTransaction().then(function (res) {
      return spawn(function* () {
        var pStep = (entries.length - entries.length % 10) / 10;

        for (var i=0; i<entries.length; i++ ) {
          var entry = entries[i];

          var m = entry.filename.match(/^(.+)\.(png|jpe?g)$/i);
          if (!m) {
            continue;
          }

          var obj = {
            id: m[1],
            offset: entry._dataOffset,
            size: entry.compressedSize
          };

          var sql = (function () {
            if (isCutImage(obj.id)) {
              return 'INSERT OR REPLACE INTO DjdatCutImages (djdatNo,circleId,image) VALUES (?,?,?)';
            }

            return 'INSERT OR REPLACE INTO DjdatEtcImages (djdatNo,imageId,image) VALUES (?,?,?)';
          })();

          var query = {
            sql: sql,
            params: [eventId, obj.id, [obj.offset, obj.size].join(',')],
            onSuccess: function (r) {
              return r;
            }
          };

          yield SandboxDB.executeQuery(res.transaction, query);

          if ((1+i) % pStep == 0) {
            console.debug('STORED: cut image progress='+(1+i)+' / '+(1+entries.length));
          }
        }
      });
    });
  };

  /**
   *
   * @param id
   * @returns {boolean}
   */
  function isCutImage (id) {
    return /^\d+$/.test(id) && ! /^0*1$/.test(id);
  }

  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.CutImage = CutImage;

})(DjCRDView);