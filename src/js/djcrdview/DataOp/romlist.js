
(function (global) {

  var RomList = {};

  //---

  const SHIFT_JIS = 'sjis';

  const STORE_STEP_CUT_IMAGES = 100;

  const DONT_OVERWRITE_CUT_IMAGE = 'cutprotected';

  //---

  /**
   *
   * @param eventInfo
   * @param file
   * @returns {*}
   */
  function deocdeDefDat (eventInfo, file) {
    return new Promise(function (resolve, reject) {
        file.getData(new zip.BlobWriter(), function(blob) {

          global.Utils.readAsText(new Blob([blob], {type: 'text/plain'}), SHIFT_JIS).then(function (text) {

            var targetKey;

            text.split(/[\r\n]+/).forEach(function (s) {
              if (s.match(/^\s*$/)) {
                return;
              }

              var m = s.match(/^\*(.+)/);
              if (m) {
                targetKey = m[1].replace(/Comiket/,'Djdat');
                //eventInfo[s] = {};
                return;
              }

              var data = s.split(/\t/);

              if (targetKey === 'Djdat') {
                eventInfo[targetKey] = {
                  id: data[0],
                  name: data[1]
                };
              }
              else if (targetKey === 'cutInfo') {
                eventInfo[targetKey] = {
                  width: data[0],
                  height: data[1],
                  topX: data[2],
                  topY: data[3],
                  offsetX: data[4],
                  offsetY: data[5]
                };
              }
              else if (targetKey === 'mapTableInfo') {
                eventInfo[targetKey] = {
                  width: data[0],
                  height: data[1],
                  topX: data[2],
                  topY: data[3]
                };
              }
              else if (targetKey === 'DjdatDate') {
                eventInfo[targetKey] = eventInfo[targetKey] || [];
                eventInfo[targetKey].push({
                  year: data[0],
                  month: data[1],
                  day: data[2],
                  wday: data[3],
                  startPage: data[4]
                });
              }
              else if (targetKey === 'DjdatMap') {
                eventInfo[targetKey] = eventInfo[targetKey] || [];
                eventInfo[targetKey].push({
                  name: data[0],
                  baseName: data[1].toUpperCase(),
                  x: data[2],
                  y: data[3],
                  width: data[4],
                  height: data[5],
                  outlineBaseName: data[6].toUpperCase(),
                  highX: data[7],
                  highY: data[8],
                  highWidth: data[9],
                  highHeight: data[10],
                  rotate: data[11]
                });
              }
              else if (targetKey === 'DjdatArea') {
                eventInfo[targetKey] = eventInfo[targetKey] || [];
                eventInfo[targetKey].push({
                  areaName: data[0],
                  mapName: data[1],
                  blockNames: data[2],
                  x: data[3],
                  y: data[4],
                  width: data[5],
                  height: data[6],
                  outlineBaseName: data[7].toUpperCase(),
                  highX: data[8],
                  highY: data[9],
                  highWidth: data[10],
                  highHeight: data[11]
                });
              }
              else if (targetKey === 'DjdatGenre') {
                eventInfo[targetKey] = eventInfo[targetKey] || [];
                eventInfo[targetKey].push({
                  code: data[0],
                  name: data[1]
                });
              }
            });

            try {
              console.debug('LOAD days',eventInfo.DjdatDate.length);
              console.debug('LOAD maps',eventInfo.DjdatMap.length);
              console.debug('LOAD areas',eventInfo.DjdatArea.length);
              console.debug('LOAD genres',eventInfo.DjdatGenre.length);
            }
            catch (e) {}

            resolve();
          });
        }, function(current, total) {
          // onprogress callback
        });
      }
    );
  }

  /**
   *
   * @param eventInfo
   * @param file
   * @returns {*}
   */
  function decodeMapDat (eventInfo, file) {
    return new Promise(function (resolve, reject) {
      file.getData(new zip.TextWriter(), function (text) {
          var map = [];
          Papa.parse(
            text,
            {
              delimiter: "\t",
              newline: "",
              header: false,
              dynamicTyping: false,
              preview: 0,
              encoding: "",
              worker: false,
              comments: false,
              step: function (results, parser) {
                results.data.forEach(function (d) {
                  map.push({
                    block: global.Utils.h2z(d[0]),
                    space: d[1],
                    x: d[2],
                    y: d[3],
                    layout: d[4]
                  });
                });
              },
              complete: function (results, file) {
                eventInfo.MapLayout = map;
                resolve();
              },
              error: function (e) {
                reject(e);
              },
              download: false,
              skipEmptyLines: true,
              chunk: undefined
            }
          );
        }
      );
    }, function (current, total) {
      // onprogress callback
    });
  }

  /**
   *
   * @param eventInfo
   * @param file
   * @returns {*}
   */
  function decodeRomDat (eventInfo, file) {
    return new Promise(function (resolve, reject) {
        file.getData(new zip.TextWriter(), function (text) {
          var rom = [];
          Papa.parse(
            text,
            {
              delimiter: "\t",
              newline: "",
              header: false,
              dynamicTyping: false,
              preview: 0,
              encoding: "",
              worker: false,
              comments: false,
              step: function (results, parser) {
                results.data.forEach(function (d) {
                  rom.push({
                    id: d[0],
                    pageNo: d[1],
                    cutIndex: d[2],
                    wday: d[3],
                    hall: d[4],
                    block: global.Utils.h2z(d[5]),
                    spaceNo: d[6],
                    spaceNoSub: (parseInt(d[2],10)+1) % 2,
                    genreCode: d[7],
                    name: d[8],
                    kana: d[9],
                    author: d[10],
                    book: d[11],
                    url: d[12],
                    mail: d[13],
                    desc: d[14],
                    updateData: d[15],
                    circlems: d[16],
                    rss: d[17]
                  });
                });
              },
              complete: function (results, file) {
                eventInfo.CircleList = rom;
                console.debug('LOAD circles',eventInfo.CircleList.length);
                resolve();
              },
              error: function (e) {
                reject(e);
              },
              download: false,
              skipEmptyLines: true,
              chunk: undefined,
              fastMode: true
            }
          );
        }, function (current, total) {
          // onprogress callback
        });
      }
    );
  }

  /**
   *
   * @param eventInfo
   * @param files
   * @returns {Promise.<T>|*}
   */
  function storeMapImages (eventInfo, files) {

    global.GuiParts.ProgressDialog.setMax('配置図データ登録中...', files.length);

    function foo (entry) {
      return new Promise(function (resolve, reject) {
        entry.file.getData(new zip.BlobWriter(entry.mimeType), function (blob) {
          return SandboxFS.openFileWriter(entry.path, true)
            .then(function (fileWriter) {
              fileWriter.onwriteend = function () {
                console.log('map image copied: ' + entry.path);
                resolve({
                  file: {
                    path: entry.path,
                    basename: entry.basename
                  }
                });
              };

              fileWriter.onerror = function (e) {
                reject(e);
              };

              fileWriter.write(blob);
            });
        });
      });
    }

    function onprogress (v) {
      global.GuiParts.ProgressDialog.setValue(v);
    }

    //

    return spawn(function* () {
      var images = [];

      for (var i=0; i<files.length; i++) {
        var file = files[i];

        var m = file.filename.match(/\/(([^/]+)\.(png|jpe?g))$/i);
        if (!m) {
          continue;
        }

        var filename = m[1].toUpperCase();
        var basename = m[2].toUpperCase();
        var ext = m[3].toUpperCase();
        var mimeType = /png/i.exec(ext) ? "image/png" : "image/jpeg";
        var path = RomList.getMapImagePath(eventInfo['Djdat'].id, filename);

        if (/^H/i.test(basename)) {
          console.debug('LOAD map image skips filename=' + filename);
          continue;
        }

        // ファイルを保存
        var image = yield foo({
          file: file,
          path: path,
          basename: basename,
          mimeType: mimeType
        });

        images.push(image);
      }

      // DBに登録
      return global.DataOp.MapImage.storeMapImages(eventInfo, images, onprogress);
    });
  }

  /**
   *
   * @param eventInfo
   * @param file
   * @returns {Promise}
   */
  function storeCutImageCCZ (eventInfo, file) {

    if (eventInfo.dontOverwriteCutImage) {
      return Promise.resolve();
    }

    function decodeAndStoreAsync (reader) {

      return new Promise(function (resolve) {
        reader.getEntries(function (entries) {
          global.GuiParts.ProgressDialog.setMax('カット画像保存中...', entries.length);
          resolve(entries);
        })
      })
        .then(function (entries) {
          return global.DataOp.CutImage.storeCutImageEntry(eventInfo.Djdat.id, entries);
        })
        .then(function () {
          console.log('closed: ' + file.filename);
          reader.close();
        })
        .catch(function (e) {
          reader.close();
          return Promise.reject(e);
        });
    }

    function updateUnzipProgress (v) {
      if (v >= pNext) {
        global.GuiParts.ProgressDialog.setValue(v);
        pNext += pStep;
      }
    }

    var pStep = file.uncompressedSize / 10;
    var pNext = pStep;
    var path = RomList.getCutCCZPath(eventInfo['Djdat'].id, file.filename.toUpperCase());

    global.GuiParts.ProgressDialog.setMax('カット画像アーカイブ抽出中...',file.uncompressedSize);

    return new Promise(function (resolve) {
      // FIXME CCZを全部メモリに読み込んでから処理することになっていてメモリを食うので、読み込みを小分けにしたい
      file.getData(new zip.Uint8Writer(file.uncompressedSize, null), function (uint8a) {
        resolve(uint8a);
      }, updateUnzipProgress);
    })
      .then(function (uint8a) {
        console.log('copy: '+file.filename+' -> '+path);

        return SandboxFS.openFileWriter(path, true)
          .then(function (fileWriter) {
            return SandboxFS.writeUint8Array(fileWriter, uint8a);
          });

      })
      .then(function () {
        console.log('open: '+path);

        return SandboxFS.openFile(path)
          .then(function (file) {
            return zip.createReader(new zip.BlobReader(file), decodeAndStoreAsync);
          });
      });
  }

  /**
   *
   * @param file
   */
  RomList.loadRomZip = function (file, finallyCallback) {

    //
    function warnNotZip (filename) {
      var config = {
        title: '警告',
        content: '選択されたのがzipファイルではありません。',
        message: '(file:' + filename + ')',
        buttons: [
          {label: '閉じる'}
        ]
      };

      return global.GuiParts.DynDialog.show(config);
    }

    //
    function warnInvalidZip (filename) {
      var config = {
        title: '警告',
        content: 'zipファイルの内容に不足があります。',
        message: '(file:' + filename + ')',
        buttons: [
          {label: ' 閉じる'}
        ]
      };

      return global.GuiParts.DynDialog.show(config);
    }

    //
    function warnFailReadZip (filename) {
      var config = {
        title: 'エラー',
        content: 'zipファイルの読み込みに失敗しました。',
        message: '(file:' + filename + ')',
        buttons: [
          {label: '閉じる'}
        ]
      };

      global.GuiParts.DynDialog.show(config);
    }

    //
    function confirmOverwrite (eventInfo) {
      var config = {
        title: '警告',
        content: '既に存在しているイベントIDです。上書きしますか？',
        message: '(ID:' + eventInfo.djdatNo + ',NAME:' + eventInfo.djdatName + ')',
        checks: [
          {label:'カット画像は上書きしない', name:DONT_OVERWRITE_CUT_IMAGE, checked:false}
        ],
        buttons: [
          {label:'上書きする', value: 1},
          {label:'キャンセル'}
        ]
      };

      return global.GuiParts.DynDialog.show(config)
        .then(function (r) {
          if (!r.value) {
            return Promise.reject();
          }

          eventInfo.dontOverwriteCutImage = r.checked[DONT_OVERWRITE_CUT_IMAGE];

          return Promise.resolve();
        });
    }

    //
    function showProgressBar () {

      var config = {
        title: '進捗',
        content: 'イベントを登録しています。',
        message: '開催情報登録中...',
        maxOverall: 4,
        maxValue: 100,
        value: 50
      };

      return global.GuiParts.ProgressDialog.show(config);
    }

    //

    if ( ! file.name.match(/\.zip$/) ) {
      return warnNotZip(file.name);
    }

    zip.createReader(new zip.BlobReader(file), function(reader) {

      console.log('open: '+file.name);

      reader.getEntries(function(entries) {

        var files = {};

        entries.forEach(function (entry) {

          if (entry.filename.match(/^CDATA\/C\d+DEF.TXT$/i)) {
            files['DEF'] = entry;
          }
          else if (entry.filename.match(/^UDATA\/C\d+MAP.TXT$/i)) {
            files['MAP'] = entry;
          }
          else if (entry.filename.match(/^UDATA\/C\d+ROM.TXT$/i)) {
            files['ROM'] = entry;
          }
          else if (entry.filename.match(/^UDATA\/C\d+ROM(?:\d).TXT$/i)) {
            // future use.
            files['OLDROM'] = files['OLDROM'] || [];
            files['OLDROM'].push(entry);
          }
          else if (entry.filename.match(/^MDATA\/.+\.(?:PNG|JPG)$/i)) {
            files['MDATA'] = files['MDATA'] || [];
            files['MDATA'].push(entry);
          }
          else if (entry.filename.match(/^C\d+CUTH.CCZ/i)) {
            files['CUT'] = entry;
          }
          else if (entry.filename.match(/^C\d+CUTL.CCZ/i)) {
            files['CUT'] = files['CUT'] || entry;
          }
        });

        if (['DEF','MAP','ROM','CUT'].some(function (key) { return ! (key in files); })) {
          return warnInvalidZip(file.name);
        }

        //

        var eventInfo = {
          dontOverwriteCutImage: false
        };

        var success = false;

        spawn(function* () {

          yield deocdeDefDat(eventInfo, files['DEF']);

          var event = yield global.DataOp.EventList.findEvent(eventInfo.Djdat.id);
          if (event) {
            // 上書きするか？
            yield confirmOverwrite(event.Event);
          }

          yield showProgressBar();

          yield decodeMapDat(eventInfo,files['MAP']);
          yield decodeRomDat(eventInfo,files['ROM']);

          yield storeEventInfo(eventInfo);

          yield createEventFolder(eventInfo);

          yield storeMapImages(eventInfo, files['MDATA']);

          yield storeCutImageCCZ(eventInfo, files['CUT']);

          success = true;
        })
          .catch(function (e) {
            if (e) {
              console.error(e.stack || e);
              return;
            }

            console.log('cancelled')
          })
          .then(function (r) {
            // finally
            global.GuiParts.ProgressDialog.close();
            reader.close(function () {
              console.log('closed: '+file.name);
              if (success && finallyCallback)
                finallyCallback(eventInfo.Djdat.id);
            });
          });
      });
    }, function(e) {
      // onerror callback
      if (e)
        console.error(e.stack || e);

      warnFailReadZip(filename);
    });
  };

  /**
   *
   * @param eventInfo
   */
  function storeEventInfo (eventInfo) {
    return global.DataOp.EventList.storeEvent(eventInfo);
  }

  /**
   *
   * @param eventInfo
   * @returns {*}
   */
  function createEventFolder (eventInfo) {
    return spawn(function* () {
      eventInfo['Files'] = {
        foldername: RomList.getImageRoot(eventInfo['Djdat'].id),
        mapfolder: RomList.getMapImagePath(eventInfo['Djdat'].id),
        altcutfolder: RomList.getAltCutImagePath(eventInfo['Djdat'].id)
      };

      var keys = Object.keys(eventInfo['Files']);
      console.log('create folder: '+keys.map(function (e) {return eventInfo['Files'][e];}).join(' , '));

      for (var i=0; i<keys.length; i++) {
        yield SandboxFS.createFolder(eventInfo['Files'][keys[i]]);
      }
    });
  }

  /**
   *
   * @param id
   * @param name
   * @param callback
   */
  RomList.removeRom = function (id,name) {

    function confirmRemoveZip (id,name) {
      var config = {
        title: '警告',
        content: 'イベントを削除しますか？',
        message: '(ID:' + id + ',NAME:' + name + ')',
        buttons: [
          {label: 'はい', value: 1},
          {label: 'いいえ'}
        ]
      };

      return global.GuiParts.DynDialog.show(config)
        .then(function (r) {
          if (!r.value) {
            // キャンセル
            return Promise.reject();
          }

          return Promise.resolve();
        });
    }

    function openProgressDialog () {
      var config = {
        title: '進捗',
        message: 'イベント削除中...',
        maxOverall: 1,
        maxValue: 9,
        value: 0
      };

      return global.GuiParts.ProgressDialog.show(config);
    }

    function progress (v) {
      global.GuiParts.ProgressDialog.setValue(v);
    }

    //

    return Promise.resolve()
      .then(function () {
        return confirmRemoveZip(id,name);
      })
      .then(function () {
        return openProgressDialog();
      })
      .then(function () {
        // FIXME
        return SandboxFS.removeFolder(RomList.getImageRoot(id));
      })
      .then(function () {
        return global.DataOp.EventList.removeEvent(id, false, progress);
      })
      .then(function () {
        // finally
        global.GuiParts.ProgressDialog.close();
      });

  };


  RomList.getImageRoot = function (eventId) {
    return 'CM'+eventId;
  };

  RomList.getMapImagePath = function (eventId, fileName) {
    return [RomList.getImageRoot(eventId), 'MDATA', fileName].filter(function (e) {return !!e;}).join('/');
  };

  RomList.getAltCutImagePath = function (eventId, fileName) {
    return [RomList.getImageRoot(eventId), 'ADATA', fileName].filter(function (e) {return !!e;}).join('/');
  };

  RomList.getCutCCZPath = function (eventId, fileName) {
    //return [RomList.getImageRoot(eventId), fileName].filter(function (e) {return !!e;}).join('/');
    return [RomList.getImageRoot(eventId), 'C'+eventId+'CUTX.CCZ'].filter(function (e) {return !!e;}).join('/');
  };

  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.RomList = RomList;

})(DjCRDView);