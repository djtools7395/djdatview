(function (global) {

  var Layout = {};

  // キャッシュ
  var layoutCache = {};

  /**
   * キャッシュのクリア
   */
  Layout.clearCache = function () {

    layoutCache = {};

    var query = {
      sql: 'SELECT blockId,spaceNo,xpos,ypos,layout,mapId FROM DjdatLayout WHERE djdatNo=?',
      get params () {
        return [global.Env.selectedEventId];
      },
      onSuccess: function (r) {
        if (r.rows.length == 0) {
          // スキップ
          console.debug('CACHE no layout table eventId='+global.Env.selectedEventId);
          return null;
        }

        for (var i=0; i<r.rows.length; i++) {
          var item = r.rows.item(i);

          var obj = {};
          for (var p in item) {
            obj[p] = item[p];
          }

          layoutCache[getIndex(obj.blockId,obj.spaceNo)] = obj;
        }

        console.debug('CACHE layout table length='+Object.keys(layoutCache).length);

        return layoutCache;
      },
      onError: function (e) {
        return e;
      }
    };

    return SandboxDB.transactionQuery(query);
  };

  //---

  /**
   * レイアウト情報キャッシュのインデックス値取得
   * @param blockId
   * @param spaceNo
   * @returns {string}
   */
  function getIndex (blockId,spaceNo) {
    return [blockId,spaceNo].join('/');
  }

  /**
   * レイアウト情報個別取得
   * @param blockId
   * @param spaceNo
   * @returns {*}
   */
  Layout.findLayout = function (blockId,spaceNo) {
    return layoutCache[getIndex(blockId,spaceNo)];
  };

  /**
   * レイアウト情報全取得
   * @returns {{}}
   */
  Layout.getLayoutsAll = function () {
    return layoutCache;
  };

  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.Layout = Layout;

})(DjCRDView);
