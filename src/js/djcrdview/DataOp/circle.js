// FIXME fetchCircleInfoMultiple は100くらいずつ小分けにやったほうがよいか？

(function (global) {

  var Circle = {};

  // キャッシュ
  var circleCache = new LRUCache(global.Env.circleCacheSize > 0 ? global.Env.circleCacheSize : 120);

  var spaceCache = new LRUCache(global.Env.circleCacheSize > 0 ? global.Env.circleCacheSize : 120);

  var circlesInBlockCache = new LRUCache(global.Env.circlesInBlockCacheSize > 0 ? global.Env.circlesInBlockCacheSize : 10);

  /**
   * キャッシュのクリア
   */
  Circle.clearCache = function () {
    circleCache.removeAll();
    spaceCache.removeAll();
    circlesInBlockCache.removeAll();

    console.debug('CACHE circle cleared');

    return Promise.resolve();
  };

  //---

  // DjdatCircleのメンバ名の変換ルール
  const MEMBER_NAME_MAP = {
    'circleName': 'name',
    'circleKana': 'kana',
    'penName': 'author',
    'day': 'dateId'
  };

  // 更新系

  /**
   *
   * @param circleId
   * @param url
   * @returns {*}
   */
  Circle.addExtraUrl = function (url) {

    if (!url || /\.(?:jpe?g|png|gif|bmp)/.test(url)) {
      console.error('cannot add image', url);
      return Promise.reject();
    }

    var circleId = global.Env.selectedCircleId;

    var colNames = ['url','circlems','rss','extraUrl1','extraUrl2','extraUrl3'];
    var extraColNames = ['extraUrl1', 'extraUrl2', 'extraUrl3'];

    return Circle.getCircle(circleId).then(function (obj) {

      if (!obj) {
        return Promise.reject();
      }

      var item = obj.Circle;

      // 同じのがあるなら何もしない
      if (colNames.some(function (col) {return item[col] && item[col] === url})) {
        console.debug('already exists',url);
        return Promise.reject();
      }

      var col = (function () {

        // 同じドメインなら置き換える
        var repCol = extraColNames.filter(function (col) {
          if (!item[col]) {
            return;
          }

          var a = item[col].replace(/^(http)s?(:\/\/[^/]+?\/).*$/, '$1$2');
          var b = url.replace(/^(http)s?(:\/\/[^/]+?\/).*$/, '$1$2');
          if (a == b) {
            return true;
          }
        })[0];
        if (repCol) {
          console.debug('replace', repCol, item[repCol], ' -> ', url);
          return repCol;
        }

        // そうでないなら空いているところを探す
        var empCol = extraColNames.filter(function (col) {return !item[col]})[0];
        if (empCol) {
          console.debug('add', empCol, url);
          return empCol;
        }

        return;
      })();

      // FIXME extraUrl1,2,3の編集
      if (!col) {
        console.debug('no empty column', url);
        return Promise.reject();
      }

      // 更新
      var query = {
        sql: 'UPDATE DjdatCircle SET '+col+'=? WHERE djdatNo=? AND circleId=?',
        get params () {
          return [url, global.Env.selectedEventId, circleId];
        },
        onSuccess: function (r) {
          // キャッシュの更新
          var cache = circleCache.get(circleId);
          if (cache) {
            cache.Circle[col] = url;
          }

          // お気に入り情報キャッシュのリフレッシュ
          global.DataOp.FavCircle.refreshFavCircleCache(circleId);

          return {circleId:circleId, column:col, url:url};
        },
        onError: function (e) {
          return e;
        }
      };

      return SandboxDB.transactionQuery(query);
    });
  };

  /**
   * 差し替えURLを全部抽出
   */
  Circle.getExtraUrlObjects = function () {
    var query = {
      sql: 'SELECT circleId,url,circlems,rss,extraUrl1,extraUrl2,extraUrl3 FROM DjdatCircle WHERE djdatNo=? AND extraUrl1 IS NOT NULL',
      params: [global.Env.selectedEventId],
      onSuccess: function (r) {
        var obj = {};
        obj.fileId = global.Const.FileId.Urls;
        obj.version = 2;
        obj.eventId = global.Env.selectedEventId;
        obj.extraurl = {};
        for (var i = 0; i < r.rows.length; i++) {
          var item = r.rows.item(i);
          var a = [item.extraUrl1, item.extraUrl2, item.extraUrl3].filter(function (v) { return !!v; });
          if (a.length > 0) {
            obj.extraurl[r.rows.item(i).circleId] = {
              e: a,
              u: [item.url, item.circlems, item.rss].filter(function (v) { return v && /^https?:\/\//.test(v); })
            };
          }
        }

        return obj;
      },
      onError: function (e) {
        return e;
      }
    };

    return SandboxDB.transactionQuery(query);
  };

  /**
   * 差し替えURLをまとめて書き込み
   * @param json
   */
  Circle.storeExtraUrlObjects = function (text, progress) {

    //
    function removeExtraUrlAll (r) {
      var query = {
        sql: 'UPDATE DjdatCircle SET extraUrl1=NULL,extraUrl2=NULL,extraUrl3=NULL WHERE djdatNo=? AND extraUrl1 IS NOT NULL',
        params: [global.Env.selectedEventId],
        onSuccess: function (r) {
          return r;
        },
        onError: function (e) {
          return e;
        }
      };

      return SandboxDB.executeQuery(r.transaction, query);
    }

    //
    function storeExtraUrlAll (r) {
      return spawn(function* () {

        var keys = Object.keys(json.extraurl).filter(function (circleId) {return !isNaN(circleId)});

        for (var i=0; i<keys.length; i++) {
          var circleId = keys[i];

          var query = {
            sql: 'UPDATE DjdatCircle SET extraUrl1=?,extraUrl2=?,extraUrl3=? WHERE djdatNo=? AND circleId=?',
            get params () {
              var p = ['', '', '', global.Env.selectedEventId, circleId];
              var l = (function (ver) {
                if (ver == 2) {
                  return json.extraurl[circleId].e;
                }
                return json.extraurl[circleId];
              })(json.version);
              for (var i=0; i<l.length && i<3; i++) {
                p[i] = l[i];
              }
              return p;
            },
            onSuccess: function (r) {
              progress({value: i+1});
              return r;
            },
            onError: function (e) {
              return e;
            }
          };

          yield SandboxDB.executeQuery(r.transaction, query);
        }
      });
    }

    //
    function updateExtraUrlAll (r) {
      function queryExtraUrl (urls) {
        return spawn(function* () {
          for (var i=0; i<urls.length; i++) {
            var url = urls[i];

            var tx = yield global.DataOp.Circle.executeMatchURLQuery(r.transaction, url);
            if (tx) {
              return tx;
            }
          }
        });
      }

      function storeExtraUrl (s, urls) {
        if (s.result == null) {
          return;
        }

        // FIXME 最初の一つだけ更新
        var item = s.result.rows[0];
        var a = [item.url, item.circlems, item.rss, item.extraUrl1, item.extraUrl2, item.extraUrl3].filter(function (v) { return !!v && /^https?:\/\//.test(v); });
        var l = global.Utils.getNoOverlaps(a, urls);
        if (l.length == 0) {
          return;
        }
        var c = [item.extraUrl1, item.extraUrl2, item.extraUrl3];

        var query = {
          sql: 'UPDATE DjdatCircle SET extraUrl1=?,extraUrl2=?,extraUrl3=? WHERE djdatNo=? AND circleId=?',
          get params () {
            var p = ['', '', '', global.Env.selectedEventId, item.circleId];
            for (var i=0; i<l.length && i<3; i++) {
              p[i] = c[i] || l[i];
            }
            console.debug('updated extra url:', item.circleId, p[0], p[1], p[2]);
            return p;
          },
          onSuccess: function (r) {
            return r;
          },
          onError: function (e) {
            return e;
          }
        };

        return SandboxDB.executeQuery(r.transaction, query);
      }

      //

      return spawn(function* () {
        var keys = Object.keys(json.extraurl).filter(function (circleId) {return !isNaN(circleId)});
        for (var i=0; i<keys.length; i++) {
          var circleId = keys[i];

          var urls = (function (ver) {
            if (ver == 2) {
              return json.extraurl[circleId].u.concat(json.extraurl[circleId].e);
            }
            return json.extraurl[circleId];
          })(json.version);

          urls = global.Utils.uniqueArray(urls);

          var s = yield queryExtraUrl(urls);

          yield storeExtraUrl(s, urls);
        }

      });
    }

    //

    try {
      var json = JSON.parse(text);
    }
    catch (e) {
      return Promise.reject(e);
    }

    if (json.fileId != global.Const.FileId.Urls) {
      // 違う種類のファイルだ
      return Promise.reject(new Error('unmatch file type fileId=' + json.fileId + ', currentId=' + global.Const.FileId.Urls));
    }

    if (json.eventId == global.Env.selectedEventId) {
      return SandboxDB.beginTransaction()
        .then(removeExtraUrlAll)
        .then(storeExtraUrlAll);
    }
    else {
      // 違うイベントのファイルだ
      //return Promise.reject(new Error('unmatch event eventId=' + json.eventId + ', currentId=' + global.Env.selectedEventId));
      console.log('unmatch event eventId=' + json.eventId + ', currentId=' + global.Env.selectedEventId);
      return SandboxDB.beginTransaction()
        .then(updateExtraUrlAll);
    }

  };

  /**
   *
   * @param item
   * @returns {{}}
   */
  Circle.getCircleObject = function (item) {
    var obj = {};
    for (var p in item) {
      obj[MEMBER_NAME_MAP[p] || p] = item[p];
    }
    obj.Pubs = [];
    return obj;
  };

  /**
   *
   * @param item
   * @returns {{}}
   */
  Circle.getPubInfoOjbect = function (item) {
    var obj = {};
    for (var p in item) {
      if (p === 'djdatNo') {
        continue;
      }
      if (p === 'urls' || p === 'shops') {
        if (item[p]) {
          try {
            var v = JSON.parse(item[p]);
            obj[p] = (Array.isArray(v) ? v : [v]).map(function (v) {
              if (p === 'urls') {
                return global.Utils.expandAnnounceLink(v);
              }
              else if (p === 'shops') {
                return global.Utils.expandShopLink(v);
              }
              return v;
            });
            continue;
          }
          catch (e) {}
        }
        obj[p] = [];
      }
      else {
        obj[p] = item[p];
      }
    }
    return obj;
  };

  /**
   *
   * @param eventId
   * @param circleId
   * @param pubs
   * @returns {{sql: string, params: Array.<*>}}
   */
  function getPubInfoStoreQuery (eventId, circleId, pubs) {
    var pKeys = Object.keys(pubs).filter(function (p) {
      return ! (p === 'djdatNo' || p === 'circleId');
    });

    var pValues = pKeys.map(function (p) {
      if (p === 'urls') {
        return JSON.stringify(pubs[p].map(function (v) {
          return global.Utils.contractAnnounceLink(v);
        }));
      }
      else if (p === 'shops') {
        return JSON.stringify(pubs[p].map(function (v) {
          return global.Utils.contractShopLink(v);
        }));
      }
      else {
        return pubs[p];
      }
    });

    var insertKeys = ['djdatNo', 'circleId'].concat(pKeys);

    // SQLのバインド変数分'?'を作る
    var bindKeys = insertKeys.join(',');
    var binds = insertKeys.map(function () {return '?'}).join(',');

    var query = {
      sql: 'INSERT OR REPLACE INTO DjdatPublication ('+bindKeys+') VALUES ('+binds+')',
      params:  [eventId, circleId].concat(pValues)
    };

    return query;
  }


  /**
   *
   * @type {copyObject}
   */
  Circle.getLayoutInfoObject = copyObject;

  /**
   *
   * @param item
   * @returns {{}}
   */
  function copyObject (item) {
    var obj = {};
    for (var p in item) {
      obj[p] = item[p];
    }
    return obj;
  }

  /**
   *
   * @param pubs
   * @returns {Promise}
   */
  Circle.insertOrReplacePubsInfo = function (pubs) {

    var circleId = global.Env.selectedCircleId;
    var query = getPubInfoStoreQuery(global.Env.selectedEventId, circleId, pubs);

    return SandboxDB.transactionQuery(query)
      .then(function () {
        var cache = circleCache.get(circleId);
        var cache = cache && cache.Circle;
        if (cache) {
          function foo (p,index) {
            if (p.id == pubs.id) {
              cache.Pubs[index] = pubs;
              return true;
            }
          }

          cache.Pubs = cache.Pubs || [];
          if (!cache.Pubs.some(foo)) {
            cache.Pubs.push(pubs);
          }
        }

        // お気に入り情報キャッシュのリフレッシュ
        global.DataOp.FavCircle.refreshFavCircleCache(circleId);
      });
  };

  /**
   *
   * @param pubs
   * @returns {Promise}
   */
  Circle.removePubsInfo = function (pubsId) {

    var circleId = global.Env.selectedCircleId;

    var query = {
      sql: 'DELETE FROM DjdatPublication WHERE djdatNo=? AND circleId=? AND id=?',
      params:  [global.Env.selectedEventId, circleId, pubsId],
      onSuccess: function (r) {
        // キャッシュの更新
        var cache = circleCache.get(circleId);
        if (cache && cache.Pubs) {
          cache.Pubs = cache.Pubs.filter(function (p) {
            return p.id != pubsId;
          });
        }

        // お気に入り情報キャッシュのリフレッシュ
        global.DataOp.FavCircle.refreshFavCircleCache(circleId);

        return true;
      },
      onError: function (e) {
        return e;
      }
    };

    return SandboxDB.transactionQuery(query);
  };

  // 参照系

  // サークル情報を検索する
  function fetchCircleInfo (transaction, result, circleId) {
    var query = {
      sql: 'SELECT rowid as rid,* FROM DjdatCircle WHERE djdatNo=? AND circleId=?',
      get params () {
        return [global.Env.selectedEventId, circleId]
      },
      onSuccess: function (r) {
        if (r.rows.length == 0) {
          console.error(new Error('not found circle info: circleId='+circleId));
          return;
        }

        result.Circle = Circle.getCircleObject(r.rows.item(0));

        return result;
      },
      onError: function (e) {
        return e;
      }
    };

    return SandboxDB.executeQuery(transaction, query);
  }

  // レイアウト情報を検索する
  function fetchLayoutInfo (transaction, result) {
    var query = {
      sql: 'SELECT xpos,ypos,layout,mapId FROM DjdatLayout WHERE djdatNo=? AND blockID=? AND spaceNo=?',
      get params () {
        return [global.Env.selectedEventId, result.Circle.blockId, result.Circle.spaceNo]
      },
      onSuccess: function (r) {
        if (r.rows.length == 0) {
          console.error(new Error('not found layout info: blockId='+result.Circle.blockId+' spaceNo='+result.Circle.spaceNo));
          return;
        }

        var obj = Circle.getLayoutInfoObject(r.rows.item(0));
        for (var p in obj) {
          result.Circle[p] = obj[p];
        }

        return result;
      },
      onError: function (e) {
        return e;
      }
    };

    return SandboxDB.executeQuery(transaction, query);
  }

  // 発行物情報を検索する
  function fetchPubsInfo (transaction, result, circleId) {
    var query = {
      sql: 'SELECT * FROM DjdatPublication WHERE djdatNo=? AND circleId=? ORDER BY id',
      get params () {
        return [global.Env.selectedEventId, circleId]
      },
      onSuccess: function (r) {

        result.Pubs = [];

        for (var i=0; i<r.rows.length; i++) {
          var obj = Circle.getPubInfoOjbect(r.rows.item(i));
          result.Pubs.push(obj);
        }

        return result;
      },
      onError: function (e) {
        return e;
      }
    };

    return SandboxDB.executeQuery(transaction, query);
  }


  /**
   * キャッシュ情報のサークル情報を検索する（同期）
   * @param circleId
   * @returns {*}
   */
  Circle.findCircle = function (circleId) {
    return circleCache.get(circleId);
  };

  /**
   * サークル情報を検索する（非同期）
   * @param circleId
   * @returns {*}
   */
  Circle.getCircle = function (circleId) {

    // キャッシュ検索
    var cache = Circle.findCircle(circleId);
    if (cache) {
      // キャッシュにヒット
      return Promise.resolve(cache);
    }

    // DB検索
    return SandboxDB.beginTransaction()
      .then(function (res) {
        return Circle.fetchCircle(circleId,res);
      })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      });
  };

  /**
   * FavCircleからも呼び出すので
   * @param circleId
   * @param res
   * @returns {*}
   */
  Circle.fetchCircle = function (circleId, res) {

    return Promise.resolve()
      .then(function () {
        return fetchCircleInfo(res.transaction, res.result, circleId);
      })
      .then(function (res) {
        return fetchLayoutInfo(res.transaction, res.result);
      })
      .then(function () {
        return fetchPubsInfo(res.transaction, {}, circleId)
          .then(function (cond) {
            res.result.Circle.Pubs = cond.result.Pubs;
            return Promise.resolve(res);
          });
      })
      .then(function (res) {
        // キャッシュに保存
        circleCache.put(circleId, res.result);
        return res.result;
      });
  };

  /**
   * スペース上にあるサークル情報の取得
   * @param blockId
   * @param spaceNo
   * @param spaceNoSub
   * @returns {*}
   */
  Circle.getCircleInSpace = function (blockId,spaceNo,spaceNoSub) {

    function getSpaceCacheIndex (blockId,spaceNo,spaceNoSub) {
      return [blockId,spaceNo,spaceNoSub].join("/");
    }

    var query = {
      sql: 'SELECT circleId FROM DjdatCircle WHERE djdatNo=? AND day=? AND blockId=? AND spaceNo=? AND spaceNoSub=?',
      get params () {
        return [global.Env.selectedEventId,global.Env.selectedDateId,blockId,spaceNo,spaceNoSub];
      },
      onSuccess: function (r) {
        if (r.rows.length == 0) {
          // reject
          return;
        }

        return r.rows.item(0).circleId;
      },
      onError: function (e) {
        return e;
      }
    };

    //---

    var index = getSpaceCacheIndex(blockId,spaceNo,spaceNoSub);

    return Promise.resolve().then(function () {
      var cache = spaceCache.get(index);
      if (cache) {
        return Promise.resolve(cache);
      }

      return SandboxDB.transactionQuery(query);

    }).then(function (circleId) {

      // キャッシュに保存
      spaceCache.put(index,circleId);

      return Circle.getCircle(circleId);
    });
  };

  /**
   * ブロック内のサークルの列挙
   * @param blockId
   * @returns {*}
   */
  Circle.getCirclesInBlock = function (dateId,blockId) {

    dateId = dateId ? dateId :global.Env.selectedDateId;
    blockId = blockId ? blockId :global.Env.selectedBlockId;

    var query = {
      sql: 'SELECT circleId,spaceNo,spaceNoSub FROM DjdatCircle WHERE djdatNo=? AND day=? AND blockId=? ORDER BY circleId',
      get params () {
        return [global.Env.selectedEventId,dateId,blockId];
      },
      onSuccess: function (r) {
        var result = new Array(r.rows.length);
        for (var i=0; i < r.rows.length; i++) {
          var item = r.rows.item(i);
          result[i] ={
            circleId: item.circleId,
            spaceNo: item.spaceNo,
            spaceNoSub: item.spaceNoSub
          };
        }

        // キャッシュに保存
        circlesInBlockCache.put(getBlockIndex(dateId,blockId), result);

        return result;
      },
      onError: function (e) {
        return e;
      }
    };

    var cache = circlesInBlockCache.get(getBlockIndex(dateId,blockId));
    if (cache) {
      return Promise.resolve(cache);
    }

    return SandboxDB.transactionQuery(query);
  };

  function getBlockIndex (dateId,blockId) {
    return [dateId,blockId].join("/");
  }

  // 検索系

  /**
   * URLでサークルリストを検索するクエリを実行
   * @param t
   * @param url
   * @returns {Promise}
   */
  Circle.executeMatchURLQuery = function (t,url) {
    // ２スぺならLIMIT=2でいいが複数日参加も考えてLIMIT=4で
    var query = {
      sql: 'SELECT rowid as rid,* FROM DjdatCircle WHERE djdatNo=? AND (url=? OR circlems=? OR rss=? OR extraUrl1=? OR extraUrl2=? OR extraUrl3=?) ORDER BY circleId LIMIT 4',
      get params () {
        return [global.Env.selectedEventId,url,url,url,url,url,url];
      },
      onSuccess: function (r) {
        if (r.rows.length == 0) {
          return null;
        }

        var rows = [];
        for (var i=0; i<r.rows.length; i++) {
          var obj = Circle.getCircleObject(r.rows.item(i));
          rows.push(obj);
        }

        return {rows:rows, url:url};
      },
      onError: function (e) {
        return e;
      }
    };

    return SandboxDB.executeQuery(t, query);
  };

  /**
   * サークル名と作家名でサークルリストを検索するクエリを実行
   * @param t
   * @param key
   * @returns {Promise}
   */
  Circle.executeMatchNameQuery = function (t,key) {

    var sql = (function () {
      if (!key.author)
        return 'SELECT rowid as rid,* FROM DjdatCircle WHERE djdatNo=? AND (circleNameNrm LIKE ?)';
      if (!key.name)
        return 'SELECT rowid as rid,* FROM DjdatCircle WHERE djdatNo=? AND (penNameNrm LIKE ?)';

      return 'SELECT rowid as rid,* FROM DjdatCircle WHERE djdatNo=? AND (circleNameNrm LIKE ? OR penNameNrm LIKE ?)';
    })();

    var params = (function () {
      if (!key.author)
        return [global.Env.selectedEventId, key.name];
      if (!key.name)
        return [global.Env.selectedEventId, key.author];

      return [global.Env.selectedEventId, key.name, key.author];
    })();

    var query = {
      sql: sql,
      params: params,
      onSuccess: function (r) {
        if (r.rows.length == 0) {
          return null;
        }

        var rows = [];
        for (var i = 0; i < r.rows.length; i++) {
          var obj = Circle.getCircleObject(r.rows.item(i));
          rows.push(obj);
        }

        return {rows:rows, key:key};
      }
    };

    return SandboxDB.executeQuery(t, query);
  };

  //---

  Circle.loadPubsInfo = function (file) {

    if ( ! global.DataOp.EventList.Selected.Event.djdatNo) {
      // 選択中のイベントがないのでキャンセル
      console.error(new Error('no event selected'));
      return Promise.reject();
    }

    return Promise.resolve()
      .then(function () {
        return global.Utils.readAsText(file);
      })
      .then(function (text) {
        var json = JSON.parse(text);
        // FIXME xxx
        if (json.fileId != global.Const.FileId.Pubs || json.eventId != global.DataOp.EventList.Selected.Event.djdatNo) {
          return Promise.reject();
        }

        return Promise.resolve()
          .then(function () {
            // 古い頒布物情報を削除する
            var query = {
              sql: 'DELETE FROM DjdatPublication WHERE djdatNo=?',
              params: [global.DataOp.EventList.Selected.Event.djdatNo]
            };

            return SandboxDB.transactionQuery(query);
          })
          .then(function () {
            // 新しい頒布物情報を登録する
            return SandboxDB.beginTransaction().then(function (res) {
              return spawn(function* () {
                for (var i=0; i<json.pubsinfo.length; i++) {
                  var pubs = json.pubsinfo[i];

                  var query = getPubInfoStoreQuery(global.Env.selectedEventId, pubs.circleId, pubs);

                  yield SandboxDB.executeQuery(res.transaction, query);
                }
              });
            });
          });
      })
      .then(function () {
        return console.log('done');
      })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      });

  };

  Circle.getPubsInfoAll = function () {
    var query = {
      sql: 'SELECT * FROM DjdatPublication WHERE djdatNo=?',
      get params () {
        return [global.Env.selectedEventId];
      },
      onSuccess: function (r) {
        if (r.rows.length == 0) {
          // 頒布物情報がないのでなにもしない
          console.debug('no pubs info', global.Env.selectedEventId);
          return;
        }

        var cache = [];
        for (var i=0; i< r.rows.length; i++) {
          var obj = Circle.getPubInfoOjbect(r.rows.item(i));
          cache.push(obj);
        }

        return cache;
      },
      onError: function (e) {
        return e;
      }
    };

    return SandboxDB.transactionQuery(query);
  };

  //---

  global.DataOp = global.DataOp || {};

  global.DataOp.Circle = Circle;

})(DjCRDView);