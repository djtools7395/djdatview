(function (global) {

  var Const = {

    // お気に入り配色の既定値
    FavColors: ['ff944a','ff00ff','fff700','00b54a','00b5ff','9c529c','0000ff','00ff00','ff0000'],

    // お気に入りのソート順
    FavSortType : [
      {name:'配置順'},
      {name:'色順'},
      {name:'情況順'}
    ],

    // 状況の既定値
    Conditions: [
      {label:'未', enabled:1, alarm:0},
      {label:'済', enabled:1, alarm:0},
      {label:'完売', enabled:1, alarm:0},
      {label:'欠席', enabled:1, alarm:0},
      {label:'遅刻', enabled:1, alarm:1},
      {label:'時限', enabled:1, alarm:1},
      {label:'再訪', enabled:1, alarm:1},
      {label:'取置', enabled:1, alarm:0},
      {label:'人まかせ', enabled:1, alarm:0},
      {label:'書店購入', enabled:1, alarm:0},
      {label:'補填予定', enabled:1, alarm:0},
      {label:'新刊なし', enabled:1, alarm:0}
    ],

    // サークルの属性
    Clan: {
      'group': ['マンガ','イラスト','小説','評論','音楽','ＣＧ','ゲーム','グッズ','その他'],
      'assort': ['assort-1','assort-2','assort-3','assort-4','assort-5','assort-6','assort-7','assort-8','その他']
    },

    // 発行物の属性
    Distribution: {
      'class': ['新刊', '既刊', 'おまけ', 'プレビュー版', '体験版', '委託新刊', '委託既刊'],
      'category': ['個人誌', '合同誌', '改訂版', '総集編', 'ゲスト参加'],
      'type': ['オフセ', '折り綴じ本', 'コピー誌', 'チラシ', 'グッズ', 'ゲーム', '音楽', 'ＣＧ集'],
      'method': ['書店委託あり', '会場限定', '後日委託', '委託未定', 'ＤＬ販売予定', 'ネット公開予定'],
      'terms': ['通常頒布', 'セット頒布', '条件付き頒布', 'プレビュー版と交換']
    },

    // FILE ID
    FileId: {
      'Urls': 'extraurl',
      'Cuts': 'altcutimage',
      'Pubs': 'pubsinfo',
      'Hist': 'history',
      'Search': 'searchresult'
    },

    FsQuota: 200*1024*1024
  };

  //---

  global.Const = Const;

})(DjCRDView);
