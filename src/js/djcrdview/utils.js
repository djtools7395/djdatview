
(function (global) {

  var Utils = {};

  //---

  /**
   *
   * @param reader
   * @param blob
   * @param charset
   * @returns {Promise}
   */
  Utils.readAsText = function (blob, charset) {
    return new Promise(function (resolve,reject) {
      var reader = new FileReader();

      reader.onload = function (event) {
        resolve(event.target.result);
      };

      reader.onerror = function (event) {
        reject(event.target.error);
      };

      reader.readAsText(blob, charset);
    });
  };

  /**
   * オブジェクトをJSONテキスト形式でファイルに保存する
   * @param filename
   * @param obj
   */
  Utils.saveAsJson = function (filename, obj) {
    var data = JSON.stringify(obj);
    var blob = new Blob([data], {type: 'text/json'});
    Utils.saveAsText(filename,URL.createObjectURL(blob));
  };

  /**
   * テキストをファイルに保存する
   * @param filename
   * @param text
   */
  Utils.saveAsText = function (filename, text) {
    var link = document.createElement('a');
    link.download = filename;
    link.href = text;
    link.click();
  };

  /**
   *
   * @param s
   * @returns {*}
   */
  Utils.h2z = function (s) {
    return s.replace(/[\!-\~]/g, function(s) {
      return String.fromCharCode(s.charCodeAt(0) + 0xFEE0);
    });
  };

  /**
   *
   * @param s
   * @returns {*}
   */
  Utils.z2h = function (s) {
    return s.replace(/[！-～]/g, function(s) {
      return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
    });
  };

  const Z2HMAP = (function () {
    const Z =
      "　！”＃＄％＆’（）＊＋，－．／：；＜＝＞？＠［￥］＾＿‘｛｜｝￣０１２３４５６７８９ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚabcdefghijklmnopqrstuvwxyz"+
      "アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワヲンヴガギグゲゴザジズゼゾダヂヅデドバビブベボパピプペポァィゥェォッャュョあいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをんゔがぎぐげござじずぜぞだぢづでどばびぶべぼぱぴぷぺぽぁぃぅぇぉっゃゅょｧｨｩｪｫｯｬｭｮ" +
      "“×－ー～〜『』◇□☆○○" +
      "壷胄㈱㈲";

    const H =
      " !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ" +
      "ｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜｦﾝｳｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾊﾋﾌﾍﾎﾊﾋﾌﾍﾎｱｲｳｴｵﾂﾔﾕﾖｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜｦﾝｳｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾊﾋﾌﾍﾎﾊﾋﾌﾍﾎｱｲｳｴｵﾂﾔﾕﾖｱｲｳｴｵﾂﾔﾕﾖ" +
      "\"x----「」◆■★●◎" +
      "壺冑株有";

    var map = {};
    for (var i=0; i<Z.length; i++) {
      map[Z.slice(i,i+1)] = H.slice(i,i+1);
    }

    return map;
  })();

  /**
   *
   * @param s
   * @returns {string}
   */
  Utils.strNormalize = function (s) {
    return s.split("").map(function (c) {
      return Z2HMAP[c] || c;
    }).join('').replace(/\s/g,'');
  };

  Utils.zeroPad = function (n,d) {
    var s = parseInt(n,10).toString();
    if (s.length >= d)
      return s;
    s = '00000000'+s;
    return s.slice(-d);
  };

  /**
   *
   * @param f
   * @returns {string}
   */
  Utils.hereDoc = function (f) {
    return f.toString().replace(/^[\s\S]*?\/\*([\s\S]+)\*\/[\s\S]*$/, '$1');
  };

  /**
   *
   * @param s
   * @returns {Number}
   */
  Utils.strToWday = function (s) {
    return '日月火水木金土'.indexOf(s);
  };

  /**
   *
   * @param wday
   * @returns {string}
   */
  Utils.wdayToStr = function (wday) {
    return (0<=wday && wday<=6) &&'日月火水木金土'.slice(wday,wday+1);
  };

  Utils.getDateTimeMD = function (d) {
    return Utils.zeroPad(d.getMonth()+1,2)+"/"+Utils.zeroPad(d.getDate(),2)+" "+Utils.zeroPad(d.getHours(),2)+":"+Utils.zeroPad(d.getMinutes(),2);
  };

  /**
   *
   * @param s
   * @returns {*}
   */
  Utils.pxToInt = function (s) {
    try {
      return parseInt(s.replace(/px$/, ''), 10);
    }
    catch (e) {}

    return 0;
  };

  /**
   *
   * @param message
   * @returns {{end: end}}
   * @constructor
   */
  Utils.TatCount = function (message) {
    var timestamp = Date.now();
    var prevtimestamp;

    // console.time()は使わない

    console.debug('BEGIN: '+message);

    function lap (s) {
      var d = Date.now();
      var tat = d-timestamp;
      var lap = prevtimestamp ? d-prevtimestamp : tat;
      prevtimestamp = d;
      console.debug('LAP: '+message+(s ? ' '+s:'')+' '+tat+'ms ('+lap+'ms)');
    }

    function end (s) {
      var tat = Date.now()-timestamp;
      console.debug('END: '+message+(s ? ' '+s:'')+' '+tat+'ms');
    }

    return {
      lap: lap,
      end: end
    }
  };

  /**
   *
   * @param list
   * @returns {Array.<T>}
   */
  Utils.getSortedKeys = function (list) {
    return Object.keys(list).filter(function (k) {return ! isNaN(k)}).sort(function (a,b) {return a-b});
  };

  /**
   *
   * @param a
   * @returns {Array.<T>|*|{TAG, CLASS, ATTR, CHILD, PSEUDO}}
   */
  Utils.uniqueArray = function (a) {
    return a.filter(function (x, i, self) {
      return self.indexOf(x) === i;
    });
  };

  /**
   *
   * @param a
   * @param b
   * @returns {Array.<T>|*|{TAG, CLASS, ATTR, CHILD, PSEUDO}}
   */
  Utils.getNoOverlaps = function (a, b) {
    return b.filter(function (x) {
      return a.indexOf(x) == -1;
    });
  };

  Utils.dataURItoUint8Array = function (dataURI) {
    var BASE64_MARKER = ';base64,';
    var index = dataURI.indexOf(BASE64_MARKER);
    if (index == -1) {
      return;
    }
    var mimeType =  dataURI.slice('data:'.length, index);
    var raw = window.atob(dataURI.substring(index+BASE64_MARKER.length));
    var uint8a = Uint8Array.from(Array.prototype.map.call(raw, function (x) {
      return x.charCodeAt(0);
    }));

    return {
      uint8a: uint8a,
      mimeType: mimeType
    }
  };

  /**
   *
   * @returns {{expr: RegExp, icon: string, link: null}[]}
   */
  Utils.linkExrps = function () {
    return [
      {expr:/^https?:\/\/([^/]*)pixiv\.(?:net|me|com)\//, icon:'pixiv', link:null},
      {expr:/^https?:\/\/([^/]*)twitter\.com\//, icon:'twitter', link:null},
      {expr:/^https?:\/\/([^/]*)nijie\.info\//, icon:'nijie', link:null},
      {expr:/^https?:\/\/([^/]*)nicovideo\.jp\//, icon:'niconico', link:null},
      {expr:/^https?:\/\/([^/]*)tumblr\.com\//, icon:'tumblr', link:null},
      {expr:/^https?:\/\/([^/]*)circle\.ms\//, icon:'circlems', link:null}
    ];
  };

  /**
   *
   * @param url
   * @returns {*}
   */
  Utils.expandLink = function (url) {
    if (!url) {
      return url;
    }

    var m = /^([A-Z]+?)\?(.+)$/.exec(url);
    if (m) {
      if (m[1] == 'PXV') {
        return 'http://www.pixiv.net/member.php?id='+m[2];
      }
      if (m[1] == 'PXM') {
        return 'http://pixiv.me/'+m[2];
      }
      if (m[1] == 'TWT') {
        return 'https://twitter.com/'+m[2];
      }
      if (m[1] == 'NJE') {
        return 'https://nijie.info/members.php?id='+m[2];
      }
      if (m[1] == 'NCS') {
        return 'http://www.nicovideo.jp/user/'+m[2];
      }
      if (m[1] == 'CMS') {
        return 'https://webcatalog.circle.ms/Circle/'+m[2];
      }
    }

    return url;
  };

  /**
   *
   * @param url
   * @returns {*}
   */
  Utils.contractLink = function (url) {
    if (!url) {
      return url;
    }

    var m;
    if (m = /^http:\/\/www\.pixiv\.net\/member(?:_illust)?\.php\?id=(.+?)(?:\W|$)/.exec(url)) {
      return 'PXV?'+m[1];
    }
    if (m = /^http:\/\/pixiv\.me\/(.+?)(?:\W|$)/.exec(url)) {
      return 'PXM?'+m[1];
    }
    if (m = /^https?:\/\/twitter\.com\/([^\\]+?)(?:\W|$)/.exec(url)) {
      return 'TWT?'+m[1];
    }
    if (m = /^https?:\/\/nijie\.info\/members(?:_illust)?\?id=(.+?)(?:\W|$)/.exec(url)) {
      return 'NJE?'+m[1];
    }
    if (m = /^http:\/\/www\.nicovideo\.jp\/user\/(.+?)(?:\W|$)/.exec(url)) {
      return 'NCS?'+m[1];
    }
    if (m = /^https?:\/\/webcatalog(?:-free)?\.circle\.ms\/Circle\/(.+?)(?:\W|$)/.exec(url)) {
      return 'CMS?'+m[1];
    }

    return url;
  };

  /**
   *
   * @param url
   * @returns {*}
   */
  Utils.expandAnnounceLink = function (url) {
    if (!url) {
      return url;
    }

    var m = /^([A-Z]+?)\?(.+)$/.exec(url);
    if (m) {
      if (m[1] == 'PPXV') {
        return 'http://www.pixiv.net/member_illust.php?mode=medium&illust_id='+m[2];
      }
      if (m[1] == 'PTWT') {
        var n = /^(.+?)&(.+)$/.exec(m[2]);
        if (n) {
          return 'https://twitter.com/'+n[1]+'/status/'+n[2];
        }
      }
    }

    return url;
  };

  /**
   *
   * @param url
   * @returns {*}
   */
  Utils.contractAnnounceLink = function (url) {
    if (!url) {
      return url;
    }

    var m;
    if (m = /^http:\/\/www\.pixiv\.net\/member_illust\.php\?(?:.+&)*illust_id=(.+?)(?:\W|$)/.exec(url)) {
      return 'PPXV?'+m[1];
    }
    if (m = /^https:\/\/twitter\.com\/([^\/]+?)\/status\/(.+?)(?:\W|$)/.exec(url)) {
      console.log(m[1], m[2]);
      return 'PTWT?'+[m[1], m[2]].join('&');
    }

    return url;
  };

  /**
   *
   * @param url
   * @returns {*}
   */
  Utils.expandShopLink = function (url) {
    if (!url) {
      return url;
    }

    var m = /^([A-Z]+?)\?(.+)$/.exec(url);
    if (m) {
      if (m[1] == 'TRA') {
        var n = /^(\d{2})(\d{4})(\d{2})(\d{2})/.exec(m[2]);
        if (n) {
          return 'http://www.toranoana.jp/mailorder/article/'+[n[1], n[2], n[3], n[4], m[2]].join('/')+'.html';
        }
      }
      if (m[1] == 'MEL') {
        return 'https://www.melonbooks.co.jp/detail/detail.php?product_id='+m[2];
      }
    }

    return url;
  };

  /**
   *
   * @param url
   * @returns {*}
   */
  Utils.contractShopLink = function (url) {
    if (!url) {
      return url;
    }

    var m;
    if (m = /^http:\/\/www\.toranoana\.jp\/mailorder\/article\/.+?\/(\d+)\.html(?:\W|$)/.exec(url)) {
      return 'TRA?'+m[1];
    }
    if (m = /^https:\/\/www\.melonbooks\.co\.jp\/detail\/detail\.php\?product_id=(.+?)(?:\W|$)/.exec(url)) {
      return 'MEL?'+m[1];
    }

    return url;
  };

  /**
   * スペース番号にabなしの場合は整形する
   * @param space
   * @param noab
   * @returns {*}
   */
  Utils.getSpaceNoStr = function (space,noab) {
    if (noab) {
      // 1,2,3,4,...のパターン
      return global.Utils.zeroPad(space.spaceNo + (space.spaceNoSub ? 1 : 0), 2);
    }
    else {
      // 1a,1b,2a,2b,...のパターン
      return global.Utils.zeroPad(space.spaceNo, 2) + (space.spaceNoSub ? 'b' : 'a');
    }
  };

  function spawn (generatorFunc) {
    function continuer(verb, arg) {
      var result;
      try {
        result = generator[verb](arg);
      } catch (err) {
        return Promise.reject(err);
      }
      if (result.done) {
        return Promise.resolve(result.value);
      } else {
        return Promise.resolve(result.value).then(onFulfilled, onRejected);
      }
    }
    var generator = generatorFunc();
    var onFulfilled = continuer.bind(continuer, "next");
    var onRejected = continuer.bind(continuer, "throw");
    return onFulfilled();
  }

  window.spawn = window.spawn || spawn;

  //---

  global.Utils = Utils;

})(DjCRDView);