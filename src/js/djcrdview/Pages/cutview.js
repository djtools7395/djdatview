// TODO キーボードでのサークル移動の実装
// FIXME リサイズで未描画カットが表示領域に入ってきたときの書き換え

(function (global) {

  var CutView = {};

  //

  var elm = {
    $container: null,
    $containerGrid: null
  };

  /**
   * 頻繁に利用する element を保存しておく
   */
  function setElements () {
    elm.$container = $('#cutGridContainer');
    elm.$containerGrid = $('#cutGrid');
  }

  //

  // カットグリッドへのアクセスを容易にするため
  var CUT_BOXIES = [];

  //---

  var selectedId = {};

  CutView.clearSelection = function () {
    selectedId = {
      eventId: 0,
      dateId: 0,
      mapId: 0,
      blockId: 0,
      circleId: 0
    };

    // FIXME descBarの消去も

    return selectedId;
  };

  function isEventSelected () {
    return selectedId.eventId == global.Env.selectedEventId;
  }

  function isDateSelected () {
    return isEventSelected() &&
      selectedId.dateId == global.Env.selectedDateId;
  }

  function isBlockSelected () {
    return isDateSelected() &&
      selectedId.blockId == global.Env.selectedBlockId;
  }

  function isCircleSelected () {
    return isEventSelected() &&
      selectedId.circleId == global.Env.selectedCircleId;
  }

  function changeBlockSelection () {
    selectedId.eventId = global.Env.selectedEventId;
    selectedId.dateId = global.Env.selectedDateId;
    selectedId.blockId = global.Env.selectedBlockId;
  }

  /**
   * タップ等でサークルを選択した時に、テンポラリ変数と環境変数の両方を更新する
   * @param circleId
   */
  function setSelectedCircleId (circleId) {
    if (!circleId)
      return;

    selectedId.circleId = circleId;
    global.DataOp.EnvOp.save({
      'selectedCircleId': circleId
    });
  }

  //

  // カットグリッドのカラム数の最少＆最大値
  const GRIG_COLUMNS_MIN = 4;
  const GRIG_COLUMNS_MAX = 20;

  // ナビパネル関連
  const DAY_SELECTOR_COLUMNS = 3;
  const BLOCK_SELECTOR_COLUMNS = 4;

  //--

  /**
   * お気に入りの表示更新
   * @param circleId
   * @param colorId
   * @private
   */
  CutView.updateFavItem = function (fav) {

    // お気に入り詳細
    updateFavDescbarItem(fav);

    // カット画像
    setCutFavColor(fav.Fav.circleId);
  };

  /**
   * サークル詳細バーのアイテムを更新
   * @param fav
   */
  function updateFavDescbarItem (fav) {
    global.GuiParts.DescBar.updateDescItem(fav);
  }

  /**
   * カットグリッドをタップした際のハンドラ
   */
  function tapGridHandler () {

    var circleId = $(this).attr('data-circle-id');
    if (!circleId) {
      return;
    }

    global.DataOp.Circle.getCircle(circleId)
      .then(function (r) {
        global.GuiParts.DescBar.updateDescBar(r.Circle.circleId);

        setSelectedCircleId(circleId);

        global.DataOp.EnvOp.save({
          'selectedDateId': r.Circle.dateId,
          'selectedMapId': global.DataOp.EventList.Selected.Block[r.Circle.blockId].mapId,
          'selectedBlockId': r.Circle.blockId,
          'selectedCircleId': r.Circle.circleId
        });

        global.Pages.MapView.updateView();

      })
      .catch(function (e) {
        // スペースに対応するサークル情報がない（準備会スペースとか）もここに落ちる
        if (e)
          console.error(e.stack || e);
      });
  }

  const CUT_CANVAS_TEMPLATE = `
<div data-circle-id="#CIRCLE_ID#" class="dj-mapview-cutgrid-#BASE#-container" id="#BASE#ItemContainer-#CIRCLE_ID#">
    <canvas data-canvas-type="image" class="dj-mapview-cutgrid-#BASE#-canvas" id="#BASE#Item-#CIRCLE_ID#"></canvas>
    <div data-canvas-type="overlay" class="dj-mapview-cutgrid-#BASE#-canvas-overlay" id="#BASE#ItemOverlay-#CIRCLE_ID#"></div>
</div>
`;

  /**
   *
   * @param circleId
   */
  function createCutCanvasHTML (circleId) {
    var isBlock = isNaN(circleId);
    return CUT_CANVAS_TEMPLATE.replace(/#CIRCLE_ID#/g,circleId).replace(/#BASE#/g,isBlock ? 'block' : 'cut');
  }

  /**
   * カットのスペース番号の書き込み
   * @param config
   */
  function drawCutSpaceNoText (config) {

    var $overlay = config.$canvas || $('#cutItemOverlay-'+config.circleId)[0];
    if (!$overlay) {
      return;
    }

    $overlay.text(config.spaceNo && !config.spaceNoSub ? config.spaceNo : '');
  }

  /**
   * カットのお気に入り色の変更
   * @param circleId
   */
  function setCutFavColor (circleId, $box) {
    $box = $box || $('#cutItemContainer-'+circleId);
    if (!$box[0]) {
      return;
    }

    var fav = global.DataOp.FavCircle.findFavCircle(circleId);
    if (fav) {
      $box.attr('data-color-id', fav.Fav.colorId);
    }
    else {
      $box.attr('data-color-id', null);
    }
  }

  /**
   *
   * @param colorId
   */
  function updateCutFavColorStyle (colorId) {
    var chk = colorId && global.DataOp.FavColor.findFavColor(colorId);
    if (!chk) {
      return;
    }

    var selector = '.dj-mapview-cutgrid-cut-container[data-color-id="'+colorId+'"] > .dj-mapview-cutgrid-cut-canvas-overlay';
    global.GuiParts.Css.insertStyle(selector, {'background-color': '#'+chk.checkColor});
  }

  /**
   * お気に入りサイドバーのアイテムの表示・非表示＆お気に入り色のスタイルをリセットする
   */
  function resetCutFavColorStyle () {
    global.DataOp.FavColor.getSortedKeys().forEach(function (colorId) {
      updateCutFavColorStyle(colorId);
    });
  }

  /**
   * カットグリッドorブロックレイヤーに画像を書き込む
   * @param eventid
   */
  function putCutsToGrid () {

    var tat = new global.Utils.TatCount('update cut grid');

    // サークルカット
    var blockId = global.Env.selectedBlockId;

    if (CUT_BOXIES.length == 0) {
      // 初回描画時はブロック名キャンバスをまず追加
      var innerHTML = createCutCanvasHTML('BB');
      var $box = $(innerHTML).appendTo(elm.$containerGrid);
      var box = {
        $box: $box,
        $canvas: $box.find('canvas[data-canvas-type="image"]'),
        $canvasspno: $box.find('div[data-canvas-type="overlay"]')
      };
      CUT_BOXIES.push(box);

      // 初期サイズ
      box.$canvas[0].width = global.DataOp.EventList.Selected.Event.cutSizeW *2;
      box.$canvas[0].height = global.DataOp.EventList.Selected.Event.cutSizeH;
    }

    // ブロック名キャンバス
    (function (box) {

      var blockKey = 'B' + global.Utils.zeroPad(blockId, 3);

      box.$box.attr({
        'data-circle-id': blockKey
      }).removeClass('dj-mapview-cutgrid-image-loading');

      global.GuiParts.drawImageToCanvas(box.$canvas[0],global.DataOp.CutImage.findEtcImage(blockKey));
    })(CUT_BOXIES[0]);

    // カットキャンバス
    return global.DataOp.Circle.getCirclesInBlock().then(function (rows) {

      var boxCount = CUT_BOXIES.length;

      tat.lap('DONE get circles in block block='+blockId+' num='+rows.length+' append='+(rows.length-(boxCount-1)));

      var innerHTML = '';

      rows.slice(boxCount-1).forEach(function (item) {
        // カットキャンバスの追加
        innerHTML += createCutCanvasHTML(item.circleId);
      });

      if (innerHTML != '') {
        $(innerHTML).appendTo(elm.$containerGrid);

        elm.$containerGrid.children()
          .each(function (index) {
            if (index < boxCount) {
              return;
            }

            var $box = $(this);
            var box = {
              $box: $box,
              $canvas: $box.find('canvas[data-canvas-type="image"]'),
              $canvasspno: $box.find('div[data-canvas-type="overlay"]')
            };
            CUT_BOXIES.push(box);

            // 初期サイズ
            box.$canvas[0].width = global.DataOp.EventList.Selected.Event.cutSizeW;
            box.$canvas[0].height = global.DataOp.EventList.Selected.Event.cutSizeH;
          });

        tat.lap('DONE append cut canvas');

        // 追加したカットキャンバスの初期化
        CUT_BOXIES.slice(boxCount).forEach(function (box) {
          // タップハンドラの設定
          box.$box.on('tap', tapGridHandler);
        });

        tat.lap('DONE init appended cut canvas');
      }

      // 必要なカットキャンバスだけ有効化する

      CUT_BOXIES.forEach(function (box, index) {

        if (index==0) {
          // ブロック名
          return;
        }

        if (index > rows.length) {
          // カット数より多いキャンバスは非表示に
          box.$box
            .attr({
              'id': null,
              'data-circle-id': ''
            })
            .addClass('dj-mapview-cutgrid-cut-disabled');
          box.$canvas.removeClass('dj-mapview-cutgrid-image-loading');

          return;
        }

        var item = rows[index-1];

        // 表示する分は circleId を差し替え
        box.$box
          .attr({
            'id': 'cutItemContainer-'+item.circleId,
            'data-circle-id': item.circleId
          })
          .removeClass('dj-mapview-cutgrid-cut-disabled');
        box.$canvas.addClass('dj-mapview-cutgrid-image-loading');

        var $canvas = box.$canvas;
          $canvas
          .attr({
            'id': 'cutItem-' + item.circleId
          });

        var $canvasspno = box.$canvasspno;
        $canvasspno
          .attr({
            'data-circle-id': item.circleId,
            'id': 'cutItemOverlay-' + item.circleId
          });

        //
        setCutFavColor(item.circleId, box.$box);

        //
        drawCutSpaceNoText({$canvas:$canvasspno, circleId:item.circleId, spaceNo:item.spaceNo, spaceNoSub:item.spaceNoSub});
      });

      tat.end();

      // カット画像の遅延ロードの設定
      setCutImageLoadLazy();

      return Promise.resolve(rows);

    });
  }

  /**
   * 表示中のカットグリッドのみ画像を描画する
   * @param target
   */
  function cutGridScrollTimeoutHandler (target) {

    var tat = new global.Utils.TatCount('draw cut images');

    var drawCount = 0;
    var boxies = {};

    // カットグリッドの表示範囲
    var top = target.scrollTop;
    var bottom = top + target.clientHeight;
    if (top == bottom) {
      // 隠れている状態
      return;
    }

    // 書き換え対象のグリッドを抽出する
    CUT_BOXIES.forEach(function (box) {

      var $box = box.$box;
      var $canvas = box.$canvas;

      var circleId = $box.attr('data-circle-id');
      if (isNaN(circleId)) {
        return;
      }

      if (!$canvas[0].classList.contains('dj-mapview-cutgrid-image-loading') || $box[0].classList.contains('dj-mapview-cutgrid-cut-disabled')) {
        // 画像読み込み済み or 非表示
        return;
      }

      // カット位置
      var t = $box[0].offsetTop - $box[0].parentElement.offsetTop;
      var b = t + $box[0].clientHeight;

      if (b < top || bottom < t) {
        // 表示範囲外
        return;
      }

      boxies[circleId] = box;
    });

    // 書き換えるグリッドのサークルIDをソート済みの配列にする
    var cutKeys = Object.keys(boxies).sort(function (a,b) {return a-b});

    // サークルカットを一気に取得する
    return global.DataOp.CutImage.getCutImageEntryMultiple(cutKeys, true)
      .then(function (images) {

        tat.lap('cut image loaded');

        Object.keys(images).forEach(function (circleId) {
          var box = boxies[circleId];
          if (box.$box.attr('data-circle-id') != circleId) {
            // 処理中にページ移動とか発生してすでに別のサークルになっているかも
            return;
          }

          ++drawCount;

          box.$canvas.removeClass('dj-mapview-cutgrid-image-loading');

          global.GuiParts.drawImageToCanvas(box.$canvas[0], images[circleId].altImage ||images[circleId].image, global.DataOp.EventList.Selected.Event.cutSizeW*2, global.DataOp.EventList.Selected.Event.cutSizeH*2);
        });

        tat.end('length='+drawCount);

        return Promise.resolve();
      });
  }

  /**
   *
   * @param circleId
   */
  CutView.drawCutImage = function (circleId) {
    var canvas = document.getElementById('cutItem-'+circleId);
    if (!canvas) {
      return Promise.resolve();
    }

    return global.DataOp.CutImage.getCutImageEntry(circleId).then(function (obj) {
      global.GuiParts.drawImageToCanvas(canvas, obj.altImage || obj.image, global.DataOp.EventList.Selected.Event.cutSizeW*2, global.DataOp.EventList.Selected.Event.cutSizeH*2);
    });
  };

  /**
   * カット画像の遅延ロードを設定する
   */
  var setCutImageLoadLazy  = (function () {

    var tid = 0;

    function setLoadLazy () {
      function setLazyTimer (e, interval) {
        if (tid) {
          clearTimeout(tid);
        }

        tid = setTimeout(function () {cutGridScrollTimeoutHandler(e)}, interval);
      }

      // キー押しっぱなしで連続でコールされる場合があるので必ず遅延させる
      setLazyTimer(elm.$container[0], 100);

      elm.$container.off('scroll').on('scroll', function (e) {
        setLazyTimer(e.target, 100);
      });
    }

    return setLoadLazy;
  })();

  function disableUnusedBlockButton (dateId) {
    $('.dj-block-select-button').each(function () {
      var blockId = $(this).attr('data-block-id');
      var obj = global.DataOp.EventList.Selected.BlockList.getBlockList(dateId, blockId);
      this.disabled = !obj;
    });
  }

  /**
   * イベント切り替えなどの操作があったらナビパネルの書き換えを行う
   */
  CutView.updateNavPanel = function () {

    function setDayButtonSelectedStyle (dateId) {

      disableUnusedBlockButton(dateId);

      $('#date-select-buttons button').each(function () {
        var $btn = $(this);
        if ($btn.attr('data-date-id') == dateId) {
          $btn.addClass('selected');
        }
        else {
          $btn.removeClass('selected');
        }
      });
    }

    function setBlockButtonSelectedStyle (blockId) {
      $('#block-select-buttons button').each(function () {
        var $btn = $(this);
        if ($btn.attr('data-block-id') == blockId) {
          $btn.addClass('selected');
        }
        else {
          $btn.removeClass('selected');
        }
      });
    }

    function addDateSelector ($listview) {
      // ディバイダー
      $('<li>').attr({
        'data-role': 'list-divider'
      }).text('日付選択').appendTo($listview);

      var cols = DAY_SELECTOR_COLUMNS;

      var $li = $('<li>').addClass('dj-navpanel-selector-container').attr({'id':'date-select-buttons'}).appendTo($listview);
      var $grid = $('<div>').addClass('ui-grid-'+String.fromCharCode('a'.charCodeAt(0) + (cols-2))).appendTo($li);

      var dobj = global.DataOp.EventList.Selected.Date;
      var dkeys = global.Utils.getSortedKeys(dobj);

      dkeys.forEach(function (d) {
        var x = (d-1) % cols + 1;
        var dd = dobj[d];
        var day = dd.day;
        var dateId = dd.dateId;
        var abc = String.fromCharCode('a'.charCodeAt(0) + (x-1));
        var $box = $('<div>').addClass('ui-block-'+abc).appendTo($grid);
        var $btn = $('<button>')
          .addClass('dj-navpanel-selector ui-btn ui-corner-all dj-day-select-button')
          .attr({'data-date-id': dateId})
          .text(day)
          .appendTo($box);

        (function (d) {
          $btn.on('tap', function () {
            global.DataOp.EnvOp.save({
              'selectedDateId': d
            });

            setDayButtonSelectedStyle(d);
          });
        })(dateId);
      });
    }

    function addBlockSelector ($listview) {
      // ディバイダー
      $('<li>').attr({
        'data-role': 'list-divider'
      }).text('ブロック選択').appendTo($listview);

      var cols = BLOCK_SELECTOR_COLUMNS;

      var bobj = global.DataOp.EventList.Selected.Block;
      var bkeys = bobj.sortedKeys;
      var akeys = (function () {
        var keys = {};
        bkeys.forEach(function (b) {
          keys[bobj[b].areaId] = true;
        });
        return global.Utils.getSortedKeys(keys);
      })();

      akeys.forEach(function (a) {
        var $li = $('<li>').addClass('dj-navpanel-selector-container').attr({'id':'block-select-buttons'}).appendTo($listview);
        var $grid = $('<div>').addClass('ui-grid-'+String.fromCharCode('a'.charCodeAt(0) + (cols-2))).appendTo($li);
        var num = 0;
        bkeys.forEach(function (b) {
          if (bobj[b].areaId != a) {
            return;
          }
          var x = num++ % cols + 1;
          var abc = String.fromCharCode('a'.charCodeAt(0) + (x-1));
          var $box = $('<div>').addClass('ui-block-'+abc).appendTo($grid);
          var $btn = $('<button>')
            .addClass('dj-navpanel-selector ui-btn ui-corner-all dj-block-select-button')
            .attr({'data-block-id': bobj[b].blockId})
            .text(bobj[b].name)
            .appendTo($box);

          (function (b) {
            $btn.on('tap', function () {
              global.DataOp.EnvOp.save({
                'selectedBlockId': b
              });

              putCutsToGrid();

              setBlockButtonSelectedStyle(b);
            });
          })(b);
        });
      });

      disableUnusedBlockButton(global.DataOp.EventList.Selected.Date[global.DataOp.EventList.Selected.Date.sortedKeys[0]].dateId);
    }

    function addGridColumnChanger ($listview) {
      // ディバイダー
      $('<li>').attr({
        'data-role': 'list-divider'
      }).text('カラム数調整').appendTo($listview);

      var akeys = ['-','+'];

      var cols = akeys.length;

      var $li = $('<li>').addClass('dj-navpanel-selector-container').appendTo($listview);
      var $grid = $('<div>').addClass('ui-grid-'+String.fromCharCode('a'.charCodeAt(0) + (cols-2))).appendTo($li);

      akeys.forEach(function (b,index) {
        var abc = String.fromCharCode('a'.charCodeAt(0) + index);
        var $box = $('<div>').addClass('ui-block-'+abc).appendTo($grid);
        var $btn = $('<button>').addClass('dj-navpanel-selector ui-btn ui-corner-all').text(b).appendTo($box);

        (function (i) {
          $box.on('tap', function () {
            CutView.changeGridColumnCount(i == 0 ? -1 : 1);
          });
        })(index);
      });
    }

    function addCutOpacityChanger ($listview) {
      // ディバイダー
      $('<li>').attr({
        'data-role': 'list-divider'
      }).text('未チェックサークルを隠す').appendTo($listview);

      var akeys = ['-','+'];

      var cols = akeys.length;

      var $li = $('<li>').addClass('dj-navpanel-selector-container').appendTo($listview);
      var $grid = $('<div>').addClass('ui-grid-'+String.fromCharCode('a'.charCodeAt(0) + (cols-2))).appendTo($li);

      akeys.forEach(function (b,index) {
        var abc = String.fromCharCode('a'.charCodeAt(0) + index);
        var $box = $('<div>').addClass('ui-block-'+abc).appendTo($grid);
        var $btn = $('<button>').addClass('dj-navpanel-selector ui-btn ui-corner-all').text(b).appendTo($box);

        (function (i) {
          $box.on('tap', function () {
            changeUncheckedCutOpacity(i == 0);
          });
        })(index);
      });
    }


    //

    var $navpanel = $('#cutNavPanelList').empty();

    if (global.DataOp.EventList.Selected.Date.sortedKeys.length > 1) {
      addDateSelector($navpanel);
    }

    addBlockSelector($navpanel);
    addGridColumnChanger($navpanel);
    addCutOpacityChanger($navpanel);

    setDayButtonSelectedStyle(global.Env.selectedDateId);
    setBlockButtonSelectedStyle(global.Env.selectedBlockId);

    $navpanel.listview().listview('refresh');
  };

  /**
   *
   */
  var changeUncheckedCutOpacity = (function () {

    var OPACITY_MAX = 100;
    var OPACITY_STEP = 20;

    var opacity = OPACITY_MAX;

    function setStyle (b) {

      if (b === false) {
        opacity = opacity + OPACITY_STEP;
      }
      else if (b === true) {
        opacity = opacity - OPACITY_STEP;
      }
      else if (!isNaN(b)) {
        opacity = b;
      }
      else {
        opacity = OPACITY_MAX;
      }

      if (opacity > OPACITY_MAX) {
        opacity = OPACITY_MAX;
      }
      else if (opacity < OPACITY_STEP) {
        opacity = OPACITY_STEP;
      }

      global.DataOp.EnvOp.save({
        'uncheckedCutOpacity': opacity
      });

      var op = opacity / OPACITY_MAX;

      // 未チェックサークル
      global.GuiParts.Css.insertStyle('.dj-mapview-cutgrid-cut-container', {'opacity': op});

      // チェック済みサークル
      global.DataOp.FavColor.getSortedKeys().forEach(function (colorId) {
        var chk = colorId && global.DataOp.FavColor.findFavColor(colorId);
        if (!chk) {
          return;
        }

        var selector = '.dj-mapview-cutgrid-cut-container[data-color-id="' + colorId + '"]';
        if (chk.enabled) {
          global.GuiParts.Css.insertStyle(selector, {'opacity': chk.enabled ? 1 : op});
        }
        else {
          global.GuiParts.Css.removeStyle(selector);
        }
      });
    }

    return setStyle;
  })();

  /**
   * 前後のブロックに移動する
   * @param toNext
   */
  CutView.moveToNextBlock = function (toNext, keycode) {

    if (keycode) {
      var target = elm.$container[0];
      var top = (function (target) {
        if (toNext && (target.scrollTop+target.clientHeight) < target.scrollHeight) {
          return target.scrollTop + target.clientHeight * .9;
        }
        else if (!toNext && target.scrollTop > 0) {
          return target.scrollTop - target.clientHeight * .9;
        }
      })(elm.$container[0]);
      if (top !== undefined) {
        return elm.$container.animate({scrollTop: top}, !global.Env.useAnimation ? 0 : 'fast');
      }
    }

    var obj = global.DataOp.EventList.Selected.BlockList.getNext(global.Env.selectedDateId, global.Env.selectedBlockId, toNext);

    global.DataOp.EnvOp.save({
      'selectedDateId': obj.dateId,
      'selectedBlockId': obj.blockId
    });

    global.DataOp.CutImage.getCutImageInBlock(obj.dateId, obj.blockId)
      .then(function () {
        return putCutsToGrid();
      })
      .then(function () {
        // FIXME 前ブロックへの移動時にブロック名書き換え→スクロールの順なるのがキモいので後で直す
        target.scrollTop = toNext ? 0 : target.scrollHeight - target.clientHeight;
      });
  };

  /**
   * 指定個数のカットが画面内に収まるようにカット画像サイズを調整する
   */
  function alignCutWidthFitToScreen () {
    // TODO 個数分 class を作っておいて addClass/removeClass で制御した方がいいかも
    global.GuiParts.Css.insertStyle('.dj-mapview-cutgrid-cut-container', { width: Math.floor(1000/global.Env.cutColumnsLandscape)*.1+'%' }, 'only screen and (orientation:landscape)');
    global.GuiParts.Css.insertStyle('.dj-mapview-cutgrid-block-container', { width: Math.floor(2000/global.Env.cutColumnsLandscape)*.1+'%' }, 'only screen and (orientation:landscape)');

    global.GuiParts.Css.insertStyle('.dj-mapview-cutgrid-cut-container', { width: Math.floor(1000/global.Env.cutColumnsPortrait)*.1+'%' }, 'only screen and (orientation:portrait)');
    global.GuiParts.Css.insertStyle('.dj-mapview-cutgrid-block-container', { width: Math.floor(2000/global.Env.cutColumnsPortrait)*.1+'%' }, 'only screen and (orientation:portrait)');

    // FIXME 10・14 を計算で求められないものか
    global.GuiParts.Css.insertStyle('.dj-mapview-cutgrid-cut-canvas-overlay', { 'font-size': (12/global.Env.cutColumnsLandscape)+'vw'}, 'only screen and (orientation:landscape)');
    global.GuiParts.Css.insertStyle('.dj-mapview-cutgrid-cut-canvas-overlay', { 'font-size': (16/global.Env.cutColumnsPortrait)+'vw'}, 'only screen and (orientation:portrait)');
  }

  /**
   * グリッドのカラム数を変更する
   * @param d : +-
   */
  CutView.changeGridColumnCount = function (d) {
    var mode = global.GuiParts.isLandscape() ? 'cutColumnsLandscape' : 'cutColumnsPortrait';

    var oldColumns = global.Env[mode];
    var newColumns = oldColumns + (d > 0 ? 2 : -2);
    if (newColumns < GRIG_COLUMNS_MIN) {
      newColumns = GRIG_COLUMNS_MIN
    }
    else if (newColumns > GRIG_COLUMNS_MAX) {
      newColumns = GRIG_COLUMNS_MAX;
    }


    if (oldColumns != newColumns) {
      var obj = {};
      obj[mode] = newColumns;
      global.DataOp.EnvOp.save(obj);

      alignCutWidthFitToScreen();
      setCutImageLoadLazy();
    }
  };

  /**
   * ビューリサイズに呼び出す
   */
  CutView.resizedViewHandler = function () {
    // リサイズにより未描画のカットグリッドが画面上に現れるかも
    setCutImageLoadLazy();
  };

  /**
   * ページを左右にスワイプしたら前後のブロックに移動する
   * @param e
   */
  function swipePageHandler (e) {
    console.debug('on '+e.type+' (cut)');
    CutView.moveToNextBlock(e.type == 'swipeleft');
  }

  /**
   * カットビューの更新
   */
  CutView.updateView = function () {

    if (!global.Env.selectedEventId) {
      // イベントが選択されていない場合は書き換えの必要が無い
      return;
    }

    // 内部状態の保存
    var isBlockChanged = ! isBlockSelected();
    var isCircleChanged = ! isCircleSelected();

    // 内部状態の切り替え
    changeBlockSelection();

    resetCutFavColorStyle();

    changeUncheckedCutOpacity(global.Env.uncheckedCutOpacity);

    if (isBlockChanged) {
      putCutsToGrid();

      // TODO サークル移動
    }
    else if (isCircleChanged) {
      // TODO サークル移動
    }
  };

  //--

  /**
   * pageinit時に実行される初期化処理
   */
  CutView.initView = function () {

    console.debug('init CutView');

    // 頻繁に利用するエレメントの保存
    setElements();

    // 選択状態の初期化
    CutView.clearSelection();

    // カットサイズを画面幅に合わせる
    alignCutWidthFitToScreen();

    // ページを左右にスワイプしたら前後のブロックに移動する
    elm.$containerGrid.on('swipeleft swiperight', swipePageHandler);
  };

  //---

  global.Pages = global.Pages || {};

  global.Pages.CutView = CutView;

})(DjCRDView);
