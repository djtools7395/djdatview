(function (global) {

  var FavListSelect = {};

  FavListSelect.$page = $('#favListSelectPage');

  var elm = {
    $container: null
  };

  function setElements () {
    elm.$container = $('#favListSelector');
  }

  //---

  /**
   *
   * @param eventid
   */
  function putItemsToList (eventid) {

    // イベント情報を全部取得する
    global.DataOp.FavList.getFavListEntries().then(function (obj) {
      var favlists = [];
      obj.forEach(function (item) {
        favlists.push({
          id:item.djdatNo,
          name:item.djdatName
        });
      });

      return Promise.resolve(favlists);

    }).then(function (favlists) {
      // リストアイテムを追加する
      var basename = 'favlistNameList';

      global.GuiParts.CheckboxList.setListItems(elm.$container, basename, global.GuiParts.CheckboxList.CHECKBOX, favlists);

      favlists.forEach(function (favlist) {
        $(document.getElementById([basename,'label',favlist.id].join('-'))).on('taphold', function () {/*romRemoveHandler(this)*/});
        $(document.getElementById([basename,'radio',favlist.id].join('-'))).on('change', function () {/*romSelectHandler(this)*/});
      });
    }).catch (function (e) {
      if (e)
        console.error(e.stack || e);
    });
  }

  /**
   *
   */
  FavListSelect.initView = function () {

    // 頻繁に利用するエレメントの保存
    setElements();

    FavListSelect.$page.on('pageshow', function () {
      putItemsToList();
    });

  };

  //---

  global.Pages = global.Pages || {};

  global.Pages.FavListSelect = FavListSelect;

})(DjCRDView);
