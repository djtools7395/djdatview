// FIXME 選択中のイベントを削除すると起動しなくなる

(function (global) {

  var RomSelect = {};

  RomSelect.$page = $('#romSelectPage');

  //

  var elm = {
    $container: null,
    $label: null
  };

  /**
   * 頻繁に利用する element を保存しておく
   */
  function setElements () {
    elm.$label = $('#romSelectorLabel');
    elm.$container = $('#romSelector');
  }

  //---

  /**
   * イベントの削除を選択した
   */
  function romRemoveHandler () {
    var $box = $(this);
    var btn = $(document.getElementById($box.attr('for')));
    var eventId = btn.val();
    var name = global.DataOp.EventList.Selected.Event.djdatName;

    global.DataOp.RomList.removeRom(eventId, name)
      .then(function () {

        console.log('ROM REMOVED: '+eventId);

        // 内部状態のリセット
        global.DataOp.EnvOp.setSelectedEvent(0);
        global.DataOp.EventList.setSelectedEvent(0);

        putItemsToList();
      })
      .catch(function (e) {
        if (e) {
          console.error(e.stack || e);
          return;
        }

        console.log('event remove cancelled');
      });
  }

  /**
   * イベントを選択した
   * @param obj
   */
  function romSelectHandler () {
    var $box = $(this);
    var eventId = $box.val();

    console.log('ROM SELECTED: '+eventId);

    if (global.Env.selectedEventId != eventId) {
      // 内部状態の切り替え
      global.DataOp.EnvOp.setSelectedEvent(eventId);
    }

    global.DataOp.EventList.setSelectedEvent(eventId);

    // キャッシュのクリア→ビューの書き換え
    global.clearCacheAll()
      .then(function () {
        global.Pages.MapView.updateMapViewOnChangeEvent();
      })
      .catch(function (e) {
      if (e)
        console.error(e.stack || e);
    });
  }

  /**
   * イベントを選択する
   */
  RomSelect.setRomSelected = function (eventId) {
    if (!eventId) {
      return;
    }

    $('#eventNameList-'+eventId).click().checkboxradio('refresh');
  };

  /**
   * イベントリストを書き換える
   */
  function putItemsToList () {

    var keys = global.DataOp.EventList.getSortedKeys();

    if (keys.length == 0) {
      elm.$label.text('Naviメニューからイベントデータ(zip)を追加してください');
      elm.$container.empty();
      return;
    }

    elm.$label.text('イベント選択／削除');

    // アイテムをリストに
    var basename = 'eventNameList';

    var events = keys.map(function (id) {
      var event = global.DataOp.EventList.findEvent(id);
      return {
        id: id,
        name: event.Event.djdatName+' ('+event.Event.djdatNo+')'
      }
    });

    // リストアイテムの追加
    global.GuiParts.CheckboxList.setListItems(elm.$container, basename, global.GuiParts.CheckboxList.RADIO, events);

    // イベントの追加
    events.forEach(function (entry) {
      // 削除
      $(getEventRemoveElement(basename,entry.id)).on('taphold', romRemoveHandler);
      // 選択
      $(getEventSelectElement(basename,entry.id)).on('change', romSelectHandler);
    });
  }

  function getEventRemoveElement (basename, eventId) {
    return document.getElementById([basename,'Remove',eventId].join('-'));
  }

  function getEventSelectElement (basename, eventId) {
    return document.getElementById([basename,eventId].join('-'));
  }

  /**
   * ZIPファイルを選択した
   * @param obj
   */
  function zipSelectHandler (obj) {
    function callback  (eventId) {
      $input.val('');

      global.DataOp.EnvOp.setSelectedEvent(eventId);
      global.DataOp.EventList.setSelectedEvent(eventId);

      putItemsToList();
      RomSelect.setRomSelected(eventId);
    }

    //

    var file = obj.files.length > 0 && obj.files[0];
    if ( ! file ) {
      return;
    }

    var $input = $(obj);

    // zipファイルを選択したら読み込む
    global.DataOp.RomList.loadRomZip(file, callback);
  }

  /**
   *
   */
  function updateNavPanel () {

    //
    function addZipLoadButton ($listview) {
      var buttons = [
        { label:'zip追加', type:'file', accept:'.zip', callback:zipSelectHandler }
      ];

      global.GuiParts.NaviPanel.addButtons('イベントデータ追加', buttons, $listview);
    }

    //

    var $navpanel = $('#romSelectNaviPanelList').empty();

    addZipLoadButton($navpanel);

    $navpanel.listview().listview('refresh');

  }

  //---

  /**
   * 
   */
  RomSelect.initView = function () {

    console.debug('init RomSelect');

    // 頻繁に利用するエレメントの保存
    setElements();

    updateNavPanel();

    putItemsToList();
  };

  //---

  global.Pages = global.Pages || {};

  global.Pages.RomSelect = RomSelect;

})(DjCRDView);
