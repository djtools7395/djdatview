// FIXME お気に入りサークルカットのサイズが固定
// TODO mapPointer の色に (255,0,0) 以外も選択できるようにする
// TODO お気に入りのソート
// FIXME MapViewとそれ以外を行ったり来たりしたとき、MapView時のスクロール位置が保持されない

(function (global) {

  var MapView = {};

  MapView.$page = $('#mapViewPage');

  //

  var elm = {
    overlay: null,
    map: null,
    fav: null,
    genre: null,
    $container: null,

    $favcontainer: null
  };

  /**
   * 頻繁に利用する element を保存しておく
   */
  function setElements () {

    elm.$container = $('#mapCanvasContainer');

    elm.overlay = $('#mapOverlayBitmap')[0];
    elm.map = $('#mapBitmap')[0];
    elm.fav = $('#mapFavBitmap')[0];
    elm.genre = $('#mapGenreBitmap')[0];

    elm.$favcontainer = $('#mapFavsList');
  }

  //---

  // グリッドマップの大きさの基準（若干前後する）
  const GRID_SIZE_BASE = 1000;

  //---

  function isEventSelected () {
    return selectedId.eventId == global.Env.selectedEventId;
  }

  function isDateSelected () {
    return isEventSelected() &&
      selectedId.dateId == global.Env.selectedDateId;
  }


  function isMapSelected () {
    return isDateSelected() &&
      selectedId.mapId == global.Env.selectedMapId;
  }

  function isCircleSelected () {
    return isEventSelected() &&
      selectedId.circleId == global.Env.selectedCircleId;
  }

  function changeMapSelection () {
    selectedId.eventId = global.Env.selectedEventId;
    selectedId.dateId = global.Env.selectedDateId;
    selectedId.mapId = global.Env.selectedMapId;
    selectedId.circleId = global.Env.selectedCircleId;
  }

  function clearSelection () {
    selectedId = {
      eventId: 0,
      dateId: 0,
      mapId: 0,
      circleId: 0
    };

    return selectedId;
  }

  // 内部状態を保持する（ページに遷移した時、全体の状態と不一致ならビューを書き換えるため）
  var selectedId = clearSelection();

  // グリッドマップ
  var gridMap = null;

  // 各グリッドの大きさ
  var divider = {
    x: 1,
    y: 1
  };

  function getGridX (x) {
    return Math.floor(x/divider.x);
  }

  function getGridY (y) {
    return Math.floor(y/divider.y);
  }

  function getGridPos (x,y) {
    return {
      x: getGridX(x),
      y: getGridY(y)
    };
  }

  /**
   * マップ上のサークルの座標を取得する
   * @param layout
   * @param isSpaceB
   * @returns {{xpos: (*|defs.tables.DjdatLayout.columns.xpos), ypos: (*|defs.tables.DjdatLayout.columns.ypos), width: (defs.tables.DjdatInfo.columns.mapSizeW|*), height: (defs.tables.DjdatInfo.columns.mapSizeH|*), centerW: number, centerH: number}}
   */
  function getRectAngle (layout,isSpaceB) {
    var x = layout.xpos;
    var y = layout.ypos;
    var w = global.DataOp.EventList.Selected.Event.mapSizeW;
    var h = global.DataOp.EventList.Selected.Event.mapSizeH;

    if (layout.layout == 1) { // →
      w = w / 2;
      if (isSpaceB)
        x += w;
    }
    else if (layout.layout == 2) { // ↑
      h = h / 2;
      if (!isSpaceB)
        y += h;
    }
    else if (layout.layout == 3) { // ←
      w = w / 2;
      if (!isSpaceB)
        x += w;
    }
    else if (layout.layout == 4) { // ↓
      h = h / 2;
      if (isSpaceB)
        y += h;
    }

    return {
      xpos: x,
      ypos: y,
      width: w,
      height: h
    };
  }

  /*
   * 操作系
   */

  /**
   * タップ等でサークルを選択した時に、テンポラリ変数と環境変数の両方を更新し、表示に反映する
   * @param circleId
   */
  MapView.setSelectedCircleId = function (obj, eventType) {
    if (!obj || !obj.circleId)
      return;

    selectedId.circleId = obj.circleId;

    global.DataOp.EnvOp.save({
      'selectedBlockId': obj.blockId,
      'selectedCircleId': obj.circleId
    });

    if (eventType) {
      if (eventType == 'fav') {
        // お気に入りをタップした
        global.Pages.FavView.setSelectedFavSidebarItemStyle();
        jumpToSelectedSpace();
      }
      else if (eventType == 'map') {
        // 配置図をタップした
        mapPointer.updatePointer(obj, obj.spaceNoSub == '1');
        global.Pages.FavView.jumpToFavSidebarItem();
      }

      return global.GuiParts.DescBar.updateDescBar(obj.circleId);
    }
  };

  //

  /**
   * お気に入り別表示・状況別表示の変更があった
   */
  function updateFavDisplay (config) {
    global.Pages.FavView.updateFavSidebarStyle(config);
    global.Pages.MapView.updateFavBitmap();
  }

  /**
   * お気に入りの変更を表示に反映する（全部）
   */
  MapView.updateFav = function () {
    global.Pages.FavView.setFavItemsVisible(selectedId.dateId, selectedId.mapId);
    global.Pages.FavView.updateFavSidebar();
    global.Pages.MapView.updateFavBitmap();
  };

  /**
   * お気に入りの変更を表示に反映する（個別）
   * @param fav
   */
  MapView.updateFavItem = function (fav) {
    global.Pages.FavView.updateFavSidebarItem(fav);
    global.GuiParts.DescBar.updateDescItem(fav);
    global.Pages.MapView.updateFavBitmapItem(fav);
  };

  //---

  /**
   * 全お気に入りの中から表示中の配置図上にあるもののみに絞る
   */
  function filterFavCirclesInMap () {

    var mapFavKeys = [];

    global.DataOp.FavCircle.getSortedKeys().forEach(function (circleId) {
      var fav = global.DataOp.FavCircle.findFavCircle(circleId);
      if (!fav) {
        console.error(new Error('not found circleId: '+circleId));
        return;
      }

      if (selectedId.dateId != fav.Circle.dateId) {
        return;
      }

      var layout = global.DataOp.Layout.findLayout(fav.Circle.blockId, fav.Circle.spaceNo);
      if (layout.mapId != selectedId.mapId) {
        return;
      }

      mapFavKeys.push(circleId);
    });

    console.debug('fav filtered all='+global.DataOp.FavCircle.getSortedKeys().length+' length='+mapFavKeys.length);

    return mapFavKeys;
  }

  /**
   * お気に入りレイヤーを一括で書き換える
   */
  MapView.updateFavBitmap = function () {
    var ctx = elm.fav.getContext('2d');
    ctx.clearRect(0,0,elm.fav.width,elm.fav.height);

    var mapFavKeys = filterFavCirclesInMap();

    mapFavKeys.forEach(function (circleId) {
      var fav = global.DataOp.FavCircle.findFavCircle(circleId);
      putSpaceToFavBitmap(ctx, fav);
    });
  };

  /**
   * お気に入りレイヤー上のアイテムを個別に追加・更新・削除
   * @param fav
   */
  MapView.updateFavBitmapItem = function (fav) {
    var ctx = elm.fav.getContext('2d');
    putSpaceToFavBitmap(ctx, fav);
  };

  /**
   * お気に入りレイヤーにお気に入りスペースを描画していく
   * @param ctx
   * @param circleId
   */
  function putSpaceToFavBitmap(ctx, fav) {

    var layout = global.DataOp.Layout.findLayout(fav.Circle.blockId, fav.Circle.spaceNo);
    var r = getRectAngle(layout, fav.Circle.spaceNoSub == '1');

    if (fav.Fav.colorId == -1) {
      ctx.clearRect(r.xpos, r.ypos, r.width, r.height);
      return;
    }

    // FIXME
    var cond = global.DataOp.Condition.findCondition(fav.Cond.condId);
    var c = (function () {
      if (fav.Fav.colorId == -1) {
        return;
      }

      var chk = global.DataOp.FavColor.findFavColor(fav.Fav.colorId);
      if (chk) {
        // チェックされているもの。非表示状態になっていればグレーで
        return chk.enabled == 1 && (!cond || cond.enabled == 1) ? '#' + chk.checkColor : '#ddd';
      }

      // colorId=0(メモはされたがチェックは外されている)状態か、バグ
      return '#eee';
    })();

    ctx.fillStyle = c;
    ctx.fillRect(r.xpos, r.ypos, r.width, r.height);
  }

  /**
   * 配置図の各種ビットマップを描画する
   * @returns {*}
   */
  function updateMapImages () {

    function drawMapBitmap (canvas, mapType) {
      var obj = global.DataOp.MapImage.findMapImage(mapType,selectedId.dateId,selectedId.mapId);
      if (obj) {
        global.GuiParts.drawImageToCanvas(canvas, obj.image);
      }
      return Promise.resolve();
    }

    function genGridMap () {
      // グリッドの大きさより机のサイズが大きいと検索範囲が近傍４グリッドに限定できなくなるので、グリッドサイズを机のサイズに拡張する
      function getGridSide(d, t) {
        d = Math.floor(d);
        t = Math.ceil(t / 2);
        return t > d ? t : d;
      }

      // 各グリッドの大きさを決定
      var aspect = elm.map.width / elm.map.height;
      var measure = elm.map.width * elm.map.height;
      var ratio = measure / GRID_SIZE_BASE;
      divider = {
        x: getGridSide(Math.sqrt(ratio*aspect), global.DataOp.EventList.Selected.Event.mapSizeW),
        y: getGridSide(Math.sqrt(ratio/aspect), global.DataOp.EventList.Selected.Event.mapSizeH)
      };

      // グリッドマップの大きさを決定
      var grid = {
        w: Math.ceil(elm.map.width/divider.x),
        h: Math.ceil(elm.map.height/divider.y)
      };

      console.debug('grid size: map('+elm.map.width+'x'+elm.map.height+'), grid('+grid.w+'x'+grid.h+'='+(grid.w*grid.h)+'), divider('+divider.x+'x'+divider.y+'), table('+global.DataOp.EventList.Selected.Event.mapSizeW+'x'+global.DataOp.EventList.Selected.Event.mapSizeH+')');

      // 空マップを作成する
      gridMap = new Array(grid.w);
      for (var x = 0; x < grid.w; x++) {
        gridMap[x] = new Array(grid.h);
        for (var y = 0; y < grid.h; y++) {
          gridMap[x][y] = [];
        }
      }

      // マップを埋める
      var layoutMap = global.DataOp.Layout.getLayoutsAll();
      for (var p in layoutMap) {
        var o = layoutMap[p];
        if (!o || o.mapId != selectedId.mapId) {
          continue;
        }

        var x = getGridX(o.xpos);
        var y = getGridY(o.ypos);

        gridMap[x][y].push(o);
      }

      return Promise.resolve();
    }

    //---

    var tat = new global.Utils.TatCount('draw map');

    // フェードイン効果
    $('#mapCanvasContainer').css('display','none').fadeIn('slow');

    return Promise.resolve()
      .then(function () {
        return drawMapBitmap(elm.map, 'MAP');
      })
      .then(function () {
        tat.lap('DONE draw map bitmap');
        return drawMapBitmap(elm.genre, 'GNR');
      })
      .then(function () {
        tat.lap('DONE draw genre bitmap');
        return genGridMap();
      })
      .then(function () {
        tat.lap('DONE gen layout data gird');

        elm.overlay.width = elm.fav.width = elm.map.width;
        elm.overlay.height = elm.fav.height = elm.map.height;

        MapView.updateFavBitmap();
      })
      .then(function () {
        tat.lap('DONE draw fav bitmap');
        tat.end();
      })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      });
  }

  /**
   * サークルの位置を示す十字のポインタ
   */
  var mapPointer = (function () {

    var prevPoint = null;

    return {
      updatePointer: function (layout, isSpaceB) {

        var r = getRectAngle(layout,isSpaceB);

        var x = Math.floor(r.xpos + r.width/2) + 0.5;
        var y = Math.floor(r.ypos + r.height/2) + 0.5;
        var ctx = elm.overlay.getContext('2d');

        // 縮小表示した際に、1pxだと消えたりするのでとりあえず太くしておく
        ctx.lineWidth = 4;

        if (prevPoint) {
          // 前回引いた線を消す
          ctx.clearRect(prevPoint.x-prevPoint.w, 0, prevPoint.w*2, elm.overlay.height);
          ctx.clearRect(0, prevPoint.y-prevPoint.w, elm.overlay.width, prevPoint.w*2);
        }

        ctx.strokeStyle = 'rgba(255, 0, 0, 0.5)';
        ctx.beginPath();
        ctx.moveTo(0, y);
        ctx.lineTo(elm.overlay.width - 1, y);
        ctx.moveTo(x, 0);
        ctx.lineTo(x, elm.overlay.height - 1);
        ctx.closePath();
        ctx.stroke();

        prevPoint = {x:x,y:y,w:ctx.lineWidth};
      }
    };
  })();

  /**
   * 選択中のサークルが画面中央にくるようにスクロールさせる
   * @param layout
   */
  function scrollToCenter (layout) {

    var zoomRatio = getMapCanvasZoomRatio();

    var x = layout.xpos * zoomRatio - elm.$container.width() / 2;
    var y = layout.ypos * zoomRatio - elm.$container.height() / 2;

    x = (x > 0 ? x : 0);
    y = (y > 0 ? y : 0);

    elm.$container.animate({scrollLeft: x, scrollTop: y}, !global.Env.useAnimation ? 0 : 'slow')
  }

  /**
   * 指定のサークルスペースに移動する
   */
  function jumpToSelectedSpace () {
    function moveTo (obj) {
      var c = obj.Circle;
      mapPointer.updatePointer(c,c.spaceNoSub=='1');
      scrollToCenter(c);
    }

    //

    global.DataOp.Circle.getCircle(global.Env.selectedCircleId)
      .then(moveTo)
      .catch(function (e) {
        // スペースに対応するサークル情報がない（準備会スペースとか）もここに落ちる
        if (!e)
          console.error(e.stack || e);
      });
  }

  /**
   * 指定のサークルスペースに移動する
   */
  // FIXME moveToSelectedSpace と moveToSelectedSpaceXのなんやかや
  function moveToSelectedSpaceX () {

    var circleId = global.Env.selectedCircleId;

    var $favitem = tappingFavSidebarItem(circleId);
    if ($favitem) {
      // お気に入りにあればアイテムをタップして移動イベントをキック
      return;
    }
    //moveToFavSidebarItem();

    // お気に入りになければ移動処理を実行
    jumpToSelectedSpace();
    global.GuiParts.DescBar.updateDescBar(circleId);
  }

  /**
   * ジャンルレイヤーの表示／非表示の変更
   * @param visible
   */
  function setShowGenre () {
    var v = global.Env.showGenreBitmap;

    $('#mapViewNaviShowGenre').text(v ? 'ジャンル非表示' : 'ジャンル表示');

    var hide = 'dj-mapview-canvas-hide';

    if (v) {
      $(elm.genre).removeClass(hide);
    }
    else {
      $(elm.genre).addClass(hide);
    }
  }

  /**
   * ジャンルレイヤーの表示／非表示のトグル操作
   */
  function toggleShowGenre () {
    global.DataOp.EnvOp.save({
      'showGenreBitmap': ! global.Env.showGenreBitmap
    });

    setShowGenre();
  }

  /**
   * 配置図の透過／不透過の操作
   */
  function setTransparentMap () {

    var t = global.Env.transparentMapBitmap;

    $('#mapViewNaviTransparent').text(t ? '配置図非透過' : '配置図透過');

    var transparent = 'dj-mapview-canvas-transparent-70';

    if (t) {
      $(elm.map).addClass(transparent);
    }
    else {
      $(elm.map).removeClass(transparent);
    }
  }

  /**
   * 配置図の透過／不透過のトグル操作
   */
  function toggleTransparent() {
    global.DataOp.EnvOp.save({
      'transparentMapBitmap': ! global.Env.transparentMapBitmap
    });

    setTransparentMap();
  }

  var FIT_MODE_TEXT = ['等倍表示', '長辺に合わせる', '短辺に合わせる'];

  function getNextFitMode (mode) {
    return (1+mode) % FIT_MODE_TEXT.length;
  }

  /**
   * 配置図サイズの縦方向合わせの操作
   */
  function setFitToHeight (fitMode) {

    function fitTo (y) {
      if (y) {
        global.GuiParts.Css.insertStyle('.dj-mapview-canvas-container', { 'overflow-x': 'scroll' });
        global.GuiParts.Css.insertStyle('.dj-mapview-canvas', { 'height': '100%' });
      }
      else {
        global.GuiParts.Css.insertStyle('.dj-mapview-canvas-container', { 'overflow-y': 'scroll' });
        global.GuiParts.Css.insertStyle('.dj-mapview-canvas', { 'width': '100%' });
      }
    }

    var f = fitMode !== undefined ? fitMode : global.Env.mapFitToHeight;

    if (f == 1) {
      fitTo(elm.map.width < elm.map.height);
    }
    else if (f == 2) {
      fitTo(elm.map.width > elm.map.height);
    }
    else {
      global.GuiParts.Css.removeStyle('.dj-mapview-canvas-container');
      global.GuiParts.Css.removeStyle('.dj-mapview-canvas');
    }

    $('#mapViewNaviFitToHeight').text(FIT_MODE_TEXT[getNextFitMode(f)]);
  }

  /**
   * 表示上の座標（スクロール、タップ用）を決めるための比率
   * @param fitMode
   */
  function getMapCanvasZoomRatio (fitMode) {

    function fitTo (y) {
      if (y) {
        var sh = global.GuiParts.getScrollbarSize().height;
        return (elm.$container.height()-sh)/elm.map.height;
      }
      else {
        var sw = global.GuiParts.getScrollbarSize().width;
        return (elm.$container.width()-sw)/elm.map.width;
      }
    }

    var f = fitMode !== undefined ? fitMode : global.Env.mapFitToHeight;

    if (f == 1) {
      return fitTo(elm.map.width < elm.map.height);
    }
    else if (f == 2) {
      return fitTo(elm.map.width > elm.map.height);
    }

    return 1;
  }

  /**
   * 配置図サイズの縦方向合わせのトグル操作
   */
  function toggleFitToHeight () {
    global.DataOp.EnvOp.save({
      'mapFitToHeight': getNextFitMode(global.Env.mapFitToHeight)
    });

    setFitToHeight();
  }

  //---

  const FAV_COLOR_ENABLER_TEMPLATE = `
<label class="dj-navpanel-enabler">
    <input type="checkbox" class="dj-navpanel-enabler-input" data-#TYPE#-id="#ID#" data-role="none">
    <span class="ui-btn ui-corner-all"></span>
</label>
`;

  const FAV_COLOR_ENABLER_COLUMNS = 3;

  /**
   * ナビゲーションパネルの更新
   */
  MapView.updateNavPanel = function () {

    //
    function addFavEnabler ($listview) {
      //
      function tapFavEnablerHandler () {
        var $btn = $(this);
        var colorId = $btn.attr('data-color-id');
        var color = global.DataOp.FavColor.findFavColor(colorId);

        color.enabled = $btn.prop('checked') ? 1 : 0;

        updateFavDisplay({'colorId':colorId});

        global.DataOp.FavColor.setFavColorEnabled(colorId, color.enabled);
      }

      // ディバイダー
      $('<li>').attr({
        'data-role': 'list-divider'
      }).text('お気に入り表示選択').appendTo($listview);

      // ボタンの追加
      var $li = $('<li>').addClass('dj-navpanel-enabler-container').appendTo($listview);
      var $grid = $('<div>').addClass('ui-grid-'+String.fromCharCode('a'.charCodeAt(0) + (FAV_COLOR_ENABLER_COLUMNS-2))).appendTo($li);

      var html = '';
      global.DataOp.FavColor.getSortedKeys().forEach(function (colorId) {
        html += FAV_COLOR_ENABLER_TEMPLATE.replace(/#TYPE#/g, 'color').replace(/#ID#/g, colorId);
      });

      $grid.append(html).trigger('create');

      // ボタンの修飾
      $grid.children().each(function (index) {
        var $box = $(this);
        var $btn = $box.find('input');
        var $span = $box.find('span');

        var colorId = $btn.attr('data-color-id');
        var color = global.DataOp.FavColor.findFavColor(colorId);
        var abc = String.fromCharCode('a'.charCodeAt(0) + (index % FAV_COLOR_ENABLER_COLUMNS));

        $box.addClass('ui-block-'+abc);
        $btn.attr('checked',!!color.enabled);
        $span.addClass('dj-fav-color').attr('data-color-id',colorId).text(colorId);

        // ボタンON・OFF時のハンドラ
        $btn.on('change', tapFavEnablerHandler)
      });
    }

    //
    function addConditionEnabler ($listview) {
      //
      function tapCondEnablerHandler () {
        var $btn = $(this);
        var condId = $btn.attr('data-cond-id');
        var cond = global.DataOp.Condition.findCondition(condId);

        cond.enabled = $btn.prop('checked') ? 1 : 0;

        updateFavDisplay({'condId':condId});

        global.DataOp.Condition.setConditionEnabled(condId, cond.enabled);
      }

      // ディバイダー
      $('<li>').attr({
        'data-role': 'list-divider'
      }).text('状況別表示選択').appendTo($listview);

      // ボタンの追加
      var $li = $('<li>').addClass('dj-navpanel-enabler-container').appendTo($listview);
      var $grid = $('<div>').addClass('ui-grid-'+String.fromCharCode('a'.charCodeAt(0) + (FAV_COLOR_ENABLER_COLUMNS-2))).appendTo($li);

      var html = '';
      global.DataOp.Condition.getSortedKeys().forEach(function (condId) {
        html += FAV_COLOR_ENABLER_TEMPLATE.replace(/#TYPE#/g, 'cond').replace(/#ID#/g, condId);
      });

      $grid.append(html).trigger('create');

      // ボタンの修飾
      $grid.children().each(function (index) {
        var $box = $(this);
        var $btn = $box.find('input');
        var $span = $box.find('span');

        var condId = $btn.attr('data-cond-id');
        var cond = global.DataOp.Condition.findCondition(condId);
        var abc = String.fromCharCode('a'.charCodeAt(0) + (index % FAV_COLOR_ENABLER_COLUMNS));

        $box.addClass('ui-block-'+abc);
        $btn.attr('checked',!!cond.enabled);
        $span.text(cond.shortlabel);

        // ボタンON・OFF時のハンドラ
        $btn.on('change', tapCondEnablerHandler);
      });
    }

    //
    function addExtendLinks ($listview) {

      var buttons = [
        {label:'MAP選択', id:'mapViewNaviMapSelect', href:"#mapSelectNavPanel"},
        {label:'縦に合わせる', id:'mapViewNaviFitToHeight', href:"#", callback:toggleFitToHeight},
        {label:'ジャンル表示', id:'mapViewNaviShowGenre', href:"#", callback:toggleShowGenre},
        {label:'配置図透過', id:'mapViewNaviTransparent', href:"#", callback:toggleTransparent}
        //{label:'色付きスペースのみタップ可', id:'mapViewNaviTapLimit', href:"#", callback:null},
        //{label:'お気に入り並べ替え', id:'mapViewNaviFavSort', href:"#mapFavSortPanel", callback:null},
      ];

      global.GuiParts.NaviPanel.addButtons('その他', buttons, $listview);
    }

    //
    function addMapSelector ($listview,parent) {
      //
      function tapMapSelectorHandler () {
        var $li = $(this);
        var d = $li.attr('data-date-id');
        var m = $li.attr('data-map-id');
        global.DataOp.EnvOp.save({
          'selectedDateId': d,
          'selectedMapId': m
        });

        MapView.updateView();
      }

      // ディバイダー
      $('<li>').attr({
        'data-role': 'list-divider'
      }).text('MAP選択').appendTo($listview);

      // 配置図一覧
      var dobj = global.DataOp.EventList.Selected.Date;
      var dkeys = dobj.sortedKeys;
      var mobj = global.DataOp.EventList.Selected.Map;
      var mkeys = mobj.sortedKeys;

      dkeys.forEach(function (d) {
        mkeys.forEach(function (m) {
          var $li = $('<li>').attr({
            'data-icon': false,
            'data-date-id': d,
            'data-map-id': m
          });

          $('<a>').attr({
            'href': parent ? parent : '#',
            'data-rel': 'close'
          }).text(d+'日目 '+mobj[m].name).appendTo($li);

          $li.appendTo($listview);

          $li.on('tap', tapMapSelectorHandler);
        })
      });
    }

    //---

    var isMapNaviEnabled = global.DataOp.EventList.Selected.Date.sortedKeys.length > 1 || global.DataOp.EventList.Selected.Map.sortedKeys.length > 1;

    var $navlist = $('#mapNavPanelList').empty();

    addFavEnabler($navlist);
    addConditionEnabler($navlist);
    addExtendLinks($navlist);

    $navlist.listview().listview('refresh');

    if (!isMapNaviEnabled) {
      $('#mapViewNaviMapSelect').css({'display':'none'});
    }

    //---

    var $sellist = $('#mapSelectNavPanelList').empty();

    if (isMapNaviEnabled) {
      addMapSelector($sellist,'#mapNavPanel');
    }

    $sellist.listview().listview('refresh');

  }

  //---

  /**
   * タップした場所にあるスペース情報を取得する
   * @param e
   * @returns {*}
   */
  function getTappedSpace (e) {
    var tableW = global.DataOp.EventList.Selected.Event.mapSizeW;
    var tableH = global.DataOp.EventList.Selected.Event.mapSizeH;
    var offset = $(e.target.offsetParent).offset();

    var zoomRatio = getMapCanvasZoomRatio();

    var canvasPos = {
      x: (e.offsetX !== undefined ? e.offsetX : e.target.offsetParent.scrollLeft-offset.left+e.pageX)/zoomRatio,
      y: (e.offsetY !== undefined ? e.offsetY : e.target.offsetParent.scrollTop-offset.top+e.pageY)/zoomRatio
    };

    var gridPos = getGridPos(canvasPos.x, canvasPos.y);

    for (var x = gridPos.x; x >= gridPos.x - 1 && x >= 0; x--) {
      for (var y = gridPos.y; y >= gridPos.y - 1 && y >= 0; y--) {

        var gmap = gridMap[x] && gridMap[x][y];
        if (!gmap) {
          // 配置図外の領域？
          console.error('gridMap: out of range ',x,y);
          continue;
        }

        var len = gmap.length;
        for (var i=0; i<len; i++) {
          var space = gmap[i];
          if (
            (space.xpos<=canvasPos.x && canvasPos.x<(space.xpos+tableW)) &&
            (space.ypos<=canvasPos.y && canvasPos.y<(space.ypos+tableH))
          ) {
            space.spaceNoSub = (function () {
              if (space.layout == 1) { // →
                return (space.xpos+tableW/2 >= canvasPos.x) ? 0 : 1;
              }
              if (space.layout == 2) { // ↑
                return (space.ypos+tableH/2 > canvasPos.y) ? 1 : 0;
              }
              if (space.layout == 3) { // ←
                return (space.xpos+tableW/2 > canvasPos.x) ? 1 : 0;
              }
              if (space.layout == 4) { // ↓
                return (space.ypos+tableH/2 >= canvasPos.y) ? 0 : 1;
              }
            })();

            return space;
          }
        }
      }
    }
  }

  /**
   * 配置図上のグリッドをタップした時のハンドラ
   * @param e
   */
  function tapGridHandler (e) {

    var space = getTappedSpace(e);
    if (!space) {
      return;
    }

    global.DataOp.Circle.getCircleInSpace(space.blockId,space.spaceNo,space.spaceNoSub)
      .then(function (obj) {

        MapView.setSelectedCircleId(obj.Circle, 'map');

      })
      .catch(function (e) {
        // スペースに対応するサークル情報がない（準備会スペースとか）もここに落ちる
        if (e)
          console.error(e.stack || e);
      });
  }

  /**
   * お気に入りサイドバーアイテムをタップする
   * @param circleId
   */
  function tappingFavSidebarItem (circleId) {
    var $item = $('#mapFavItem-' + circleId);
    if ($item[0]) {
      $item.trigger('tap');
      return $item;
    }
  }

  /**
   * お気に入り色をCSSで設定する
   */
  function setFavColorCSSStyles () {
    if (!global.Env.selectedEventId) {
      return;
    }

    global.DataOp.FavColor.getSortedKeys().forEach(function (colorId) {
      var selector = '.dj-fav-color[data-color-id="'+colorId+'"]';
      global.GuiParts.Css.insertStyle(selector, {
        'background-color': '#'+global.DataOp.FavColor.findFavColor(colorId).checkColor+' !important'
      });
    });
  }

  /**
   * ペーストによるカット画像のアップデート
   * @param circleId
   * @param image
   * @returns {*}
   */
  function pasteCutImage (circleId,image) {

    return global.DataOp.CutImage.addCutImageAlt(MapView.$page.attr('id'), circleId, image)
      .then(function (c) {

        global.Pages.FavView.drawFavSidebarCutImage(circleId);
        global.Pages.CutView.drawCutImage(circleId);
        global.GuiParts.DescBar.updateDescCut(circleId);

        return Promise.resolve();

      })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      });
  }

  /**
   * テキストのペーストによる情報のアップデート
   * @param circleId
   * @param s
   */
  function pasteText (d) {

    function pasteUrl (s) {
      global.DataOp.Circle.addExtraUrl(s)
        .then(function (r) {
          return global.DataOp.FavCircle.getFavCircle(r.circleId);
        })
        .then(function (fav) {
          global.GuiParts.DescBar.updateDescLinks(fav.Fav.urls);
        })
        .catch(function (e) {
          if (e)
            console.error(e.stack || e);
        });
    }

    d.getAsString(function (s) {
      if (/^https?:\/\//.test(s)) {
        // URLの場合
        pasteUrl(s);
      }
    });
  }

  /**
   * 画像のペーストによる情報のアップデート
   * @param blob
   */
  function pasteImage (d) {
    // 差し替えカット画像のペースト
    if (selectedId.circleId) {
      var blob = d.getAsFile();
      var reader = new FileReader();
      reader.onload = function (event) {
        pasteCutImage(selectedId.circleId, event.target.result);
      };
      reader.readAsDataURL(blob);
    }
  }

  /**
   * キー押下イベントのハンドラ
   * @param e
   */
  MapView.keydownHandler = function (e) {
    if (48 <= e.keyCode && e.keyCode <= 57 || e.keyCode == 46) {
      global.DataOp.FavCircle.updateColor(e.keyCode == 46 ? -1 : e.keyCode - 48);
      return
    }

    if (global.GuiParts.ViewMode.isCutViewMode()) {
      if (33 == e.keyCode) {
        global.Pages.CutView.moveToNextBlock(false, e.keyCode);
      }
      else if (34 == e.keyCode) {
        global.Pages.CutView.moveToNextBlock(true, e.keyCode);
      }
    }
  };

  /**
   * クリップボードのペーストイベントのハンドラ
   * @param e
   */
  MapView.pasteHandler = function (e) {
    var items = e.originalEvent.clipboardData.items;
    for (var i=0; i<items.length; i++) {
      var d = items[i];
      if (/^image\/(?:png|jpeg)$/i.test(d.type)) {
        // 画像のペースト
        pasteImage(d);
      }
      else if (/^text\/plain$/i.test(d.type)) {
        // テキストのペースト
        pasteText(d);
      }
    }
  };

  /**
   * 画面リサイズ時のハンドラ
   */
  MapView.resizedWindowHandler = function () {
    global.Pages.CutView.resizedViewHandler();
  };

  //---

  /**
   * イベント切り替え時
   */
  MapView.updateMapViewOnChangeEvent = function () {
    // 選択状態の初期化
    clearSelection();

    // CSSの設定
    setFavColorCSSStyles();

    MapView.updateView();

    global.Pages.CutView.updateView();
  };

  /**
   * ページ表示切り替え時等の書き換え
   */
  MapView.updateView = function () {

    if (!global.Env.selectedEventId) {
      // イベントが選択されていない場合は書き換えの必要が無い
      return;
    }

    // 内部状態の保存
    var isEventChanged = ! isEventSelected();
    var isMapChanged = ! isMapSelected();
    var isCircleChanged = ! isCircleSelected();

    // 内部状態の切り替え
    changeMapSelection();

    if (isEventChanged) {
      // パネル類の初期化
      global.Pages.MapView.updateNavPanel();
      global.Pages.FavView.updateNavPanel();
      global.Pages.CutView.updateNavPanel();
      global.Pages.SearchView.updateNavPanel();

      global.GuiParts.FavColorSelectPanel.initPanel();

      // サークル詳細バーの初期化
      global.GuiParts.DescBar.clearDescBar();
    }

    if (isMapChanged) {
      // 配置図が切り替わった

      updateMapImages()
        .then(function () {

          global.Pages.FavView.setFavItemsVisible(selectedId.dateId, selectedId.mapId);

          if (isEventChanged) {
            global.Pages.FavView.updateFavSidebar();
          }

          // ジャンル表示・配置図透過等の切替
          setShowGenre();
          setTransparentMap();
          setFitToHeight();

          global.Pages.FavView.updateSidebarCutImage();

          return global.DataOp.Circle.getCircle(global.Env.selectedCircleId)
            .then(function (obj) {
              if (!obj) {
                return;
              }

              var c = obj.Circle;
              if (c.dateId == global.Env.selectedDateId && c.mapId == global.Env.selectedMapId) {
                // 選択中のサークルが切り替えた先の配置図にあったなら移動する
                global.Pages.FavView.jumpToFavSidebarItem();
                moveToSelectedSpaceX();
                if (isCircleChanged) {
                  //global.GuiParts.DescBar.updateDescBar(global.Env.selectedCircleId);
                }
              }
            });
        });
    }
    else if (isCircleChanged) {
      // 選択中のサークルが変わったのみ
      global.Pages.FavView.jumpToFavSidebarItem();
      moveToSelectedSpaceX();
      //global.GuiParts.DescBar.updateDescBar(global.Env.selectedCircleId);
    }
  };

  /**
   * 配置図ビューの初期化
   */
  MapView.initView = function () {

    console.debug('init MapView');

    // 頻繁に利用するエレメントの保存
    setElements();

    // 選択状態の初期化
    clearSelection();

    // 配置図のタップイベント設定
    $(elm.overlay).on('tap',tapGridHandler);
  };

  /**
   * 初期化
   */
  MapView.init = function () {

    // 各ビューの初期化
    global.Pages.MapView.initView();
    global.Pages.FavView.initView();
    global.Pages.CutView.initView();
    global.Pages.SearchView.initView();

  };

  //---

  global.Pages = global.Pages || {};

  global.Pages.MapView = MapView;

})(DjCRDView);
