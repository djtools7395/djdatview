// FIXME input が jQuery mobile に書き換えられるので pageinit 前後でセレクタに引っかかるエレメントが別のものになる
// FIXME androidでソフトウェアキーボードがポップアップすると orientation が変わってしまうが、調べた限りベストな解決方法はない模様

(function (global) {

  var SearchView = {};

  var elm = {
    $container: null,

    $searchbox: null,
    $searchbtn: null,

    $navipanel: null,
    $navilist: null
  };

  function setElements () {
    // ページ構築時
    elm.$container = $('#searchResult');

    elm.$searchbox = $('#searchBox');
    elm.$searchbtn = $('#searchButton');

    elm.$navipanel = $('#searchNavPane');
    elm.$navilist = $('#searchNavPanelList');
  }

  var searchResultCache = {
    eventId: null,
    results: {}
  };

  //---

  const SEARCH_RESULT_ITEM_TEMPLATE = `
<div id="searchItem-#CIRCLE_ID#" data-circle-id="#CIRCLE_ID#" class="dj-mapview-search-item ui-corner-all ui-shadow">
  <div class="dj-mapview-search-item-count"></div>
  <div class="dj-mapview-search-item-space-container">
    <div class="dj-mapview-search-item-date"></div>
    <div class="dj-mapview-search-item-space"></div>
  </div>
  <div class="dj-mapview-search-item-name-container">
    <div class="dj-mapview-search-item-name"></div>
    <div class="dj-mapview-search-item-blank">／</div>
    <div class="dj-mapview-search-item-author"></div>
  </div>
</div>`;

  /**
   * 検索結果を画面に表示する
   */
  function putSearchResultToPane () {

    // 空にする
    elm.$container.empty();

    // 検索結果がなければ終了
    var keys = (function () {
      if (!searchResultCache.results) {
        return;
      }

      return global.Utils.getSortedKeys(searchResultCache.results);
    })();

    if (!keys) {
      setSearchResultIsEmpty();
      return;
    }

    // 結果の入れ物を作る
    var innerHTML = '';
    keys.forEach(function (circleId) {
      innerHTML += SEARCH_RESULT_ITEM_TEMPLATE.replace(/#CIRCLE_ID#/g,circleId);
    });

    $(innerHTML).appendTo(elm.$container);

    return spawn(function* () {
      var children = elm.$container.children();
      for (var i=0; i<children.length; i++) {
        var e = children[i];

        var $box = $(e);
        var circleId = $box.attr('data-circle-id');

        var obj = yield global.DataOp.Circle.getCircle(circleId);

        var c = obj.Circle;
        var d = global.DataOp.EventList.Selected.Date[c.dateId];
        var b = global.DataOp.EventList.Selected.Block[c.blockId];
        var spaceNo = global.Utils.getSpaceNoStr(c,b.noab);
        $box.find('.dj-mapview-search-item-count').text(i+1);
        $box.find('.dj-mapview-search-item-date').text(d.day+'日');
        $box.find('.dj-mapview-search-item-space').text(b.name+spaceNo);
        $box.find('.dj-mapview-search-item-name').text(c.name);
        $box.find('.dj-mapview-search-item-author').text(c.author);

        var fav = global.DataOp.FavCircle.findFavCircle(circleId);
        var chk = fav && global.DataOp.FavColor.findFavColor(fav.Fav.colorId);
        $box.attr({
          'data-color-id': chk ? fav.Fav.colorId : ''
        });

        $box.on('click', tapResultItemHandler)
      }
    });
  }

  const SEARCH_BOX_TEMPLATE = `
<div class="dj-mapview-search-input-text">
  <input type="search" maxlength="80" id="searchBox" placeholder="検索テキスト">
</div>
<div class="dj-mapview-search-input-button">
  <input type="submit" id="searchButton" value="検索">
</div>
`;

  /**
   * 検索テキストボックスを追加する（inputがjQueryに書き換えられるタイミングがわかりづらいので動的に作ることにした）
   */
  function addSearchBox () {
    var $container = $('#searchBoxContainer');

    $(SEARCH_BOX_TEMPLATE).appendTo($container);

    $container.trigger('create');

    elm.$searchbox = $container.find('#searchBox');
    elm.$searchbtn = $container.find('#searchButton');
  }

  /**
   * 検索結果を空にする
   */
  function setSearchResultIsEmpty () {
    elm.$container.text('検索結果はありません。');
  }

  /**
   * 検索結果アイテムをタップした際のハンドラ
   */
  function tapResultItemHandler () {

    var circleId = $(this).attr('data-circle-id');

    setSelectedItemStyle(circleId);

    global.DataOp.Circle.getCircle(circleId)
      .then(function (r) {

        global.GuiParts.DescBar.updateDescBar(r.Circle.circleId);

        global.DataOp.EnvOp.save({
          'selectedDateId': r.Circle.dateId,
          'selectedMapId': global.DataOp.EventList.Selected.Block[r.Circle.blockId].mapId,
          'selectedBlockId': r.Circle.blockId,
          'selectedCircleId': r.Circle.circleId
        });

        global.Pages.MapView.updateView();

      })
      .catch(function (e) {
        // スペースに対応するサークル情報がない（準備会スペースとか）もここに落ちる
        if (e)
          console.error(e.stack || e);
      });
  }

  var setSelectedItemStyle = (function () {

    var $selectedItem = null;

    function setStyle (circleId) {
      if ($selectedItem) {
        $selectedItem.removeClass('selected');
      }

      $selectedItem = $('#searchItem-'+circleId).addClass('selected');

      return $selectedItem[0];
    }

    return setStyle;
  })();

  /**
   * ank pixiv toolの履歴からひりだした pixiv やら twitter やらのURLリストでサークルリストを検索しる
   * @param obj
   */
  function urlListLoadHandler (obj) {

    var file = obj.files.length > 0 && obj.files[0];
    if ( ! file ) {
      return;
    }

    var $input = $(obj);

    searchResultCache = {
      eventId: global.Env.selectedEventId,
      results: {}
    };

    return global.DataOp.URLList.load(file)
      .then(function (list) {
        var config = {
          title: '進捗',
          message: 'URLマッチング中...',
          maxOverall: 1,
          maxValue: list.length,
          value: 0
        };

        return global.GuiParts.ProgressDialog.show(config).then(function () {
          return Promise.resolve(list);
        });
      })
      .then(function (list) {
        function progress(r) {
          global.GuiParts.ProgressDialog.setValue(r.value);
        }

        return global.DataOp.Search.searchCirclesByURLList(list, progress);
      })
      .then(function (r) {
        searchResultCache.results = r;
        putSearchResultToPane();
      })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      })
      .then(function () {
        // finally
        $input.val('');
        global.GuiParts.ProgressDialog.close();
      });

  }

  /**
   * 保存した検索結果を読み込むハンドラ
   * @param obj
   */
  function searchResultLoadHandler (obj) {
    var file = obj.files.length > 0 && obj.files[0];
    if ( ! file ) {
      return;
    }

    var $input = $(obj);

    Promise.resolve().then(function () {

      return global.Utils.readAsText(file);

    }).then(function (text) {

      var json = JSON.parse(text);

      if (json.fileId != global.Const.FileId.Search) {
        return Promise.reject(new Error('unmatch file type fileId=' + json.fileId + ', currentId=' + global.Const.FileId.Search));
      }
      if (json.eventId != global.Env.selectedEventId) {
        return Promise.reject(new Error('unmatch event eventId=' + json.eventId + ', currentId=' + global.Env.selectedEventId));
      }

      searchResultCache = {
        eventId: global.Env.selectedEventId,
        results: json.results
      };

      putSearchResultToPane();

      return Promise.resolve();

    }).catch(function (e) {
      if (e)
        console.error(e.stack || e);
    }).then(function () {
      // finally
      $input.val('');
    });
  }

  /**
   * 検索結果をファイルに保存するハンドラ
   */
  function searchResultSaveHandler () {
    if (searchResultCache.eventId != global.Env.selectedEventId || Object.keys(searchResultCache.results).length == 0) {
      console.log('no search results');
      return;
    }

    // ファイルに保存
    var obj = {};
    obj.fileId = global.Const.FileId.Search;
    obj.eventId = searchResultCache.eventId;
    obj.results = {};
    for (var p in searchResultCache.results) {
      obj.results[p] = {};
    }
    global.Utils.saveAsJson('C' + global.Env.selectedEventId + '_search_result.json', obj);

  }

  /**
   * イベント切り替えなどの操作があったら書き換えを行う
   */
  SearchView.updateNavPanel = function () {

    //
    function addURLListMatchingButtton ($listview) {

      var buttons = [
        { label:'リストファイル読み込み', type:'file', accept:'.txt', callback:urlListLoadHandler }
      ];

      global.GuiParts.NaviPanel.addButtons('URLリストで検索', buttons, $listview);
    }

    //
    function addResultLoadButton ($listview) {
      var buttons = [
        { label:'読み込み', type:'file', accept:'.json', callback:searchResultLoadHandler }
      ];

      global.GuiParts.NaviPanel.addButtons('検索結果の読み込み', buttons, $listview);
    }

    //
    function addResultSaveButton ($listview) {
      var buttons = [
        { label:'保存', href:'#',callback:searchResultSaveHandler }
      ];

      global.GuiParts.NaviPanel.addButtons('検索結果の保存', buttons, $listview);
    }

    //

    resetSearchResultStylesAll();

    elm.$navilist.empty();

    addURLListMatchingButtton(elm.$navilist);
    addResultLoadButton(elm.$navilist);
    addResultSaveButton(elm.$navilist);

    elm.$navilist.listview().listview('refresh');

  };

  SearchView.updateFavItem = function (fav) {
    var circleId = fav.Fav.circleId;

    var canvas = document.getElementById('searchItem-'+circleId);
    if (!canvas) {
      return;
    }

    var fav = global.DataOp.FavCircle.findFavCircle(circleId);
    if (fav) {
      canvas.setAttribute('data-color-id', fav.Fav.colorId);
    }
    else {
      canvas.removeAttribute('data-color-id');
    }
  };

  /**
   * サーチボックスに入力があった際のハンドラ
   */
  function enterSearchBoxHandler ($textbox) {

    var text = $textbox.val();
    var names = [{'name':text, 'author':text }];

    // TODO URL検索とかメモ検索とか

    var tat = new global.Utils.TatCount('search');

    Promise.resolve()
      .then(function () {
        return global.DataOp.Search.searchCirclesByNameList(names,null);
      })
      .then(function (r) {
        tat.lap('fetched');
        searchResultCache.results = r;
        putSearchResultToPane();
        tat.end('drawn');
      })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      });
  }

  /**
   *
   * @param colorId
   */
  function updateSearchResultColorStyle (colorId) {
    var chk = colorId && global.DataOp.FavColor.findFavColor(colorId);
    if (!chk) {
      return;
    }

    var selector = '.dj-mapview-search-item[data-color-id="'+colorId+'"]';
    global.GuiParts.Css.insertStyle(selector, {'border-left-color': '#'+chk.checkColor});
  }

  /**
   * お気に入りサイドバーのアイテムの表示・非表示＆お気に入り色のスタイルをリセットする
   */
  function resetSearchResultStylesAll () {
    global.DataOp.FavColor.getSortedKeys().forEach(function (colorId) {
      updateSearchResultColorStyle(colorId);
    });
  }


  //---

  /**
   *
   */
  SearchView.initView = function () {

    console.debug('init SearchView');

    // 頻繁に利用するエレメントの保存
    setElements();

    //
    addSearchBox();

    // 結果表示を空にする
    setSearchResultIsEmpty();

    // 検索ボックスのイベント登録
    elm.$searchbox.on('change', function () {enterSearchBoxHandler(elm.$searchbox)});
    elm.$searchbtn.on('tap', function () {enterSearchBoxHandler(elm.$searchbox)});
  };

  //---

  global.Pages = global.Pages || {};

  global.Pages.SearchView = SearchView;

})(DjCRDView);
