(function (global) {

  var HistoryTool = {};

  HistoryTool.$page = $('#historyToolPage');

  var elm = {};

  function setElements () {
    elm.$container = $('#hisPane');
  }

  //---

  const SEARCH_HISTORY_ITEM_TEMPLATE = `
<div id="hisItem-#INDEX#" data-history-index="#INDEX#" class="dj-histool-item ui-corner-all ui-shadow">
  <div class="dj-histool-item-count"></div>
  <div class="dj-histool-item-datetime"></div>
  <div class="dj-histool-item-cond"></div>
  <div class="dj-histool-item-space"></div>
  <div class="dj-histool-item-name"></div>
  <div class="dj-histool-item-blank">／</div>
  <div class="dj-histool-item-author"></div>
</div>`;

  /**
   *
   */
  function putHistoryToPane () {

    elm.$container.empty();
    var innerHTML = '';

    return global.DataOp.FavCondition.getFavConditionHistoriesAll().then(function (conditions) {

      conditions.forEach(function (item,index) {
        innerHTML += SEARCH_HISTORY_ITEM_TEMPLATE.replace(/#INDEX#/g,index);
      });

      $(innerHTML).appendTo(elm.$container);

      return spawn(function* () {
        var children = elm.$container.children();
        for (var i=0; i<children.length; i++) {
          var e = children[i];

          var $box = $(e);
          var idx = $box.attr('data-history-index');
          var cond = conditions[idx];
          yield global.DataOp.Circle.getCircle(cond.circleId).then(function (obj) {
            try {
              var c = obj.Circle;
              var d = global.DataOp.Condition.findCondition(cond.condId);
              var b = global.DataOp.EventList.Selected.Block[c.blockId];
              var spaceNo = global.Utils.getSpaceNoStr(c, b.noab);
              var datetime = global.Utils.getDateTimeMD(new Date(cond.timestamp));

              $box.find('.dj-histool-item-count').text(i + 1);
              $box.find('.dj-histool-item-datetime').text(datetime);
              $box.find('.dj-histool-item-cond').text(d.shortlabel);
              $box.find('.dj-histool-item-space').text(b.name + spaceNo);
              $box.find('.dj-histool-item-name').text(c.name);
              $box.find('.dj-histool-item-author').text(c.author);
            }
            catch (e) {
              //
              console.error(e.stack || e)
            }
          });
        }
      });

    }).catch(function (e) {
      if (e)
        console.error(e.stack || e);
    });

  }

  /**
   * イベント切り替えなどの操作があったら書き換えを行う
   */
  function updateNavPanel () {

    var $navpanel = $('#historyToolNaviPanelList').empty();

    $navpanel.listview().listview('refresh');

  }

  //---

  /**
   *
   */
  HistoryTool.initView = function () {

    // 頻繁に利用するエレメントの保存
    setElements();

    HistoryTool.$page.on('pageshow', function () {
      putHistoryToPane();
    });

    updateNavPanel();
  };

  //---

  global.Pages = global.Pages || {};

  global.Pages.HistoryTool = HistoryTool;

})(DjCRDView);
