// FIXME リストの形状は適当なテーブルライブラリを見つけるまで仮置き
// FIMXE
(function (global) {

  var FavView = {};

  //---

  var elm = {
    $container: null,
    $navipanel: null,

    $favcontainer: null,
    $favlist: null
  };

  /**
   * 頻繁に利用する element を保存しておく
   */
  function setElements () {
    elm.$container = $('#favsPane');
    elm.$navipanel = $('#favNavPanelList');

    elm.$favcontainer = $('#mapFavsListContainer');
    elm.$favlist = $('#mapFavsList');
  }

  //---

  /**
   * イベント切り替えなどの操作があったら書き換えを行う
   */
  FavView.updateNavPanel = function () {

    //
    function addControlButtons ($listview) {
      var buttons = [
        {label:'配色編集', id:null, href:'#', callback:null},
        {label:'色をリセット', id:null, href:'#', callback:null},
        {label:'メモを削除', id:null, href:'#', callback:null},
        {label:'並べ替え', id:null, href:'#', callback:null}
      ];

      global.GuiParts.NaviPanel.addButtons('お気に入り操作', buttons, $listview);
    }

    //

    elm.$navipanel.empty();

    addControlButtons(elm.$navipanel);

    elm.$navipanel.listview().listview('refresh');

  };

  //---

  // お気に入りサイドバーアイテムのテンプレート
  const FAVSIDEBAR_ITEM_TEMPLATE = `
<div id="mapFavItem-#CIRCLE_ID#" data-circle-id="#CIRCLE_ID#" class="dj-mapview-favitem-container">
  <div class="dj-mapview-favitem-detail-container">
    <div class="dj-mapview-favitem-desc-container">
      <div class="dj-mapview-favitem-name-container">
        <div class="dj-mapview-favitem-space-container">
          <div class="dj-mapview-favitem-space"></div>
          <div class="dj-mapview-favitem-condition" id="mapFavItemCond-#CIRCLE_ID#"></div>
        </div>
        <div class="dj-mapview-favitem-author"></div>
        <div class="dj-mapview-favitem-name"></div>
      </div>
      <div class="dj-mapview-favitem-text-container">
        <div class="dj-mapview-favitem-text"></div>
      </div>
    </div>
    <div class="dj-mapview-favitem-memo-container">
    </div>
  </div>
  <div class="dj-mapview-favitem-cut-container dj-mapview-favitem-cut-size">
    <div>
      <canvas id="favCut-#CIRCLE_ID#" class="dj-mapview-favitem-cut-canvas"></canvas>
    </div>
  </div>
</div>`;

  /**
   * お気に入りサイドバーアイテムのHTMLの生成
   * @param circleId
   * @returns {string}
   */
  function createFavSidebarItemHTML (circleId) {
    return FAVSIDEBAR_ITEM_TEMPLATE.replace(/#CIRCLE_ID#/g,circleId);
  }

  /**
   * お気に入りサイドバーの更新
   */
    // FIXME setFavItemsVisibleとセットになっているがfavview.jsの修正をしていない
  FavView.updateFavSidebar = function () {

    var $listView = elm.$favlist;
    var keys = global.DataOp.FavCircle.getSortedKeys();

    var tat = new global.Utils.TatCount('update fav sidebar favs='+keys.length);

    // 既存のアイテムの削除
    $listView.empty();

    // お気に入りアイテムの追加
    var innerHTML = '';
    keys.forEach(function (circleId) {
      innerHTML += createFavSidebarItemHTML(circleId);
    });

    $listView[0].innerHTML = innerHTML;

    tat.lap('DONE append item');

    $listView.children().each(function () {
      updateFavSidebarItemText($(this));
    });

    tat.lap('DONE update item');

    // お気に入り色等の設定
    resetFavSidebarStylesAll();

    tat.lap('DONE set style');

    // カット画像の遅延ロードの設定
    setFavSidebarCutImageLoadLazy();

    tat.end();
  };

  /**
   * お気に入りサイドバー上のアイテムをタップした時のハンドラ
   * @param btn
   */
  function tapFavSidebarItemHandler (e) {

    var $btn = $(e.delegateTarget);

    var space = {
      circleId: $btn.attr('data-circle-id'),
      blockId: $btn.attr('data-block-id'),
      spaceNo: $btn.attr('data-space-no'),
      spaceNoSub: $btn.attr('data-space-no-sub')
    };

    global.Pages.MapView.setSelectedCircleId(space, 'fav');

  }

  /**
   * お気に入りサイドバーアイテムのHTMLを更新
   * @param circleId
   * @returns {*|jQuery}
   */
  function updateFavSidebarItemText ($btn) {

    var circleId = $btn.attr('data-circle-id');

    var fav = global.DataOp.FavCircle.findFavCircle(circleId);

    var block = global.DataOp.EventList.Selected.Block[fav.Circle.blockId];
    var spaceNo = global.Utils.getSpaceNoStr(fav.Circle,block.noab);
    var spaceLabel = block.name+spaceNo;
    var cond = global.DataOp.Condition.findCondition(fav.Cond.condId);
    var condText = cond ? cond.shortlabel : '－';
    var layout = global.DataOp.Layout.findLayout(fav.Circle.blockId,fav.Circle.spaceNo);

    FavView.updatePubsInfo(circleId, $btn);

    $btn.find('.dj-mapview-favitem-space').text(spaceLabel);
    $btn.find('.dj-mapview-favitem-condition').text(condText);
    $btn.find('.dj-mapview-favitem-author').text(fav.Circle.author || '-');
    $btn.find('.dj-mapview-favitem-name').text(fav.Circle.name || '-');
    $btn.find('.dj-mapview-favitem-text').text(fav.Circle.description);

    $btn.attr({
      'data-date-id': fav.Circle.dateId,
      'data-map-id': layout.mapId,
      'data-block-id': fav.Circle.blockId,
      'data-space-no': fav.Circle.spaceNo,
      'data-space-no-sub': fav.Circle.spaceNoSub,
      'data-color-id': fav.Fav.colorId,
      'data-cond-id': fav.Cond.condId
    }).css({
      'order': circleId
    });

    $btn.off('tap').on('tap', tapFavSidebarItemHandler);

    return $btn;
  }

  /**
   *
   * @param circleId
   * @param $btn
   */
  FavView.updatePubsInfo = function (circleId, $btn) {

    var $btn = $btn || $('#mapFavItem-'+circleId);
    if (!$btn[0]) {
      return;
    }

    var circleId = $btn.attr('data-circle-id');

    var fav = global.DataOp.FavCircle.findFavCircle(circleId);

    var pubs = (fav.Circle.Pubs || []).map(function (p) {
      var m = (function (v) {
        if (v == '2')
          return '限';
        return '';
      })(p.method);

      var c = (function (v) {
        if (v == '6')
          return '委';
        if (v == '2')
          return '既';
        if (v == '1')
          return '新';
        return '他';
      })(p.class);

      var t = (function (v) {
        if (v == '2')
          return '折';
        if (v == '3')
          return 'コ';
        if (v == '4')
          return 'ペ';
        if (v == '8')
          return 'CG';
        return '';
      })(p.type);

      return m+c+t;
    });

    var $memo = $btn.find('.dj-mapview-favitem-memo-container');
    $memo.text(pubs.join(", "));
    if (pubs.length) {
      $memo.addClass('dj-item-checked');
    }
    else {
      $memo.removeClass('dj-item-checked');
    }

    var limited_item = (fav.Circle.Pubs || []).some(function (p) {
      return p.method == '2';
    });

    var $desc = $btn.find('.dj-mapview-favitem-desc-container');
    if (limited_item) {
      $desc.addClass('dj-item-limited');
    }
    else {
      $desc.removeClass('dj-item-limited');
    }
  };


  /**
   * お気に入りサイドバーのアイテムの表示・非表示＆お気に入り色のスタイルを変更する（お気に入り・状況別）
   * @param config
   */
  FavView.updateFavSidebarStyle = function (config) {
    updateFavSidebarColorStyle(config.colorId);
    updateFavSidebarConditionStyle(config.condId);
  };

  /**
   *
   * @param colorId
   */
  function updateFavSidebarColorStyle (colorId) {
    var chk = colorId && global.DataOp.FavColor.findFavColor(colorId);
    if (!chk) {
      return;
    }

    var selector = '.dj-mapview-favitem-container[data-color-id="'+colorId+'"]';
    if (chk.enabled) {
      global.GuiParts.Css.insertStyle(selector, {'border-left-color': '#'+chk.checkColor});
    }
    else {
      global.GuiParts.Css.insertStyle(selector, {'display':'none'});
    }
  }

  /**
   *
   * @param condId
   */
  function updateFavSidebarConditionStyle (condId) {
    var cond = condId && global.DataOp.Condition.findCondition(condId);
    if (!cond) {
      return;
    }

    var selector = '.dj-mapview-favitem-container[data-cond-id="'+condId+'"]';
    if (cond.enabled) {
      global.GuiParts.Css.removeStyle(selector);
    }
    else {
      global.GuiParts.Css.insertStyle(selector, {'display':'none'});
    }
  }

  /**
   * お気に入りサイドバーのアイテムの表示・非表示＆お気に入り色のスタイルをリセットする
   */
  function resetFavSidebarStylesAll () {
    global.DataOp.FavColor.getSortedKeys().forEach(function (colorId) {
      updateFavSidebarColorStyle(colorId);
    });

    global.DataOp.Condition.getSortedKeys().forEach(function (condId) {
      updateFavSidebarConditionStyle(condId);
    });
  }

  /**
   * お気に入りサイドバーのアイテムの表示・非表示のスタイルを変更する（日付・配置図別）
   */
  FavView.setFavItemsVisible = function (dateId, mapId) {
    // 選択中の日付のみ表示する
    var dKeys = global.DataOp.EventList.Selected.Date.sortedKeys;
    dKeys.forEach(function (dId) {
      var selector = '.dj-mapview-favitem-container[data-date-id="'+dId+'"]';
      if (dId == dateId) {
        global.GuiParts.Css.removeStyle(selector);
      }
      else {
        global.GuiParts.Css.insertStyle(selector, {'display': 'none'});
      }
    });

    // 選択中のホールのみ表示する
    var mKeys = global.DataOp.EventList.Selected.Map.sortedKeys;
    mKeys.forEach(function (mId) {
      var selector = '.dj-mapview-favitem-container[data-map-id="'+mId+'"]';
      if (mId == mapId) {
        global.GuiParts.Css.removeStyle(selector);
      }
      else {
        global.GuiParts.Css.insertStyle(selector, {'display': 'none'});
      }
    });
  };

  /**
   * お気に入りカット画像を描画する
   * @param $c
   * @param cid
   */
  FavView.drawFavSidebarCutImage = function (circleId) {
    var canvas = document.getElementById('favCut-'+circleId);
    if (!canvas) {
      return Promise.resolve();
    }

    return global.DataOp.CutImage.getCutImageEntry(circleId).then(function (obj) {
      global.GuiParts.drawImageToCanvas(canvas, obj.altImage || obj.image);
    });
  };

  /**
   * お気に入りサイドバーアイテムを選択したら背景色を変える
   * @returns {HTMLElement}
   */
  FavView.setSelectedFavSidebarItemStyle = (function () {

    var $selectedItem = null;

    function setStyle () {
      if ($selectedItem) {
        $selectedItem.removeClass('selected');
      }

      $selectedItem = $('#mapFavItem-'+global.Env.selectedCircleId).addClass('selected');

      return $selectedItem[0];
    }

    return setStyle;
  })();

  /**
   * お気に入りサイドバーの表示中の者のみカット画像を描画する
   * @param target
   */
  function favSidebarScrollTimeoutHandler () {

    return spawn(function* () {

      var target = elm.$favcontainer[0];
      var tat = new global.Utils.TatCount('draw fav cut images');
      var drawCount = 0;

      var children = elm.$favlist.children();
      for (var i=0; i<children.length; i++) {
        var e = children[i];

        var $box = $(e);
        if ($box.attr('data-cut-image-loaded') || $box.css('display') == 'none') {
          continue;
        }

        var top = target.scrollTop;
        var bottom = top + target.clientHeight;
        if (top == bottom) {
          // 隠れている状態
          continue;
        }

        var t = $box[0].offsetTop - $box[0].parentElement.offsetTop;
        var b = t + $box[0].clientHeight;
        if (b < top || bottom < t) {
          continue;
        }

        $box.attr('data-cut-image-loaded',true);

        var circleId = $box.attr('data-circle-id');

        var fav = global.DataOp.FavCircle.findFavCircle(circleId);
        if (!fav) {
          continue;
        }

        ++drawCount;

        yield FavView.drawFavSidebarCutImage(circleId);
      }

      tat.end('drawn='+drawCount);

      yield;
    })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      });
  }

  /**
   *
   */
  FavView.updateSidebarCutImage = function () {
    setFavSidebarCutImageLoadLazy();
  };

  /**
   * お気に入りリストのカット画像の遅延ロードを設定する
   */
  var setFavSidebarCutImageLoadLazy = (function () {

    var tid = 0;

    function setLoadLazy (interval) {
      function setLazyTimer () {
        if (tid) {
          clearTimeout(tid);
        }

        tid = setTimeout(function () {favSidebarScrollTimeoutHandler()}, interval);
      }

      // FIXME リサイズで表示範囲が変更されたときにも実行が必要
      setLazyTimer(100);

      elm.$favcontainer.off('scroll').on('scroll', function () {
        setLazyTimer(300)
      });
    }

    return setLoadLazy;
  })();

  /**
   * お気に入りサイドバーのアイテムを追加・更新・削除
   * @param fav
   */
  FavView.updateFavSidebarItem = function (fav) {

    var $listView = elm.$favlist;

    var circleId = fav.Circle.circleId;
    var colorId = fav.Fav.colorId;
    var condId = fav.Cond.condId;

    if (colorId == -1) {
      // 削除
      $listView.children('#mapFavItem-' + circleId).remove();
      return;
    }

    var $div = $('#mapFavItem-' + circleId);
    if (!$div[0]) {
      // 新規アイテムの追加
      var innerHTML = createFavSidebarItemHTML(circleId);
      $div = $(innerHTML).appendTo($listView);
      FavView.drawFavSidebarCutImage(circleId);
    }

    updateFavSidebarItemText($div);

    $div.attr('data-color-id', colorId);
    $div.attr('data-cond-id', condId);

    // カット画像の遅延ロードの設定
    setFavSidebarCutImageLoadLazy();
  };

  /**
   * 指定のお気に入りサイドバーアイテムに移動する
   * @param circleId
   */
  FavView.jumpToFavSidebarItem = function () {

    var favItem = FavView.setSelectedFavSidebarItemStyle();
    if (!favItem) {
      return;
    }

    if ($(favItem).css('display') == 'none') {
      return;
    }

    var top = favItem.offsetTop - elm.$favlist[0].offsetTop - favItem.offsetHeight / 4;
    top = top > 0 ? top : 0;
    elm.$favcontainer.animate({scrollTop: top}, !global.Env.useAnimation ? 0 : 'fast');

    return favItem;
  };


  //---

  /**
   * 初期化
   */
  FavView.initView = function () {

    // ページ構築時
    setElements();

  };

  //---

  global.Pages = global.Pages || {};

  global.Pages.FavView = FavView;

})(DjCRDView);
