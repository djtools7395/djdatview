
(function (global) {

  var SandboxFS = {};

  window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;

  //
  var MIN_QUOTA_SIZE = 100*1024*1024;

  var fs = null;

  //
  function setQuota (obj) {
    function foo (resolve, reject) {
      function onError (e) {
        reject(e);
      }

      function onSuccess (bytes) {
        resolve(bytes);
      }

      navigator.webkitPersistentStorage.requestQuota(obj.quota, onSuccess, onError);
    }

    return new Promise(foo);
  }

  //
  function getFileSystem (bytes) {
    function foo (resolve, reject) {
      function onError (e) {
        reject(e);
      }

      function onSuccess(newfs) {
        fs = newfs;
        resolve(fs);
      }

      window.requestFileSystem(window.PERSISTENT, bytes, onSuccess, onError);
    }

    return new Promise(foo);
  }

  /**
   *
   * @returns {Promise.<T>}
   */
  SandboxFS.open = function (quota) {
    var obj = {
      quota: quota > 0 ? quota : MIN_QUOTA_SIZE
    };

    return Promise.resolve(obj)
      .then(setQuota)
      .then(getFileSystem);
  };

  /**
   *
   * @returns {Promise}
   */
  SandboxFS.getQuotaInfo = function () {
    function foo (resolve, reject) {
      function onError (e) {
        reject(e);
      }

      function onSuccess (usage, quota) {
        resolve({
          usage: usage,
          quota: quota
        });
      }

      navigator.webkitPersistentStorage.queryUsageAndQuota(onSuccess, onError);
    }

    return new Promise(foo);
  };

  /**
   *
   * @param option
   * @returns {Promise}
   */
  function getFileEntry (filename, option) {
    function foo (resolve, reject) {
      function onError (e) {
        reject(e);
      }

      function onSuccess (fileEntry) {
        resolve(fileEntry);
      }

      fs.root.getFile(filename, option, onSuccess, onError);
    }

    return new Promise(foo);
  }

  /**
   *
   * @param filename
   * @returns {Promise.<T>}
   */
  SandboxFS.openFileEntry = function (filename) {
    return getFileEntry(filename, {create: false});
  };


  /**
   *
   * @param filename
   * @returns {Promise.<T>}
   */
  SandboxFS.openFile = function (filename) {
    function onSuccess (fileEntry) {
      function foo (resolve) {
        fileEntry.file(function (file) {
          resolve(file);
        });
      }

      return new Promise(foo);
    }

    return getFileEntry(filename, {create: false}).then(onSuccess);
  };

  /**
   *
   * @param filename
   * @returns {Promise.<T>}
   */
  SandboxFS.openFileWriter = function (filename, overwrite) {
    function onSuccess (fileEntry) {
      function foo (resolve, reject) {
        fileEntry.createWriter(
          function (fileWriter) {
            resolve(fileWriter);
          },
          function (e) {
            reject(e);
          }
        );
      }

      return new Promise(foo);
    }

    function onOverwrite (fileWriter) {
      if (!overwrite) {
        return Promise.resolve(fileWriter);
      }

      return truncate(fileWriter);
    }

    return getFileEntry(filename, {create: true})
      .then(onSuccess)
      .then(onOverwrite);
  };

  /**
   *
   * @param fileWriter
   * @returns {Promise}
   */
  function truncate (fileWriter) {
    function foo (resolve, reject) {
      var wr = fileWriter.onwriteend;
      var er = fileWriter.onerror;

      fileWriter.onwriteend = function () {
        fileWriter.onwriteend = wr;
        fileWriter.onerror = er;

        resolve(fileWriter);
      };

      fileWriter.onerror = function (e) {
        fileWriter.onwriteend = wr;
        fileWriter.onerror = er;

        reject(e);
      };

      fileWriter.truncate(0);
    }

    return new Promise(foo);
  }

  /**
   *
   * @param foldername
   * @returns {*}
   */
  SandboxFS.createFolder = function (foldername) {
    function createFoldersAll (prevPromise, curValue) {
      function createChildFolder (parentEntry) {
        function foo (resolve, reject) {
          function onError (e) {
            reject(e);
          }

          function onSuccess (dirEntry) {
            resolve(dirEntry);
          }

          parentEntry.getDirectory(curValue, {create: true}, onSuccess, onError);
        }

        return new Promise(foo);
      }

      return prevPromise.then(createChildFolder);
    }

    return foldername.replace(/\/+/, '/', 'g').split("/").reduce(createFoldersAll, Promise.resolve(fs.root));
  };

  /**
   *
   * @param foldername
   * @returns {Promise}
   */
  SandboxFS.removeFolder = function (foldername) {
    function foo (resolve, reject) {
      function onError (e) {
        reject(e);
      }

      function onSuccess (dirEntry) {
        dirEntry.removeRecursively(
          function () {
            resolve();
          },
          function (e) {
            reject(e);
          }
        );
      }

      fs.root.getDirectory(foldername.replace(/\/+/, '/', 'g'), {create: false}, onSuccess, onError);
    }

    return new Promise(foo);
  };

  /**
   *
   * @param path
   * @param uint8a
   * @param chunk
   * @returns {Promise.<T>}
   */
  SandboxFS.writeUint8Array = function (fileWriter, uint8a, chunk, onprogress) {

    var wr = fileWriter.onwriteend;
    var er = fileWriter.onerror;

    var CHUNK = chunk > 0 ? chunk : 10000000;

    return spawn(function* () {
      for (var top=0; top<uint8a.length; top+=CHUNK) {
        var len = top + CHUNK < uint8a.length ? CHUNK : (uint8a.length - top);

        yield new Promise(function (resolve, reject) {
          fileWriter.onwriteend = function () {
            fileWriter.onwriteend = wr;
            fileWriter.onerror = er;

            resolve();
          };

          fileWriter.onerror = function (e) {
            fileWriter.onwriteend = wr;
            fileWriter.onerror = er;

            reject(e);
          };

          fileWriter.seek(fileWriter.length);
          fileWriter.write(new Blob([uint8a.subarray(top, top + len)], {type: 'application/octet-stream'}));
        });

        console.log('copy progress=' + (top + len) + ' / ' + uint8a.length);

        if (onprogress) {
          onprogress(top + len, uint8a.length);
        }
      }
    });
  };

  /**
   *
   * @param fileWriter
   * @param blob
   * @returns {Promise}
   */
  SandboxFS.writeBlob = function (fileWriter, blob) {

    var wr = fileWriter.onwriteend;
    var er = fileWriter.onerror;

    return new Promise(function (resolve, reject) {
      fileWriter.onwriteend = function () {
        fileWriter.onwriteend = wr;
        fileWriter.onerror = er;

        resolve();
      };

      fileWriter.onerror = function (e) {
        fileWriter.onwriteend = wr;
        fileWriter.onerror = er;

        reject(e);
      };

      fileWriter.seek(fileWriter.length);
      fileWriter.write(blob);
    });
  };

  //

  global.SandboxFS = SandboxFS;

})(this);
