(function (global) {

  var Css = {};

  var sheet = $('#dynStyle')[0].sheet;

  /**
   * このセレクタは何番目のルールか？
   * @param selector
   * @param mediaRules
   * @returns {number}
   */
  function getIndex (selector, mediaRules) {
    var rules = mediaRules && mediaRules.cssRules || sheet.cssRules;
    for (var i = 0; i < rules.length; i++) {
      if (selector === rules[i].selectorText) {
        return i;
      }
    }
  }

  /**
   * MediaQueryを整形する
   * @param query
   */
  function replaceMediaQuery (query) {
    return query.replace(/^\s*(.+?)\s*$/,'$1').replace(/([:(])\s+/g,'$1').replace(/\s+([)])/g,'$1').replace(/\s+/g, ' ');
  }

  /**
   * INSERT or REPLACE
   * @param selector
   * @param rule
   * @param mediaQuery
   */
  Css.insertStyle = function (selector, rule, mediaQuery) {
    if (!sheet) {
      return;
    }

    Css.removeStyle(selector, mediaQuery);

    var a = selector;
    a += "{";
    for (var p in rule) {
      a += p+":"+rule[p]+";"
    }
    a += "}";

    if (mediaQuery) {
      a = '@media '+mediaQuery + '{' + a + '}';
    }
    sheet.insertRule(a, 0);
  };

  /**
   * REMOVE
   * @param selector
   * @param mediaQuery
   */
  Css.removeStyle = function (selector, mediaQuery) {
    if (!sheet) {
      return;
    }

    if (mediaQuery) {
      var mq = replaceMediaQuery(mediaQuery);
      var mediaRules = sheet.cssRules;
      for (var mqIndex = 0; mqIndex < mediaRules.length; mqIndex++) {
        if (!mediaRules[mqIndex].media || replaceMediaQuery(mediaRules[mqIndex].media.mediaText) !== mq) {
          continue;
        }

        var rules = sheet.cssRules[mqIndex];
        var index = getIndex(selector, rules);
        if (index === undefined) {
          continue;
        }

        rules.deleteRule(index);

        if (rules.cssRules.length == 0) {
          sheet.deleteRule(mqIndex);
        }
      }
    }
    else {
      var index = getIndex(selector);
      if (index === undefined) {
        return;
      }

      sheet.deleteRule(index);
    }
  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.Css = Css;

})(DjCRDView);
