(function (global) {

  var PageFooter = {};

  //---

  /**
   * ページフッタの追加
   * @param $page
   * @param pageId
   * @returns {*}
   */
  PageFooter.add = function ($page,pageId) {
    var $footer = $page.children('[data-role="footer"]')
      .attr({
        'id': pageId.replace(/Page/, 'Footer'),
        'data-theme': 'b'
      });

    // フッターは使わないのであったら削除
    if ($footer) {
      $page[0].removeChild($footer[0]);
      $footer = null;
    }

    return $footer;
  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.PageFooter = PageFooter;

})(DjCRDView);