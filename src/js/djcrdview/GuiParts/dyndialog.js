(function (global) {

  var DynDialog = {};

  //---

  const DYNAMIC_DIALOG_TEMPLATE = `
<section data-role="popup" data-history="false" data-dismissible="false" id="dynDialog" class="dj-dyn-dialog">
    <header data-role="header" data-theme="b">
        <h2 id="dynDialogTitle">DIALOG</h2>
    </header>
    <article data-role="content" class="ui-content">
        <h3 id="dynDialogContent">CONTENT</h3>
        <p id="dynDialogMessage">MESSAGE</p>
        <form id="dynDialogCheckbox"></form>
        <div id="dynDialogButton"></div>
    </article>
</section>
`;

  const DYNAMIC_DIALOG_CHECKBOX_TEMPLATE = `
<form class="ui-mini dj-dyn-dialog-chechbox-container">
    <label for="dynDialogChk-#INDEX#"></label>
    <input type="checkbox" id="dynDialogChk-#INDEX#" class="ui-shadow">
</form>
`;

  const DYNAMIC_DIALOG_BUTTON_TEMPLATE = `
<a href="#" data-rel="back" id="dynDialogBtn-#INDEX#" class="ui-btn ui-corner-all ui-btn-b ui-shadow"></a>
`;

  /**
   * メッセージダイアログを開く
   * @param targetid
   * @param config
   */
  DynDialog.show = function (config) {

    //
    function addCheckbox ($dialog, checks) {
      if (!checks) {
        return;
      }

      var $checkBoxContainer = $dialog.find('#dynDialogCheckbox');
      checks.forEach(function (chk, index) {
        var html = DYNAMIC_DIALOG_CHECKBOX_TEMPLATE.replace(/#INDEX#/g,index+1);
        var $box = $(html).appendTo($checkBoxContainer);
        $box.find('input').attr({
          name: chk.name,
          checked: chk.checked
        });
        $box.find('label').text(chk.label);
      });
      $checkBoxContainer.trigger('create');
    }

    //
    function addButton ($dialog, buttons) {
      var $buttonContainer = $dialog.find('#dynDialogButton');
      (buttons || [{label:'CLOSE'}]).forEach(function (btn, index) {
          var html = DYNAMIC_DIALOG_BUTTON_TEMPLATE.replace(/#INDEX#/g,index+1);
          var $box = $(html).appendTo($buttonContainer).text(btn.label);

          $box.on('tap', function () {
            (function (b) {
              selected = b;
            })(btn);
          });
        }
      );
      $buttonContainer.trigger('create');
    }

    //

    // 表示先ページ
    var $page = global.GuiParts.getActivePageModule();
    $page = $page && $page.$page;
    if (!$page) {
      return Promise.reject(new Error('not found active page'));
    }

    // 押したボタン
    var selected = null;

    return new Promise(function (resolve,reject) {
      $(document).ready(function () {

        $("#dynDialog").remove();

        var $dialog = $(DYNAMIC_DIALOG_TEMPLATE).trigger('create');

        $dialog.find('#dynDialogTitle').text(config.title);
        $dialog.find('#dynDialogContent').text(config.content).css('display',config.content ? 'block':'none');
        $dialog.find('#dynDialogMessage').text(config.message).css('display',config.message ? 'block':'none');;

        // チェックボックス
        addCheckbox($dialog, config.checks);

        // ボタン
        addButton ($dialog, config.buttons);

        // 閉じたときの処理
        var dialogConfig = {
          afterclose: function () {
            $dialog.remove();

            if (!selected) {
              // ×ボタンで閉じた？
              return reject();
            }

            var obj = {
              value: selected.value,
              checked: {}
            };

            $dialog.find('input[type="checkbox"]').each(function () {
              var $input = $(this);
              obj.checked[$input.attr('name')] = $input.prop('checked');
            });

            resolve(obj);
          }
        };

        return $dialog.appendTo($page).enhanceWithin().popup(dialogConfig).popup(
          "open", {
            transition: "pop",
            positionTo: "window"
          });
      });
    });
  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.DynDialog = DynDialog;

})(DjCRDView);