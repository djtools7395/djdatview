(function (global) {

  var PubsEditPanel = {};

  var $panel;

  //---

  const PUBS_ADD_BUTTON_TEMPLATE = `
<div>
  <button class="dj-navpanel-selector ui-btn ui-corner-all">頒布物を追加する</button>
</div>
`;

  const PUBS_ITEM_TEMPLATE = `
<div class="dj-pubs-item" data-pubs-id="#ID#">
  <div class="dj-pubs-item-desc">
    <div class="dj-pubs-item-title">#TITLE#</div>
    <div class="dj-pubs-item-detail">
      <div class="dj-pubs-item-method dj-pubs-item-mark"></div>
      <div class="dj-pubs-item-class dj-pubs-item-mark"></div>
      <div class="dj-pubs-item-category dj-pubs-item-mark"></div>
      <div class="dj-pubs-item-num"></div>
    </div>
  </div>
  <div class="dj-pubs-item-links">
  </div>
</div>
`;
  var latestPubsId = 0;

  /**
   *
   * @param $listView
   * @returns {*}
   */
  function addPubsList ($listView) {

    return global.DataOp.Circle.getCircle(global.Env.selectedCircleId)
      .then(function (data) {

        global.GuiParts.NaviPanel.addDivider('頒布物', $listView);

        if (!data || !data.Circle.Pubs || !data.Circle.Pubs.length ) {
          latestPubsId = 0;

          var buttons = [
            {label:'頒布物未登録', id:'', href:"#", callback:null}
          ];

          global.GuiParts.NaviPanel.addButtons(null, buttons, $listView);
        }
        else {
          latestPubsId = 0;

          var $container = $('<li class="dj-pubs-item-container"></li>').appendTo($listView);

          data.Circle.Pubs.forEach(function (p) {
            if (p.id > latestPubsId) {
              latestPubsId = p.id;
            }

            var $item = $(PUBS_ITEM_TEMPLATE.replace(/#ID#/,p.id).replace(/#TITLE#/,p.title || '-')).appendTo($container);

            var $title = $item.find('.dj-pubs-item-title');
            var $mtd = $item.find('.dj-pubs-item-method');
            var $cls = $item.find('.dj-pubs-item-class');
            var $cat = $item.find('.dj-pubs-item-category');
            var $num = $item.find('.dj-pubs-item-num');

            if (p.urls && p.urls.length>0) {
              $title.attr('href', p.urls[0]);
            }

            if (p.method>1) {
              $mtd.text(global.Const.Distribution.method[p.method-1].substr(0,2));
            }

            if (p.class>0) {
              $cls.text(global.Const.Distribution.class[p.class-1].substr(0,2));
            }

            if (p.category>0) {
              $cat.text(global.Const.Distribution.category[p.category-1].substr(0,2));
            }

            if (p.num > 0) {
              $num.text('x'+p.num);
            }

            /*
            $item.on('tap', function () {
              var $e = $(this);
              var pubsId = $e.attr('data-pubs-id');

              var a = data.Circle.Pubs.filter(function (p) {
                return p.id == pubsId;
              }).shift();;

              if (a && a.urls.length > 0) {
                open(a.urls[0], '_blank');
              }
            });
            */

            $item.on('click', function () {
              var $e = $(this);
              var pubsId = $e.attr('data-pubs-id');

              var a = data.Circle.Pubs.filter(function (p) {
                return p.id == pubsId;
              }).shift();

              if (a) {
                var config = {
                  title: '頒布物編集',
                  id: a.id,
                  content: a,
                  buttons: [
                    {label: '変更する', value:1},
                    {label: '削除する', value:2},
                    {label: 'キャンセル'}
                  ]
                };

                global.GuiParts.PubsEditDialog.show(config)
                  .then(function (value) {
                    if (value && value.value == 1) {
                      return global.DataOp.Circle.insertOrReplacePubsInfo(value.content);
                    }
                    else if (value && value.value == 2) {
                      return global.DataOp.Circle.removePubsInfo(value.content.id);
                    }

                    return Promise.reject();
                  })
                  .then(function () {
                    return global.DataOp.FavCircle.getFavCircle(global.Env.selectedCircleId);
                  })
                  .then(function () {
                    global.Pages.FavView.updatePubsInfo(global.Env.selectedCircleId, null);
                  })
                  .then(function () {
                    return global.GuiParts.DescBar.updateDescBar(global.Env.selectedCircleId);
                    //return PubsEditPanel.openNavPanel();
                  })
                  .catch(function (e) {
                    if (e) {
                      console.error(e.stack || e);
                    }
                    else {
                      console.error(new Error('cancelled'))
                    }
                  });
              }
            });
          });

          $listView.listview().listview('refresh');
        }
      });
  }

  /**
   *
   * @param $listView
   */
  function addPubAddButton ($listView) {
    // ディバイダー
    $('<li>').attr({
      'data-role': 'list-divider'
    }).text('頒布物の追加').appendTo($listView);

    var $button = $(PUBS_ADD_BUTTON_TEMPLATE)
      .find('button')
      .css({'background-color':'gray'})
      .appendTo($listView);

    $button.on('click', function () {

      $panel.panel("close");

      var config = {
        title: '頒布物追加',
        id: latestPubsId+1,
        buttons: [
          {label: '追加する', value:1},
          {label: 'キャンセル'}
        ]
      };

      global.GuiParts.PubsEditDialog.show(config)
        .then(function (value) {
          if (!value || !value.value) {
            return Promise.reject();
          }

          return global.DataOp.Circle.insertOrReplacePubsInfo(value.content);
        })
        .then(function () {
          return global.DataOp.FavCircle.getFavCircle(global.Env.selectedCircleId);
        })
        .then(function () {
          global.Pages.FavView.updatePubsInfo(global.Env.selectedCircleId, null);
        })
        .then(function () {
          return global.GuiParts.DescBar.updateDescBar(global.Env.selectedCircleId);
        })
        .catch(function (e) {
          if (e) {
            console.error(e.stack || e);
          }
          else {
            console.error(new Error('cancelled'))
          }
        });
    });
  }

  /**
   *
   * @returns {*|jQuery}
   */
  PubsEditPanel.openNavPanel = function () {

    var $listView = $('#pubsEditPanelList').empty();

    addPubsList($listView)
      .then(function () {
        addPubAddButton($listView);

        $listView.listview().listview('refresh');

        $panel = $('#pubsEditPanel').attr('data-theme', 'a').panel().trigger('updatelayout');

        return $panel.panel('open');
      });

  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.PubsEditPanle = PubsEditPanel;

})(DjCRDView);
