(function (global) {

  var Bitmap = {};

  //---

  const USE_RESIZE_JS = true;

  const IMAGE_MUL = 2;
  const BORDER_WIDTH = 2 * IMAGE_MUL;

  /**
   * ビットマップをカットサイズに収まるように加工する
   * @param image
   * @param callback
   */
  Bitmap.fitBitmapToCutSize = function (image) {
    return new Promise(function (resolve,reject) {
      $(new Image()).on('load', function () {
        // ペーストした画像をカットサイズに合わせて縮小or拡大する
        var img = this;

        var source = document.createElement('canvas');
        source.width = img.width;
        source.height = img.height;
        var sourceCtx = source.getContext('2d');
        sourceCtx.drawImage(img, 0, 0, source.width, source.height);

        // 縦長は縦長で、横長は横長で加工・保存してロード時にサイズ調整する
        var c = document.createElement('canvas');
        if (source.width <= source.height) {
          c.width = global.DataOp.EventList.Selected.Event.cutSizeW;
          c.height = global.DataOp.EventList.Selected.Event.cutSizeH;
        }
        else {
          c.width = global.DataOp.EventList.Selected.Event.cutSizeH;
          c.height = global.DataOp.EventList.Selected.Event.cutSizeW;
        }
        c.width *= IMAGE_MUL;
        c.height *= IMAGE_MUL;

        var r = (function () {
          if (c.width / c.height > img.width / img.height) {
            return {w: Math.ceil(c.height / img.height * img.width), h: c.height};
          }
          else {
            return {w: c.width, h: Math.ceil(c.width / img.width * img.height)};
          }
        })();

        c.width = r.w;
        c.height = r.h;

        var ra = {
          x: Math.ceil((c.width - r.w) / 2) + BORDER_WIDTH,
          y: Math.ceil((c.height - r.h) / 2) + BORDER_WIDTH,
          w: r.w-BORDER_WIDTH*2,
          h: r.h-BORDER_WIDTH*2
        };

        var ctx = c.getContext('2d');
        ctx.fillStyle = '#000000';
        ctx.fillRect(0, 0, c.width, c.height);

        if (USE_RESIZE_JS && Resize) {

          var rs = new Resize(source.width, source.height, ra.w, ra.h, true, true, false, function (sourceData) {
            var resizedBuffer = getResizedBuffer(ctx.getImageData(ra.x, ra.y, ra.w, ra.h), sourceData);
            ctx.putImageData(resizedBuffer, ra.x, ra.y);
            resolve(c);
          });

          rs.resize(sourceCtx.getImageData(0, 0, source.width, source.height).data);
        }
        else {
          ctx.drawImage(img, ra.x, ra.y, ra.w, ra.h);
          resolve(c);
        }

      }).attr('src', image);
    });
  };

  function getResizedBuffer(resizedBuffer, sourceData) {
    var data = resizedBuffer.data;
    var length = data.length;
    for (var x = 0; x < length; ++x) {
      data[x] = sourceData[x] & 0xFF;
    }
    return resizedBuffer;
  }
  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.Bitmap = Bitmap;

})(DjCRDView);
