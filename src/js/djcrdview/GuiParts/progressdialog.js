(function (global) {

  var ProgressDialog = {};

  //---

  const PROGRESS_DIALOG_TEMPLATE = `
<section data-role="popup" data-history="false" data-dismissible="false" id="dynDialog" class="dj-dyn-dialog">
    <header data-role="header" data-theme="b">
        <h2 id="dynDialogTitle">PROGRESS DIALOG</h2>
    </header>
    <article data-role="content" class="ui-content">
        <h3 id="dynDialogContent">CONTENT</h3>
        <p id="dynDialogMessage">MESSAGE</p>
        <div>
            <progress id="progressOverall" class="dj-dyn-dialog-progressbar" max="1" value="0"></progress>
        </div>
        <div>
            <progress id="progressBar" class="dj-dyn-dialog-progressbar" max="1" value="0"></progress>
        </div>
        <a href="#" data-rel="back" id="dynDialogBtn-1" class="ui-btn ui-corner-all ui-btn-b ui-shadow">隠す</a>
    </article>
</section>
`;

  var overallValue = 0;

  /**
   * 進捗ダイアログを開く
   * @param targetid
   * @param config
   * @returns {*|jQuery}
   */
  ProgressDialog.show = function (config) {

    // 表示先ページ
    var $page = global.GuiParts.getActivePageModule();
    $page = $page && $page.$page;
    if (!$page) {
      return Promise.reject(new Error('not found active page'));
    }

    // 全体進捗をリセット
    overallValue = 0;

    return new Promise(function (resolve,reject) {
      $(document).ready(function () {

        $("#dynDialog").remove();

        var $dialog = $(PROGRESS_DIALOG_TEMPLATE).trigger('create');

        $dialog.find('#dynDialogTitle').text(config.title);
        $dialog.find('#dynDialogContent').text(config.content).css('display',config.content ? 'block':'none');
        $dialog.find('#dynDialogMessage').text(config.message);
        $dialog.find('#progressOverall').attr('max', config.maxOverall);
        $dialog.find('#progressBar').attr({
          'max': config.maxValue,
          'value': config.value
        });

        // 閉じたときの処理
        var dialogConfig = {
          afteropen: function () {
            resolve();
          },
          afterclose: function () {
            $dialog.remove();
          }
        };

        return $dialog.appendTo($page).enhanceWithin().popup(dialogConfig).popup(
          "open", {
            transition: "pop",
            positionTo: "window"
          });
      });
    });
  };

  /**
   * 進捗ダイアログを閉じる
   * @returns {*|jQuery}
   */
  ProgressDialog.close = function () {
    return $('#dynDialogBtn-1').click();
  };

  /**
   *
   * @param s
   * @returns {XMLList|*|jQuery}
   */
  ProgressDialog.setMessage = function (s) {
    return $('#dynDialogMessage').text(s);
  };

  /**
   * 全体進捗の値を設定する
   * @param v
   * @returns {*|jQuery}
   */
  ProgressDialog.setOverallValue = function (v) {
    return $('#progressOverall').attr('value',v);
  };

  /**
   * 個別進捗の値を設定する
   * @param v
   * @returns {*|jQuery}
   */
  ProgressDialog.setValue = function (v) {
    return $('#progressBar').attr('value',v);
  };

  /**
   * 次の個別進捗へ移行して、全体進捗を＋１する
   * @param message
   * @param max
   * @returns {*|jQuery}
   */
  ProgressDialog.setMax = function (message, max) {
    this.setMessage(message);
    this.setOverallValue(++overallValue);
    return $('#progressBar').attr({max:max,value:0});
  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.ProgressDialog = ProgressDialog;

})(DjCRDView);