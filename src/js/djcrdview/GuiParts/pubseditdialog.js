(function (global) {

  var PubsEditDialog = {};

  //---

  const PRICE_MIN = 0;
  const PRICE_MAX = 30000;

  const LIMITATION_MIN = 0;
  const LIMITATION_MAX = 20;

  const NUMBER_MIN = 1;
  const NUMBER_MAX = 20;

  const DYNAMIC_DIALOG_TEMPLATE = `
<section data-role="popup" data-history="false" data-dismissible="false" id="dynDialog" class="dj-pubs-edit-dialog">
    <header data-role="header" data-theme="b">
        <h2 id="dynDialogTitle">DIALOG</h2>
    </header>
    <article data-role="content" class="ui-content">
      <form>
        <div class="dj-dyn-dialog-two-buttons-container">
          <div>
            <select name="cls" id="pubsEditDialogClass"></select>
          </div>
          <div>
            <select name="cat" id="pubsEditDialogCategory"></select>
          </div>
          <div>
            <select name="typ" id="pubsEditDialogType"></select>
          </div>
          <div>
            <select name="trm" id="pubsEditDialogTerms"></select>
          </div>
          <div>
            <select name="mtd" id="pubsEditDialogMethod"></select>
          </div>
        </div>
        <input type="text" name="title" id="pubsEditDialogTitle" placeholder="タイトル">
        <textarea type="" name="desc" id="pubsEditDialogText" placeholder="説明" data-autogrow="false" class="dj-textarea-noresize" rows="3"></textarea>
        <input type="text" name="url" id="pubsEditDialogUrl" placeholder="頒布物告知ページへのリンク" class="dj-pubs-edit-url-text">
        <textarea type="" name="shops" id="pubsEditDialogShops" placeholder="通販商品ページへのリンク（複数可）" data-autogrow="false" class="dj-textarea-noresize dj-pubs-edit-url-text" rows="2"></textarea>
        <div class="dj-dyn-dialog-three-buttons-container">
          <div>
            <select name="price" id="pubsEditDialogPrice"></select>
          </div>
          <div>
            <select name="limitation" id="pubsEditDialogLimit"></select>
          </div>
          <div>
            <select name="num" id="pubsEditDialogNumber"></select>
          </div>
        </div>
      </form>
      <div id="dynDialogButton" class="dj-dyn-dialog-button-container"></div>
    </article>
</section>
`;

  const DYNAMIC_DIALOG_BUTTON_TEMPLATE = `
<a href="#" data-rel="back" id="dynDialogBtn-#INDEX#" class="ui-btn ui-corner-all ui-btn-b ui-shadow"></a>
`;

  const SELECT_OPTION_TEMPALTE = `
<option value="#VALUE#">#TEXT#</option>
`;
  /**
   * メッセージダイアログを開く
   * @param targetid
   * @param config
   */
  PubsEditDialog.show = function (config) {

    //
    function addButton ($dialog, buttons) {
      var $buttonContainer = $dialog.find('#dynDialogButton');
      (buttons || [{label:'CLOSE'}]).forEach(function (btn, index) {
          var html = DYNAMIC_DIALOG_BUTTON_TEMPLATE.replace(/#INDEX#/g,index+1);
          var $box = $(html).appendTo($buttonContainer).text(btn.label);

          $box.on('click', function () {
            (function (b) {
              selected = b;
            })(btn);
          });
        }
      );
      $buttonContainer.trigger('create');
    }

    //

    // 表示先ページ
    var $page = global.GuiParts.getActivePageModule();
    $page = $page && $page.$page;
    if (!$page) {
      return Promise.reject(new Error('not found active page'));
    }

    // 押したボタン
    var selected = null;

    return new Promise(function (resolve,reject) {

      $(document).ready(function () {

        $("#dynDialog").remove();

        var $dialog = $(DYNAMIC_DIALOG_TEMPLATE).trigger('create');

        $dialog.find('#dynDialogTitle').text(config.title);

        var html;

        html = '';
        var $cls = $dialog.find('#pubsEditDialogClass');
        global.Const.Distribution.class.forEach(function (c, index) {
          html += SELECT_OPTION_TEMPALTE.replace(/#VALUE#/,index+1).replace(/#TEXT#/,c);
        });
        $(html).appendTo($cls);

        html = '';
        var $cat = $dialog.find('#pubsEditDialogCategory');
        global.Const.Distribution.category.forEach(function (c, index) {
          html += SELECT_OPTION_TEMPALTE.replace(/#VALUE#/,index+1).replace(/#TEXT#/,c);
        });
        $(html).appendTo($cat);

        html = '';
        var $typ = $dialog.find('#pubsEditDialogType');
        global.Const.Distribution.type.forEach(function (c, index) {
          html += SELECT_OPTION_TEMPALTE.replace(/#VALUE#/,index+1).replace(/#TEXT#/,c);
        });
        $(html).appendTo($typ);

        html = '';
        var $mtd = $dialog.find('#pubsEditDialogMethod');
        global.Const.Distribution.method.forEach(function (c, index) {
          html += SELECT_OPTION_TEMPALTE.replace(/#VALUE#/,index+1).replace(/#TEXT#/,c);
        });
        $(html).appendTo($mtd);

        html = '';
        var $trm = $dialog.find('#pubsEditDialogTerms');
        global.Const.Distribution.terms.forEach(function (c, index) {
          html += SELECT_OPTION_TEMPALTE.replace(/#VALUE#/,index+1).replace(/#TEXT#/,c);
        });
        $(html).appendTo($trm);

        html = SELECT_OPTION_TEMPALTE.replace(/#VALUE#/,-1).replace(/#TEXT#/,"頒布価格未定");
        var $price = $dialog.find('#pubsEditDialogPrice');
        var step = 100;
        for (var i=PRICE_MIN; i<=PRICE_MAX; i += step) {
          html += SELECT_OPTION_TEMPALTE.replace(/#VALUE#/,i).replace(/#TEXT#/,i ? i : "無料配布");
          if (i >= 10000) {
            step = 1000;
          }
          else if (i >= 2000){
            step = 500;
          }
        }
        $(html).appendTo($price);

        html = SELECT_OPTION_TEMPALTE.replace(/#VALUE#/,-1).replace(/#TEXT#/,"限数未定");
        var $limitation = $dialog.find('#pubsEditDialogLimit');
        for (var i=LIMITATION_MIN; i<=LIMITATION_MAX; i++) {
          html += SELECT_OPTION_TEMPALTE.replace(/#VALUE#/,i).replace(/#TEXT#/,i ? i+"限" : "限なし");
        }
        $(html).appendTo($limitation);

        html = SELECT_OPTION_TEMPALTE.replace(/#VALUE#/,-1).replace(/#TEXT#/,"購入数未定");
        var $num = $dialog.find('#pubsEditDialogNumber');
        for (var i=NUMBER_MIN; i<=NUMBER_MAX; i++) {
          html += SELECT_OPTION_TEMPALTE.replace(/#VALUE#/,i).replace(/#TEXT#/,"x "+i);
        }
        $(html).appendTo($num);

        // ボタン
        addButton ($dialog, config.buttons);

        // 閉じたときの処理
        var dialogConfig = {
          afterclose: function () {
            var urls = ($('#pubsEditDialogUrl').val() || '').split(/[\r\n]+/).map(function (v) {return v.trim();})
              .filter(function (v) {
                return /^https?:\/\//.test(v);
              });
            var shops = ($('#pubsEditDialogShops').val() || '').split(/[\r\n]+/).map(function (v) {return v.trim();})
              .filter(function (v) {
                return /^https?:\/\//.test(v);
              });

            var content = {
              'id': config.id,
              'class': $('#pubsEditDialogClass').val(),
              'category': $('#pubsEditDialogCategory').val(),
              'type': $('#pubsEditDialogType').val(),
              'method': $('#pubsEditDialogMethod').val(),
              'terms': $('#pubsEditDialogTerms').val(),
              'title': $('#pubsEditDialogTitle').val(),
              'desc': $('#pubsEditDialogText').val(),
              'urls': urls,
              'shops': shops,
              'limitation': $('#pubsEditDialogLimit').val(),
              'price': $('#pubsEditDialogPrice').val(),
              'num': $('#pubsEditDialogNumber').val()
            };

            $dialog.remove();

            if (!selected) {
              // ×ボタンで閉じた？
              return reject();
            }

            var obj = {
              value: selected.value,
              content: content
            };

            resolve(obj);
          }
        };

        return $dialog.appendTo($page).enhanceWithin().popup(dialogConfig).popup(
          "open", {
            transition: "pop",
            positionTo: "window"
          }).ready(function () {
            if (config.content) {
              $('#pubsEditDialogTitle').val(config.content.title);
              $('#pubsEditDialogText').val(config.content.desc);
              $('#pubsEditDialogUrl').val(config.content.urls && config.content.urls.length > 0 && config.content.urls[0]);
              $('#pubsEditDialogShops').val(config.content.shops && config.content.shops.join('\n'));
              $cls.val(config.content.class).selectmenu('refresh');
              $cat.val(config.content.category).selectmenu('refresh');
              $typ.val(config.content.type).selectmenu('refresh');
              $trm.val(config.content.terms).selectmenu('refresh');
              $cat.val(config.content.category).selectmenu('refresh');
              $mtd.val(config.content.method).selectmenu('refresh');
              $price.val(config.content.price).selectmenu('refresh');
              $limitation.val(config.content.limitation).selectmenu('refresh');
              $num.val(config.content.num).selectmenu('refresh');
            }
          });
      });
    });
  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.PubsEditDialog = PubsEditDialog;

})(DjCRDView);