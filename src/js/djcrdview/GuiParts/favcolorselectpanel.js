(function (global) {

  var FavColorSelectPanel = {};

  var $panel;

  //---

  const FAV_COLOR_SELECTOR_TEMPLATE = `
<div>
  <button class="dj-navpanel-selector dj-fav-color ui-btn ui-corner-all" data-color-id="#ID#"></button>
</div>
`;

  const FAV_COLOR_SELECTOR_COLUMNS = 3;

  const FAV_COLOR_DISABLER_BUTTONS = {
    '0': '無効',
    '-1': '削除'
  };

  const FAV_COLOR_DISABLER_BUTTON_KEYS = ['0','-1'];

  const FAV_COLOR_DISABLER_COLUMNS = 2;

  const FAV_CONDITION_SELECTOR_TEMPLATE = `
<div>
  <button class="dj-navpanel-selector ui-btn ui-corner-all" data-condition-id="#ID#"></button>
</div>
`;

  const FAV_CONDITION_SELECTOR_COLUMNS = 3;

  const FAV_CONDITION_DISABLER_BUTTONS = {
    '-1': '一つ戻す'
  };

  const FAV_CONDITION_DISABLER_BUTTON_KEYS = ['-1'];

  const FAV_CONDITION_DISABLER_COLUMNS = 2;

  /**
   * 色選択ボタンを押した際のハンドラ
   */
  function tapFavColorHandler () {
    var colorId = $(this).attr('data-color-id');
    $panel.panel("close");
    global.DataOp.FavCircle.updateColor(colorId);
  }

  /**
   * 状況選択ボタンを押した際のハンドラ
   */
  function tapConditionHandler () {
    var condId = $(this).attr('data-condition-id');
    $panel.panel("close");
    global.DataOp.FavCircle.updateCondition(condId);
  }

  /**
   * 色＆状況選択パネル
   * @param $listView
   */
  function addFavColorSelector ($listView) {

    // ディバイダー
    $('<li>').attr({
      'data-role': 'list-divider'
    }).text('お気に入り選択').appendTo($listView);

    // ボタンの追加
    var $li = $('<li>').addClass('dj-navpanel-selector-container').appendTo($listView);
    var $grid = $('<div>').addClass('ui-grid-'+String.fromCharCode('a'.charCodeAt(0) + (FAV_COLOR_SELECTOR_COLUMNS-2))).appendTo($li);

    var html = '';
    global.DataOp.FavColor.getSortedKeys().forEach(function (colorId) {
      html += FAV_COLOR_SELECTOR_TEMPLATE.replace(/#ID#/g, colorId);
    });

    $grid.append(html).trigger('create');

    // ボタンの修飾
    $grid.children().each(function (index) {
      var $box = $(this);
      var $btn = $box.find('button');

      var abc = String.fromCharCode('a'.charCodeAt(0) + (index % FAV_COLOR_SELECTOR_COLUMNS));

      $box.addClass('ui-block-'+abc);
      $btn.text($btn.attr('data-color-id'));

      // ボタンON・OFF時のハンドラ
      $btn.on('click', tapFavColorHandler)
    });
  }

  /**
   *
   * @param $listView
   */
  function addFavColorDisabler ($listView) {

    // ディバイダー
    $('<li>').attr({
      'data-role': 'list-divider'
    }).text('お気に入り削除').appendTo($listView);

    // ボタンの追加
    var $li = $('<li>').addClass('dj-navpanel-selector-container').appendTo($listView);
    var $grid = $('<div>').addClass('ui-grid-'+String.fromCharCode('a'.charCodeAt(0) + (FAV_COLOR_DISABLER_COLUMNS-2))).appendTo($li);

    var html = '';
    FAV_COLOR_DISABLER_BUTTON_KEYS.forEach(function (id) {
      html += FAV_COLOR_SELECTOR_TEMPLATE.replace(/#ID#/g, id);
    });

    $grid.append(html).trigger('create');

    // ボタンの修飾
    $grid.children().each(function (index) {
      var $box = $(this);
      var $btn = $box.find('button');

      var abc = String.fromCharCode('a'.charCodeAt(0) + (index % FAV_COLOR_DISABLER_COLUMNS));

      $box.addClass('ui-block-'+abc);
      $btn.text(FAV_COLOR_DISABLER_BUTTONS[$btn.attr('data-color-id')]).css({'background-color':'gray'});

      // ボタンON・OFF時のハンドラ
      $btn.on('click', tapFavColorHandler)
    });
  }

  /**
   *
   * @param $listView
   */
  function addFavConditionSelector ($listView) {

    // ディバイダー
    $('<li>').attr({
      'data-role': 'list-divider'
    }).text('状況選択').appendTo($listView);

    // ボタンの追加
    var $li = $('<li>').addClass('dj-navpanel-selector-container').appendTo($listView);
    var $grid = $('<div>').addClass('ui-grid-'+String.fromCharCode('a'.charCodeAt(0) + (FAV_CONDITION_SELECTOR_COLUMNS-2))).appendTo($li);

    var html = '';
    global.DataOp.Condition.getSortedKeys().forEach(function (condId) {
      html += FAV_CONDITION_SELECTOR_TEMPLATE.replace(/#ID#/g, condId);
    });

    $grid.append(html).trigger('create');

    // ボタンの修飾
    $grid.children().each(function (index) {
      var $box = $(this);
      var $btn = $box.find('button');

      var abc = String.fromCharCode('a'.charCodeAt(0) + (index % FAV_CONDITION_SELECTOR_COLUMNS));
      var c = global.DataOp.Condition.findCondition($btn.attr('data-condition-id'));

      $box.addClass('ui-block-'+abc);
      $btn.text(c.shortlabel);

      // ボタンON・OFF時のハンドラ
      $btn.on('click', tapConditionHandler)
    });
  }

  /**
   *
   * @param $listView
   */
  function addFavConditionDisabler ($listView) {

    // ディバイダー
    $('<li>').attr({
      'data-role': 'list-divider'
    }).text('状況を一つ戻す').appendTo($listView);

    // ボタンの追加
    var $li = $('<li>').addClass('dj-navpanel-selector-container').appendTo($listView);
    var $grid = $('<div>').addClass('ui-grid-'+String.fromCharCode('a'.charCodeAt(0) + (FAV_CONDITION_DISABLER_COLUMNS-2))).appendTo($li);

    var html = '';
    FAV_CONDITION_DISABLER_BUTTON_KEYS.forEach(function (id) {
      html += FAV_CONDITION_SELECTOR_TEMPLATE.replace(/#ID#/g, id);
    });

    $grid.append(html).trigger('create');

    // ボタンの修飾
    $grid.children().each(function (index) {
      var $box = $(this);
      var $btn = $box.find('button');

      var abc = String.fromCharCode('a'.charCodeAt(0) + (index % FAV_CONDITION_DISABLER_COLUMNS));

      $box.addClass('ui-block-'+abc);
      $btn.text(FAV_CONDITION_DISABLER_BUTTONS[$btn.attr('data-condition-id')]).css({'background-color':'gray'});

      // ボタンON・OFF時のハンドラ
      $btn.on('click', tapConditionHandler)
    });
  }

  /**
   *
   * @param $listView
   */
  function addHeader ($listView) {
    var $title = $('<a>').attr({
      'href': '#',
      'data-rel': 'close',
      'id': 'favColorSelectTitle'
    }).addClass('dj-favedit-title').text('Edit');

    $('<li>').attr({
      'data-icon': false,
      'data-theme': 'a'
    }).append($title).appendTo($listView);
  }

  /**
   *
   * @returns {*|jQuery}
   */
  FavColorSelectPanel.initPanel = function () {

    var $listView = $('#favColorSelectPanelList').empty();

    //addHeader($listView);
    addFavColorSelector($listView);
    addFavColorDisabler($listView);
    addFavConditionSelector($listView);
    addFavConditionDisabler($listView);

    $listView.listview().listview('refresh');

    $panel = $('#favColorSelectPanel').attr('data-theme', 'a').panel().trigger('updatelayout');

    return $panel;

  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.FavColorSelectPanel = FavColorSelectPanel;

})(DjCRDView);
