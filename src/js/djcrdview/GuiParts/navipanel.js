(function (global) {

  var NaviPanel = {};

  //---

  NaviPanel.addDivider = function (title, $listview) {
    $('<li>').attr({
      'data-role': 'list-divider'
    }).text(title).appendTo($listview);
  };

  /**
   * ナビパネルにボタンを追加する
   * @param title
   * @param buttons
   * @param $listview
   */
  NaviPanel.addButtons = function (title,buttons,$listview) {
    // ディバイダー
    if (title) {
      NaviPanel.addDivider(title,$listview);
    }

    // ボタン
    buttons.forEach(function (button) {

      // ガワ
      var $box = $('<li>').appendTo($listview);
      if (!button.href || button.href == '#') {
        $box.attr({
          'data-icon': !!button.href && button.href != '#'
        });
      }

      // ボタン本体
      var $btn = $('<a>').appendTo($box);
      $btn.attr({
        'href': button.href,
        'data-rel': 'close',
        'id': button.id
      }).text(button.label);

      // 普通のボタンの処理
      if (!button.type) {
        $btn.on('tap', function() {
          if (button.callback)
            button.callback(this);
        });
        return;
      }

      // ファイル入出力のボタンの処理
      var $input = $('<input>').attr({
        'type': button.type,
        'accept': button.accept
      }).css({
        'display':'none'
      });

      $btn.on('tap', function() {
        $input.click();
      });

      $input.change(function () {
        if (button.callback)
          button.callback(this);
      });
    });
  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.NaviPanel = NaviPanel;

})(DjCRDView);
