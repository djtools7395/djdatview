// FIXME descriptionbar.css と mapview.css がこんがらがっている
// TODO お気に入りの行モードとカットモード

(function (global) {

  var PageHeader = {};

  //---
  const PAGE_HEADER_BUTTON_LEFT_TEMPLATE = `
<a href="#navPanel" data-role="button" role="button" class="ui-btn ui-btn-left ui-icon-bars ui-btn-icon-left ui-corner-all dj-page-header-btn-left">Menu</a>
`;

  const PAGE_HEADER_BUTTON_RIGHT_CONTAINER_TEMPLATE = `
<div data-role="controlgroup" data-type="horizontal" class="ui-btn-right"></div>
`;

  const PAGE_HEADER_BUTTON_RIGHT_TEMPLATE = `
<a href="#" data-role="button" role="button" class="ui-btn ui-btn-b dj-page-header-btn-right"></a>
`;

  /**
   * ページヘッダの追加
   * @param $page
   * @param pageId
   * @returns {*}
   */
  PageHeader.add = function ($page,pageId) {

    var $header = $page.children('[data-role="header"]').attr({
      'id': pageId.replace(/Page/, 'Header'),
      'data-theme': 'b'
    }).addClass('dj-page-header');

    // ヘッダ＞左ボタン
    $(PAGE_HEADER_BUTTON_LEFT_TEMPLATE).appendTo($header);

    // ヘッダ＞タイトル
    $('<h2>').appendTo($header);

    // ヘッダ＞右ボタン＆右ボタンメニュー
    if (/ViewPage$/.test(pageId)) {
      // 閲覧系ページ
      var $btnContainer = $(PAGE_HEADER_BUTTON_RIGHT_CONTAINER_TEMPLATE).appendTo($header);

      ['search', 'cut', 'fav', 'map'].forEach(function (id) {

        var $rightButton = $(PAGE_HEADER_BUTTON_RIGHT_TEMPLATE).attr('data-button-id', id).text(id.toUpperCase()).appendTo($btnContainer);

        // tapだと、開いた後のパネルにまでイベントが発生してしまうので、click
        $rightButton.on('click', function (e) {

          console.debug('on '+e.type+' header button');

          var $btn = $(this);
          var btnId = $btn.attr('data-button-id');
          global.GuiParts.ViewMode.setViewMode(btnId);
        });

        $rightButton.trigger('create');

        // ナビゲーションパネルのリセット
        var $listContainer = $('#' + id + 'NavPanelList').empty();
        $listContainer.listview().listview('refresh');
      });
    }
    else {
      // 管理系ページ
      var id = pageId.replace(/Page$/,'NaviPanel');

      $(PAGE_HEADER_BUTTON_RIGHT_TEMPLATE).attr('href','#'+id).addClass('ui-corner-all').text('Navi').appendTo($header);
    }

    return $header;
  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.PageHeader = PageHeader;

})(DjCRDView);