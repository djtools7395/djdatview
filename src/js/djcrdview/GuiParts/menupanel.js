// FIXME map 表示時に mapメニュー をクリックすると Naviパネルが開いちゃう

(function (global) {

  var MenuPanel = {};

  //---

  /**
   * ヘッダの左ボタンをクリックしたときに出てくるメニューの初期化（ページ外で宣言しているので外観の初期化が必要）
   * @returns {*|jQuery}
   */
  MenuPanel.initPanel = function () {
    $('#navPanelList').listview().listview('refresh');

    // FIXME
    $('#navPanelList').find("a[data-button-id]").each(function () {
      var $btn = $(this);
      $btn.on('tap', function (e) {

        console.debug('on '+e.type+' menu button');

        if (!global.Env.selectedEventId) {
          // イベントを選択してない場合は配置図ビューに移動しない
          console.log('no event selected');
          return;
        }

        var $btn = $(this);
        var btnId = $btn.attr('data-button-id');
        global.GuiParts.ViewMode.setViewMode(btnId);
        $('body').pagecontainer('change', '#mapViewPage');
      });
    });

    return $('#navPanel').attr('data-theme', 'a').panel().trigger('updatelayout');
  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.MenuPanel = MenuPanel;

})(DjCRDView);

