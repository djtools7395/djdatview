(function (global) {

  var DataInputPanel = {};

  //---

  /**
   * お気に入りcsv読み込みボタンのハンドラ
   * @param obj
   */
  function loadFavHandler (obj) {

    //
    /**
     * お気に入りの上書きをするか確認する
     * @param favlist
     * @returns {*}
     */
    function confirmOverwrite (eventId) {
      // 確認ダイアログを開く
      function showDialog () {
        var config = {
          title: '警告',
          content: '既にお気に入りが存在しています。上書きしますか？',
          message: '(ID:' + global.DataOp.EventList.Selected.Event.djdatNo + ',NAME:' + global.DataOp.EventList.Selected.Event.djdatName + ')',
          buttons: [
            {label: '追加する', value: global.DataOp.FavList.MODE_MERGE},
            {label: '上書きする', value: global.DataOp.FavList.MODE_OVERWRITE},
            {label: 'キャンセル'}
          ]
        };

        return global.GuiParts.DynDialog.show(config);
      }

      // 確認ダイアログの選択を反映する
      function setSelectedMode (r) {
        if (!r.value) {
          // キャンセル
          return Promise.reject();
        }

        obj.mode = r.value;
        return Promise.resolve(obj);
      }

      // 進捗ダイアログを開く
      function showProgress (obj) {
        var config = {
          title: '進捗',
          message: 'お気に入り読み込み中...',
          maxOverall: 1,
          maxValue: 4,
          value: 0
        };

        return global.GuiParts.ProgressDialog.show(config)
          .then(function () {
            return Promise.resolve(obj);
          });
      }

      //

      var obj = {
        eventId: global.DataOp.EventList.Selected.Event.djdatNo,
        name: global.DataOp.EventList.Selected.Event.djdatName,
        match: global.DataOp.FavList.MATCH_EQUAL,
        mode: global.DataOp.FavList.MODE_OVERWRITE
      };

      if (eventId != global.DataOp.EventList.Selected.Event.djdatNo) {
        obj.match = global.DataOp.FavList.MATCH_LIKE;
      }

      if (global.DataOp.FavCircle.getSortedKeys().length == 0) {
        // 既存の情報が０件ならば上書きで良い
        return Promise.resolve(obj).then(showProgress(obj));
      }

      return showDialog()
        .then(setSelectedMode)
        .then(showProgress);
    }

    //
    function progress (v) {
      global.GuiParts.ProgressDialog.setValue(v);
    }

    //

    var file = obj.files.length > 0 && obj.files[0];
    if ( ! file) {
      return;
    }

    var $input = $(obj);

    var config = {
      confirmOverwrite: confirmOverwrite,
      progress: progress
    };

    global.DataOp.FavList.loadFavList(file, config)
      .then(function () {
        // キャッシュをクリアしてから画面に反映
        global.DataOp.FavColor.clearCache().then(function () {
          global.DataOp.FavCircle.clearCache().then(function () {
            global.Pages.MapView.updateFav();
          });
        });
      })
      .catch(function (e) {
        if (e) {
          console.error(e.stack || e);
          return;
        }

        console.debug('cancelled');
      })
      .then(function () {
        // finally
        $input.val('');
        global.GuiParts.ProgressDialog.close();
      });
  }

  /**
   * 差し替えURLの取り込み
   * @param obj
   */
  function loadExtraUrlHandler (obj) {

    //

    var file = obj.files.length > 0 && obj.files[0];
    if ( ! file ) {
      return;
    }

    var $input = $(obj);

    return Promise.resolve()
      .then(function () {
        var config = {
          title: '進捗',
          message: '差し替えURL読み込み中...',
          maxOverall: 2,
          maxValue: 100,
          value: 0
        };

        return global.GuiParts.ProgressDialog.show(config);
      })
      .then(function () {
        return global.Utils.readAsText(file);
      })
      .then(function (text) {
        function progress (r) {
          global.GuiParts.ProgressDialog.setValue(r.value);
        }

        return global.DataOp.Circle.storeExtraUrlObjects(text, progress);
      })
      .then(function () {
        // FIXME CutImageキャッシュのクリアが必要
      })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      })
      .then(function () {
        // finally
        $input.val('');
        global.GuiParts.ProgressDialog.close();
      });
  }

  /**
   * 頒布物情報読み込みボタンのハンドラ
   * @param obj
   */
  function loadPubsInfoHandler (obj) {

    //
    function progress (v) {
      global.GuiParts.ProgressDialog.setValue(v);
    }

    //

    var file = obj.files.length > 0 && obj.files[0];
    if ( ! file) {
      return;
    }

    var $input = $(obj);

    global.DataOp.Circle.loadPubsInfo(file)
      .then(function () {
        // キャッシュをクリアしてから画面に反映
        /*
         global.DataOp.FavColor.clearCache().then(function () {
         global.DataOp.FavCircle.clearCache().then(function () {
         global.Pages.MapView.updateFav();
         });
         });
         */
      })
      .catch(function (e) {
        if (e) {
          console.error(e.stack || e);
          return;
        }

        console.debug('cancelled');
      })
      .then(function () {
        // finally
        $input.val('');
        global.GuiParts.ProgressDialog.close();
      });
  }

  /**
   * 差し替えカット画像の取り込み
   * @param obj
   */
  function loadAltImageHandler (obj) {

    function readFromJson (file) {
      return global.Utils.readAsText(file).then(function (text) {

        var json = JSON.parse(text);

        if (json.fileId != global.Const.FileId.Cuts) {
          return Promise.reject(new Error('unmatch file type fileId=' + json.fileId + ', currentId=' + global.Const.FileId.Cuts));
        }
        if (json['eventId'] != global.Env.selectedEventId) {
          return Promise.reject(new Error('unmatch event eventId=' + json['eventId'] + ', currentId=' + global.Env.selectedEventId));
        }

        return Promise.resolve(json);
      });
    }

    //

    var file = obj.files.length > 0 && obj.files[0];
    if ( ! file ) {
      return;
    }

    return global.DataOp.CutImage.storeCutImageAltObjects(obj.files[0]);

    // FIXME 進捗状況表示うんぬん
    /*
    var $input = $(obj);

    Promise.resolve()
      .then(function () {
        var config = {
          title: '進捗',
          message: '差し替えカット画像読み込み中...',
          maxOverall: 2,
          maxValue: 100,
          value: 0
        };

        return global.GuiParts.ProgressDialog.show(config);
      })
      .then(function () {
        return readFromJson(file);
      })
      .then(function (json) {
        function progress (r) {
          global.GuiParts.ProgressDialog.setValue(r.value);
        }

        return global.DataOp.CutImage.storeCutImageAltObjects(json, progress);
      })
      .then(function () {
        // FIXME CutImageキャッシュのクリアが必要
        //putCutsToGrid();
      })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      })
      .then(function () {
        // finally
        $input.val('');
        global.GuiParts.ProgressDialog.close();
      });
      */
  }

  /**
   * 状況履歴読み込みボタンのハンドラ
   * @param obj
   */
  function historyLoadHandler (obj) {

    function readFromJson (file) {
      return global.Utils.readAsText(file).then(function (text) {

        var json = JSON.parse(text);

        if (json.fileId != global.Const.FileId.Hist) {
          return Promise.reject(new Error('unmatch file type fileId=' + json.fileId + ', currentId=' + global.Const.FileId.Hist));
        }
        if (json['eventId'] != global.Env.selectedEventId) {
          return Promise.reject(new Error('unmatch event eventId=' + json['eventId'] + ', currentId=' + global.Env.selectedEventId));
        }

        return Promise.resolve(json);
      });
    }

    //

    var file = obj.files.length > 0 && obj.files[0];
    if ( ! file ) {
      return;
    }

    var $input = $(obj);

    Promise.resolve()
      .then(function () {
        var config = {
          title: '進捗',
          message: '状況履歴読み込み中...',
          maxOverall: 2,
          maxValue: 100,
          value: 0
        };

        return global.GuiParts.ProgressDialog.show(config);
      })
      .then(function () {
        return readFromJson(file);
      })
      .then(function (json) {
        function progress (r) {
          global.GuiParts.ProgressDialog.setValue(r.value);
        }

        return global.DataOp.FavCondition.storeFavConditionHistoriesAll(json, progress);
      })
      .then(function () {
        // FIXME CutImageキャッシュのクリアが必要
      })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      })
      .then(function () {
        // finally
        $input.val('');
        global.GuiParts.ProgressDialog.close();
      });
  }

  /**
   *
   */
  DataInputPanel.initPanel = function () {

    function addFavLoadButton ($listview) {
      var buttons = [
        { label:'お気に入り読み込み', type:'file', accept:'.csv', callback:loadFavHandler }
      ];

      global.GuiParts.NaviPanel.addButtons('-', buttons, $listview);
    }

    function addExtraUrlSelector($listview) {
      var buttons = [
        { label:'差し替えURL読み込み', type:'file', accept:'.json', callback:loadExtraUrlHandler }
      ];

      global.GuiParts.NaviPanel.addButtons('-', buttons, $listview);
    }

    function addPubsInfoLoadButton ($listview) {
      var buttons = [
        { label:'頒布物読み込み', type:'file', accept:'.json', callback:loadPubsInfoHandler }
      ];

      global.GuiParts.NaviPanel.addButtons('-', buttons, $listview);
    }

    function addAltImageSelector($listview) {
      var buttons = [
        { label:'差し替えカット画像読み込み', type:'file', accept:'.CCZ', callback:loadAltImageHandler }
      ];

      global.GuiParts.NaviPanel.addButtons('-', buttons, $listview);
    }

    function addHistoryLoadButton ($listview) {
      var buttons = [
        { label:'状況履歴読み込み', type:'file', accept:'.json', callback:historyLoadHandler }
      ];

      global.GuiParts.NaviPanel.addButtons('-', buttons, $listview);
    }

    //

    var $navpanel = $('#dataInputPanelList');

    addFavLoadButton($navpanel);
    addPubsInfoLoadButton($navpanel);
    addExtraUrlSelector($navpanel);
    addAltImageSelector($navpanel);
    addHistoryLoadButton($navpanel);

    $navpanel.listview().listview('refresh');

    return $('#dataInputPanel').attr('data-theme', 'a').panel().trigger('updatelayout');
  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.DataInputPanel = DataInputPanel;

})(DjCRDView);

