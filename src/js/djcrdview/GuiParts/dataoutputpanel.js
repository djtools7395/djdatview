(function (global) {

  var DataOutputPanel = {};

  //---

  /**
   * お気に入りcsv保存ボタンのハンドラ
   */
  function saveFavHandler () {
    global.DataOp.FavList.getFavCSVText()
      .then(function (csv) {
        // ファイルに保存する
        var data = 'data:text/csv;base64,'+btoa(csv);
        global.Utils.saveAsText('C'+global.Env.selectedEventId+'.csv', data);
      })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      });
  }

  /**
   * 差し替えURLをjsonファイルに保存する
   */
  function saveExtraUrlHandler () {
    return Promise.resolve().then(function () {
      var config = {
        title: '警告',
        content: '差し替えURLをファイルに保存しますか？',
        buttons: [
          { label: '保存する', value:true },
          { label: 'キャンセル' }
        ]
      };

      return global.GuiParts.DynDialog.show(config);
    })
      .then(function (r) {
        if (!r.value) {
          return Promise.reject();
        }

        // 差し替えURLのJSON化
        return global.DataOp.Circle.getExtraUrlObjects();
      })
      .then(function (json) {

        // ファイルに保存
        global.Utils.saveAsJson('C' + global.Env.selectedEventId + '_extraurl.json', json);
      })
      .catch(function (e) {
        if (e) {
          console.error(e.stack || e);
        }
        else {
          console.log('cancelled');
        }
      });
  }

  /**
   * 頒布物情報json保存ボタンのハンドラ
   */
  function savePubsInfoHandler () {
    global.DataOp.Circle.getPubsInfoAll().then(function (pubsinfo) {

      // ファイルに保存
      var obj = {};
      obj.fileId = global.Const.FileId.Pubs;
      obj.eventId = global.Env.selectedEventId;
      obj.pubsinfo = pubsinfo;
      global.Utils.saveAsJson('C' + global.Env.selectedEventId + '_pubsinfo.json', obj);

    }).catch(function (e) {
      if (e)
        console.error(e.stack || e);
    });
  }

  /**
   * 差し替え画像をjsonファイルに保存する
   */
  function saveAltImageHandler () {
    Promise.resolve().then(function () {
      var config = {
        title: '警告',
        content: '差し替えカット画像をファイルに保存しますか？',
        buttons: [
          { label: '保存する', value:true },
          { label: 'キャンセル' }
        ]
      };

      return global.GuiParts.DynDialog.show(config);

    }).then(function (r) {
      if (!r.value) {
        return Promise.reject();
      }

      // 差し替えカット画像のzipBlob化
      return global.DataOp.CutImage.getCutImageAltObjects();

    }).then(function (blob) {

      // ファイルに保存
      var url = URL.createObjectURL(blob);
      try {
        global.Utils.saveAsText('C' + global.Env.selectedEventId + 'CUTALT.CCZ', url);
      }
      catch (e) {
        console.error(e);
      }
      URL.revokeObjectURL(url);

    }).catch(function (e) {
      if (e) {
        console.error(e.stack || e);
      }
      else {
        console.log('cancelled');
      }
    });
  }

  /**
   * 状況履歴をjsonファイルに保存する
   */
  function historySaveHandler () {

    global.DataOp.FavCondition.getFavConditionHistoriesAll().then(function (conditions) {

      // ファイルに保存
      var obj = {};
      obj.fileId = global.Const.FileId.Hist;
      obj.eventId = global.Env.selectedEventId;
      obj.histories = conditions;
      global.Utils.saveAsJson('C' + global.Env.selectedEventId + '_history.json', obj);

    }).catch(function (e) {
      if (e)
        console.error(e.stack || e);
    });
  }

  /**
   *
   */
  DataOutputPanel.initPanel = function () {

    function addFavSaveButton ($listview) {
      var buttons = [
        { label:'お気に入り保存', href:'#',callback:saveFavHandler }
      ];

      global.GuiParts.NaviPanel.addButtons('-', buttons, $listview);
    }

    function addPubsInfoSaveButton ($listview) {
      var buttons = [
        { label:'頒布物情報保存', href:'#',callback:savePubsInfoHandler }
      ];

      global.GuiParts.NaviPanel.addButtons('-', buttons, $listview);
    }

    function addExtraUrlSaveLink ($listview) {
      var buttons = [
        { label:'差し替えURL保存', href:'#',callback:saveExtraUrlHandler }
      ];

      global.GuiParts.NaviPanel.addButtons('-', buttons, $listview);
    }

    function addAltImageSaveLink ($listview) {
      var buttons = [
        { label:'差し替えカット画像保存', href:'#',callback:saveAltImageHandler }
      ];

      global.GuiParts.NaviPanel.addButtons('-', buttons, $listview);
    }

    function addHistorySaveButton ($listview) {
      var buttons = [
        { label:'状況履歴保存', href:'#',callback:historySaveHandler }
      ];

      global.GuiParts.NaviPanel.addButtons('-', buttons, $listview);
    }

    //

    var $navpanel = $('#dataOutputPanelList').empty();

    addFavSaveButton($navpanel);
    addPubsInfoSaveButton($navpanel);
    addExtraUrlSaveLink($navpanel);
    addAltImageSaveLink($navpanel);
    addHistorySaveButton($navpanel);

    $navpanel.listview().listview('refresh');

    return $('#dataOutputPanel').attr('data-theme', 'a').panel().trigger('updatelayout');

  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.DataOutputPanel = DataOutputPanel;

})(DjCRDView);

