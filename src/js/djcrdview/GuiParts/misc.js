(function (global) {

  // FIXME GuiParts直下から移動予定
  global.GuiParts = global.GuiParts || {};

  var GuiParts = global.GuiParts;

  //---

  /**
   * 画像をcanvasに描画する
   * @param c
   * @param src
   * @param callback
   */
  GuiParts.drawImageToCanvas = function (c, img, w, h, bg) {
    var ctx = c.getContext('2d');
    if (img) {
      if (!w || !h) {
        // ソース画像のサイズのまま
        c.width = img.width;
        c.height = img.height;
        ctx.drawImage(img,0,0);
      }
      else {
        // 指定のサイズに収める
        c.width = w;
        c.height = h;

        var r = (function () {
          if (c.width / c.height > img.width / img.height) {
            return {w: Math.ceil(c.height / img.height * img.width), h: c.height};
          }
          else {
            return {w: c.width, h: Math.ceil(c.width / img.width * img.height)};
          }
        })();

        var ra = {
          x: Math.ceil((c.width - r.w) / 2),
          y: Math.ceil((c.height - r.h) / 2),
          w: r.w,
          h: r.h
        };

        if (bg) {
          ctx.fillStyle = bg;
          ctx.fillRect(0, 0, w, h);
        }
        else {
          ctx.clearRect(0, 0, w, h);
        }
        ctx.drawImage(img, ra.x, ra.y, ra.w, ra.h);
      }
    }
    else {
      ctx.clearRect(0,0,c.width,c.height);
    }
    return c;
  };

  /**
   * dataURLとかを画像にデコードしておき、描画の度にデコードしなくていいようにする
   * @param src
   * @returns {Promise}
   */
  GuiParts.getImageBuffer = function (src, w, h, bg) {
    return new Promise(function (resolve, reject) {
      if (!src)
        return resolve();

      $(new Image()).on('load', function () {
        var canvas = document.createElement('canvas');
        global.GuiParts.drawImageToCanvas(canvas, this, w, h, bg);
        resolve(canvas);
      }).error(function (e) {
        console.error(e);
        resolve(null);
      }).attr('src', src);
    });
  };

  /**
   * スクロールバーのサイズを取得する
   * @returns {{width: number, height: number}}
   */
  GuiParts.getScrollbarSize = function () {
    var div = document.createElement('div');
    var s = div.style;
    s.position = 'fixed';
    s.left = s.top = '-200px';
    s.height = s.width = '100px';
    s.overflowX = s.overflowY = 'scroll';

    document.body.appendChild(div);

    var result = {
      width: 100 - div.clientWidth,
      height: 100 - div.clientHeight
    };

    document.body.removeChild(div);

    return result;
  };

  /**
   * 画面が横向きか
   * @returns {boolean}
   */
  GuiParts.isLandscape = function () {
    return !!window.matchMedia('screen and (orientation:landscape)').matches;
  };

  /**
   * アクティブなページモジュールを返す
   * @returns {*}
   */
  GuiParts.getActivePageModule = function () {
    for (var p in global.Pages) {
      var page = global.Pages[p];
      if (page.$page && page.$page.hasClass('ui-page-active')) {
        return page;
      }
    }
  };

  /**
   * 指定のIDのページモジュールを返す
   * @returns {*}
   */
  GuiParts.getPageModule = function (pageId) {
    for (var p in global.Pages) {
      var page = global.Pages[p];
      if (page.$page && page.$page.attr('id') == pageId) {
        return page;
      }
    }
  };

  //---

  /**
   * 各View上のお気に入りアイテムを更新する
   * @param circleId
   * @param colorId
   */
  GuiParts.updateFavItem = function (fav) {
    global.Pages.MapView.updateFavItem(fav);
    global.Pages.CutView.updateFavItem(fav);
    global.Pages.SearchView.updateFavItem(fav);
  };

  //---

  /**
   *
   */
  function initMapViewPage () {

    DjCRDView.Pages.MapView.init();

    $('section[data-role="page"]').each(function () {
      var $page = $(this);
      var pageId = $page.attr('id');

      var pageModule = GuiParts.getPageModule(pageId);

      var $main = $page.children('[role="main"]')
        .attr('id', pageId.replace(/Page/,'Main'));

      var $header = GuiParts.PageHeader.add($page,pageId);
      var $footer = GuiParts.PageFooter.add($page,pageId);
      var desc = (($page)[0].getElementsByClassName('dj-detail-container') || [])[0];
      if (desc) {
        GuiParts.DescBar.addDescBar(desc);
      }

      function resizeMainHeight () {
        var s = $.mobile.getScreenHeight();
        var h = $header.outerHeight();
        var f = $footer && $footer.outerHeight() || 0;
        var p = global.Utils.pxToInt($main.css('padding-top'))+global.Utils.pxToInt($main.css('padding-bottom'));
        var height = (s-h-f-p);
        $main.css('height',height+'px');
      }

      // ページ表示時にコンテンツ表示領域を画面サイズに収める
      $page.on('pageshow', function () {
        resizeMainHeight();
      });

      // 画面リサイズ時にコンテンツ表示領域を画面サイズに収める
      var timerid;
      $(window).resize(function () {
          if (timerid) {
            clearTimeout(timerid);
          }
          timerid = setTimeout(function () {
            if ($page.hasClass('ui-page-active')) {

              console.debug('[EVENT] resized window');

              resizeMainHeight();

              if (pageModule && pageModule.resizedWindowHandler) {
                pageModule.resizedWindowHandler();
              }
            }

          },200);
        }
      );
    });
  }

  /**
   * 共通イベントの初期化
   */
  function initCommonEvents () {
    if (global.Env.useKeydown) {

      // キーダウン
      $(window).on('keydown', function (e) {
        var dis = [
          '.ui-panel-open',   // サイドパネル
          '.ui-popup-active', // ポップアップ
          '.ui-focus'         // テキスト入力
        ].some(function (e) {return !!document.querySelector(e);});
        if (dis) {
          return;
        }

        for (var p in global.Pages) {
          var page = global.Pages[p];
          if (page.$page && page.$page.hasClass('ui-page-active') && page.keydownHandler) {
            page.keydownHandler(e);
          }
        }
      });

      // クリップボードペースト
      $(window).on('paste', function (e) {
        if (!!$("#dynDialog")[0]) {
          return;
        }

        for (var p in global.Pages) {
          var page = global.Pages[p];
          if (page.$page && page.$page.hasClass('ui-page-active') && page.pasteHandler) {
            page.pasteHandler(e);
          }
        }
      });
    }
  }

  /**
   * UIの初期化
   */
  GuiParts.init = function () {

    GuiParts.MenuPanel.initPanel();
    GuiParts.DataInputPanel.initPanel();
    GuiParts.DataOutputPanel.initPanel();

    // 配置図ビューの初期化
    initMapViewPage();

    // 他のビューの初期化
    DjCRDView.Pages.RomSelect.initView();
    DjCRDView.Pages.FavListSelect.initView();
    DjCRDView.Pages.HistoryTool.initView();

    // 共通イベントの初期化
    initCommonEvents();

  };

  //---

  //global["GuiParts"] = GuiParts;

})(DjCRDView);
