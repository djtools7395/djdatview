// TODO ページごとにインスタンス化したい

(function (global) {

  var DescBar = {};

  //---

  /**
   * サークル詳細バーの追加
   * @param page
   */
  DescBar.addDescBar = function (page) {

    var $container = $(page).attr({
      'data-role': 'content'
    });

    var basename = (
      Array.prototype.slice.call(page.classList)
        .map(function (c) {
          return (/^dj-([a-z]+?)view-detail-container$/.exec(c) || [])[1]
        })
        .filter(function (c) {
          return c
        })
    )[0];

    if (!basename)
      return;

    // カット
    var $cutcontainer = $('<div>').attr({
      'id': basename + 'DetailCutContainer'
    }).addClass('dj-detail-cut-container').appendTo($container);

    var $cutcanvascontainer = $('<div>').appendTo($cutcontainer);

    var $cutcanvas = $('<canvas>').attr({
      'id': basename + 'DetailCut'
    }).addClass('dj-detail-cut-canvas').appendTo($cutcanvascontainer);

    $cutcontainer.on('click', function () {
      global.GuiParts.PubsEditPanle.openNavPanel();
    });

    // サークル名、作家名ほか
    var $ccc = $('<div>').addClass('dj-detail-ccc-container').appendTo($container);

    var $desccontainer = $('<div>').addClass('dj-detail-desc-container').appendTo($ccc);

    var $namecontainer = $('<div>').addClass('dj-detail-name-container').appendTo($desccontainer);
    var $spcont = $('<div>').addClass('dj-detail-space-container').appendTo($namecontainer);

    $('<div>').attr('id', basename + 'DetailSpace').addClass('dj-detail-space').appendTo($spcont);
    $('<div>').attr('id', basename + 'DetailCond').addClass('dj-detail-condition').appendTo($spcont);
    $('<div>').attr('id', basename + 'DetailGenre').addClass('dj-detail-genre').appendTo($namecontainer);
    $('<div>').attr('id', basename + 'DetailAuthor').addClass('dj-detail-author').appendTo($namecontainer);
    $('<div>').attr('id', basename + 'DetailName').addClass('dj-detail-name').appendTo($namecontainer);

    var $textcont = $('<div>').addClass('dj-detail-text-container').appendTo($desccontainer);
    $('<div>').attr('id', basename + 'DetailText').addClass('dj-detail-text').appendTo($textcont);

    // リンク
    var $linkcontainer = $('<div>').addClass('dj-detail-link-container').appendTo($ccc);
    for (var i = 0; i < global.Env.detailLinkColumns; i++) {
      var id = basename + 'DetailLink-' + i;
      var $btn = $('<div>').attr({
        'id': id,
        'data-link-id': i,
        'data-circle-url': ''
      }).addClass('dj-detail-link ui-corner-all ui-shadow').on('tap', function () {
        var $div = $(this);
        var id = $div.attr('data-link-id');
        var link = $div.attr('data-circle-url');
        if (link && id !== 'END') {
          open(link, '_blank')
        }
      });

      if (i + 1 == global.Env.detailLinkColumns) {
        $btn.addClass('dj-detail-link-lastchild')
      }

      $btn.appendTo($linkcontainer);
    }

    // メモ、発行物
    var $memocontainer = $('<div>').attr({
      id: basename + 'MemoContainer'
    }).addClass('dj-detail-memo-container').appendTo($container);

    (function ($container) {
      $container.on('click', function () {
        var $c = $(document.getElementById(basename + 'DetailContainer'));
        var circleId = $c.attr('data-circle-id');
        var name = $c.attr('data-circle-name');
        var space = $c.attr('data-circle-space');
        if (name) {
          $('#favColorSelectTitle').text(space+' '+name);

          $('#favColorSelectPanel').attr({
            'data-circle-id': circleId
          }).panel('open');
        }
      });
    })($memocontainer);

    /*
    (function ($container) {
      $container.on('click', function () {
        var $c = $(document.getElementById(basename + 'DetailContainer'));
        var circleId = $c.attr('data-circle-id');
        var name = $c.attr('data-circle-name');
        var space = $c.attr('data-circle-space');
        if (name) {
          $('#favColorSelectTitle').text(space+' '+name);

          $('#favColorSelectPanel').attr({
            'data-circle-id': circleId
          }).panel('open');
        }
      });
    })($cutcontainer);
    */

    $container.trigger('create');
  };

  //

  /**
   * サークル詳細バーを空にする
   */
  DescBar.clearDescBar = function () {
    $(document.getElementById('mapDetailSpace')).text('');
    $(document.getElementById('mapDetailName')).text('');
    $(document.getElementById('mapDetailAuthor')).text('');
    $(document.getElementById('mapDetailGenre')).text('');
    $(document.getElementById('mapDetailCond')).text('');
    $(document.getElementById('mapDetailText')).text('');

    // TODO
    $(document.getElementById('mapMemoContainer')).text('');

    for (var i=0; i < global.Env.detailLinkColumns; i++) {
      var $box = $(document.getElementById('mapDetailLink-' + i)).attr('data-circle-url', '');
      Array.prototype.slice.call($box[0].classList)
        .filter(function (c) {
          return /^dj-detail-link-icon-/.test(c) || 'dj-detail-link-enabled' == c;
        })
        .forEach(function (c) {
          $box.removeClass(c);
        });
    }

    $(document.getElementById('mapDetailCutContainer')).css({
      'background-color': ''
    });

    var canvas = document.getElementById('mapDetailCut');
    global.GuiParts.drawImageToCanvas(canvas, null);
  };

  /**
   * サークル詳細バーを更新する
   * @param circleId
   */
  DescBar.updateDescBar = function (circleId) {

    return Promise.resolve()
      .then(function () {

        return global.DataOp.FavCircle.getFavCircle(circleId);

      })
      .then(function (fav) {

        var Circle = fav.Circle;

        // 基本情報
        var circleName = global.Utils.z2h(Circle.name);
        var penName = global.Utils.z2h(Circle.author);
        var block = global.DataOp.EventList.Selected.Block[Circle.blockId];
        var spaceNo = global.Utils.getSpaceNoStr(Circle, block.noab);
        var spaceLabel = block.name + spaceNo;
        var genre = global.DataOp.EventList.Selected.Genre[Circle.genreId].name;
        var text = global.Utils.z2h(Circle.description || '');

        // カットコンテナタップ時に引き継ぐ情報
        var $container = $(document.getElementById('mapDetailContainer'));
        $container.attr({
          'data-circle-id': circleId,
          'data-circle-name': circleName,
          'data-circle-space': spaceLabel
        });

        // テキスト
        $(document.getElementById('mapDetailSpace')).text(spaceLabel);
        $(document.getElementById('mapDetailName')).text(circleName || '-');
        $(document.getElementById('mapDetailAuthor')).text(penName || '-');
        $(document.getElementById('mapDetailGenre')).text(genre || '-');
        $(document.getElementById('mapDetailText')).text(text);

        var pubs = (Circle.Pubs || []).map(function (p) {
          var m = (function (v) {
            if (v == '2')
              return '限';
            return '';
          })(p.method);

          var c = (function (v) {
            if (v == '6')
              return '委';
            if (v == '2')
              return '既';
            if (v == '1')
              return '新';
            return '他';
          })(p.class);

          var t = (function (v) {
            if (v == '2')
              return '折';
            if (v == '3')
              return 'コ';
            if (v == '4')
              return 'ペ';
            if (v == '8')
              return 'CG';
            return '';
          })(p.type);

          return m+c+t;
        });

        var $memo = $(document.getElementById('mapMemoContainer'));

        // TODO
        $memo.text(pubs || '');

        // URLボタン
        DescBar.updateDescLinks(fav.Fav.urls);

        // お気に入り色や状況
        DescBar.updateDescItem(fav);

        // カットの描画は待ち合せない
        DescBar.updateDescCut(circleId);

      })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      });
  };

  /**
   * サークル詳細バーを更新する（URLボタンのみ）
   * @param urls
   */
  DescBar.updateDescLinks = function (urls) {

    // URLボタンのリセット
    for (var i = 0; i < global.Env.detailLinkColumns; i++) {
      var $box = $(document.getElementById('mapDetailLink-' + i)).attr('data-circle-url', '');
      Array.prototype.slice.call($box[0].classList)
        .filter(function (c) {
          return /^dj-detail-link-icon-/.test(c) || 'dj-detail-link-enabled' == c;
        })
        .forEach(function (c) {
          $box.removeClass(c);
        });
    }

    // URLの貼り付け
    var linkExprs = new global.Utils.linkExrps();
    urls.forEach(function (link) {
      if (!link) {
        return;
      }

      var f = linkExprs.some(function (e) {
        if (e.expr && e.expr.test(link)) {
          // 特定のサイトへのリンク
          e.link = link;
          return true;
        }
      });
      if (!f) {
        // その他のリンク
        linkExprs.push({link: link});
      }
    });

    linkExprs.filter(function (e) {
      return e.link
    }).forEach(function (e, i) {
      if (i < global.Env.detailLinkColumns) {
        var $btn = $(document.getElementById('mapDetailLink-' + i)).attr('data-circle-url', e.link).addClass('dj-detail-link-enabled');
        if (e.icon) {
          $btn.addClass('dj-detail-link-icon-' + e.icon);
        }
      }
    });
  };

  /**
   * サークル詳細バーを更新する（お気に入り＆状況のみ）
   * @param fav
   */
  DescBar.updateDescItem = function (fav) {

    var circleId = fav.Circle.circleId;
    var $container = $(document.getElementById('mapDetailContainer'));
    if (circleId != $container.attr('data-circle-id')) {
      return;
    }

    var cond = global.DataOp.Condition.findCondition(fav.Cond.condId);
    var condText = cond ? cond.shortlabel : '－';
    $(document.getElementById('mapDetailCond')).text(condText);

    var chk = global.DataOp.FavColor.findFavColor(fav.Fav.colorId);
    $(document.getElementById('mapDetailCutContainer')).css({
      'background-color': chk ? '#'+chk.checkColor : ''
    });
  };

  /**
   * サークル詳細バーを更新する（カットのみ）
   * @param circleId
   */
  DescBar.updateDescCut = function (circleId) {

    global.DataOp.CutImage.getCutImage(circleId).then(function (image) {
      var canvas = document.getElementById('mapDetailCut');
      if (canvas) {
        global.GuiParts.drawImageToCanvas(canvas, image, global.DataOp.EventList.Selected.Event.cutSizeW*2, global.DataOp.EventList.Selected.Event.cutSizeH*2);
      }
    }).catch(function (e) {
      if (e)
        console.error(e.stack || e);
    });
  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.DescBar = DescBar;

})(DjCRDView);