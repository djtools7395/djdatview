(function (global) {

  var ViewMode = {};

  const TARGET_ELEMENT_IDS = [
    '#mapViewMain',
    '#mapMainContainer',
    '#mapMapContainer',
    '#mapCanvasContainer',
    '#mapDetailContainer',
    '#mapSidebarContainer',
    '#mapSidebarContainer .dj-mapview-sidebar-padding',
    '#mapFavsList',
    '#cutGridContainer',
    '#searchContainer'
  ];

  const VIEW_MODE = {
    'fav': 'dj-mapview-mode-favview',
    'cut': 'dj-mapview-mode-cutview',
    'search': 'dj-mapview-mode-searchview'
  };

  var targetElements;

  /**
   * カットビューモードの表示中か？
   * @returns {boolean}
   */
  ViewMode.isCutViewMode = function () {
    return global.Env.selectedViewId == 'cut';
  };

  /**
   * MAP/CUT/FAV/SEARCHビューモードを切り替える他
   */
  ViewMode.setViewMode = function (btnId) {

    //
    var activePage = global.GuiParts.getActivePageModule();
    if (!activePage) {
      return;
    }

    // FIXME ロード時に selectedViewId へのジャンプを実装したら targetElements を見るのは止める
    // 現在のビューモードと同じボタンを押したらナビパネルを開く
    if (/ViewPage$/.test(activePage.$page.attr('id'))) {
      if ((targetElements || !targetElements && 'map' == btnId) && global.Env.selectedViewId == btnId) {
        $('#' + btnId + 'NavPanel').panel('open');
        return;
      }
    }

    global.DataOp.EnvOp.save({
      'selectedViewId': btnId
    });

    if (!targetElements) {
      // FIXME
      targetElements = TARGET_ELEMENT_IDS.map(function (id) {return $(id)});
    }

    var mode = VIEW_MODE[btnId];

    targetElements.forEach(function ($sel) {
      Object.keys(VIEW_MODE).forEach(function (key) {
        $sel.removeClass(VIEW_MODE[key]);
      });
    });

    if (mode) {
      targetElements.forEach(function ($sel) {
        $sel.addClass(mode);
      });
    }
  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.ViewMode = ViewMode;

})(DjCRDView);
