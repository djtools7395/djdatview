(function (global) {

  var FavConditionSelectPanel = {};

  //---

  function tapConditionHandler () {
    var circleId = global.Env.selectedCircleId;
    if (!circleId) {
      console.error(new Error('circle not selected'));
      return;
    }

    // TODO お気に入りに入っていなくても状況更新ができるようにする
    var fav = global.DataOp.FavCircle.findFavCircle(circleId);
    if (!fav) {
      console.log('not in favlist: '+circleId);
      return;
    }

    var newCondId = $(this).attr('data-condition-id');
    global.DataOp.FavCondition.setFavCondition(circleId, newCondId).then(function (obj) {
      fav.Cond.condId = obj ? obj.condId : 1;
      global.GuiParts.updateFavItem(fav);
    }).catch(function (e) {
      if (e)
        console.error(e.stack || e);
    });
  }

  /**
   *
   * @returns {*|jQuery}
   */
  FavConditionSelectPanel.init = function () {

    $listview = $('#favEditPanelList').empty();

    // ヘッダ（サークル名）

    var $title = $('<a>').attr({
      'href': '#',
      'data-rel': 'close',
      'id': 'favEditTitle'
    }).addClass('dj-favedit-title').text('Edit');

    $('<li>').attr({
      'data-icon': false,
      'data-theme': 'a'
    }).append($title).appendTo($listview);

    $('<li>').attr({
      'data-role': 'list-divider'
    }).text('状況更新').appendTo($listview);

    // 選択可能な状況一覧

    global.DataOp.Condition.getSortedKeys().forEach(function (condId,index) {
      var c = global.DataOp.Condition.findCondition(condId);
      var $link = $('<a>').attr({
        'href': '#',
        'data-rel': 'close',
        'data-condition-id': index+1
      }).text(c.label);

      $('<li>').attr({
        'data-icon': false
      }).append($link).appendTo($listview);

      $link.on('tap', tapConditionHandler);
    });

    // 一個戻る

    $('<li>').attr({
      'data-role': 'list-divider'
    }).text('一個戻す').appendTo($listview);

    var $backto = $('<a>').attr({
      'href': '#',
      'data-rel': 'close',
      'data-condition-id': -1
    }).addClass('dj-panel-button').text('状況を一つ前に戻す');

    $('<li>').attr({
      'data-icon': false
    }).append($backto).appendTo($listview);

    $backto.on('tap', tapConditionHandler);

    //

    $listview.listview().listview('refresh');

    return $('#favEditPanel').attr('data-theme', 'a').panel().trigger('create');
  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.FavConditionSelectPanel = FavConditionSelectPanel;

})(DjCRDView);
