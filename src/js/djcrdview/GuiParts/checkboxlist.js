//------------------
//
// チェックボックス付きリスト
//
//------------------

(function (global) {

  var CheckboxList = {
    RADIO: 'radio',
    CHECKBOX: 'checkbox'
  };

  //---

  const LIST_ITEM_TEMPLATE = `
<label for="#BASENAME#-#ID#" id="#BASENAME#-Remove-#ID#" data-item-index="#INDEX#"></label>
<input type="#TYPE#" name="#BASENAME#" id="#BASENAME#-#ID#" value="#ID#">
`;

  /**
   *
   * @param $container
   * @param basename
   * @param type
   * @param values
   * @returns {*}
   */
  CheckboxList.setListItems = function ($container, basename, type, values) {

    $container.empty();

    var $form = $('<form>').addClass('ui-field-contain').appendTo($container);
    var $body = $('<div>').attr('data-role','controlgroup').appendTo($form);

    var innerHTML = '';
    values.forEach(function (v, index) {
      innerHTML += LIST_ITEM_TEMPLATE
        .replace(/#TYPE#/g ,type)
        .replace(/#BASENAME#/g ,basename)
        .replace(/#ID#/g ,v.id)
        .replace(/#INDEX#/g ,index);
    });

    $(innerHTML).appendTo($body);

    $body.children('label').each(function () {
      var $box = $(this);
      var index = $box.attr('data-item-index');
      var v = values[index];

      $box.text(v.name);
    });

    return $form.trigger('create');
  };

  //---

  global.GuiParts = global.GuiParts || {};

  global.GuiParts.CheckboxList = CheckboxList;

})(DjCRDView);
