
(function (global) {

  var SandboxDB = {};

  var db;

  //---

  /**
   * 初期化
   */
  SandboxDB.init = function (defs) {

    return new Promise(function (resolve, reject) {
      if (!openDatabase) {
        return reject(new Error('WebSQL非対応のブラウザでは動きません'));
      }

      db = openDatabase(defs.shortName, defs.version, defs.displayName, defs.maxSize);
      if (!db) {
        return Promise.reject(new Error('DBの初期化に失敗しました'));
      }

      return resolve();
    });
  };

  //---

  /**
   * トランザクションの開始
   * @returns {Promise}
   */
  SandboxDB.beginTransaction = function (initValue) {
    return new Promise(function (resolve, reject) {
      db.transaction(function (t) {
        resolve({transaction:t, result:initValue || {}});
      });
    });
  };

  /**
   * queryを実行する（１トランザクション中に複数回実行可能）
   * @param t
   * @param query
   * @returns {Promise}
   */
  SandboxDB.executeQuery = function (t, query) {
    return new Promise(function (resolve,reject) {
      t.executeSql(
        query.sql,
        query.params,
        function (t,r) {
          if (!query.onSuccess) {
            return resolve({transaction: t, result: r});
          }

          var obj = query.onSuccess(r);
          if (obj !== undefined) {
            // obj === nullの場合もここ
            return resolve({transaction: t, result: obj});
          }

          reject();
        },
        function (t,e) {
          reject(query.onError ? query.onError(e) : e);
        }
      );
    });
  };

  /**
   * queryを実行する（１トランザクション中に１クエリーを実行して終了）
   * @param query
   * @returns {Promise}
   */
  SandboxDB.transactionQuery = function (query) {
    return SandboxDB.beginTransaction()
      .then(function (res) {
        return SandboxDB.executeQuery(res.transaction, query)
      })
      .then(function (res) {
        return Promise.resolve(res.result);
      });
  };

  //---

  global.SandboxDB = SandboxDB;

})(this);
