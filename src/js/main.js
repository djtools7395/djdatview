// FIXME イベント切り替え時のキャッシュのクリア
// FIXME イベント切り替え完了までモーダレスダイアログを表示する

(function (global) {

  var DjCRDView = {};

  //---

  /**
   * 各種キャッシュのクリア（イベント一覧は別枠）
   * @returns {*}
   */
  DjCRDView.clearCacheAll = function () {
    return new Promise(function (resolve,reject) {
      // FIXME これいらない
      var event = DjCRDView.DataOp.EventList.findEvent(DjCRDView.Env.selectedEventId);
      if (!event) {
        console.error(new Error('not found event cache eventId='+DjCRDView.Env.selectedEventId));
        return reject();
      }

      return resolve();
    })
      .then(function () {
        return DjCRDView.DataOp.Layout.clearCache();
      })
      .then(function () {
        return DjCRDView.DataOp.Condition.clearCache();
      })
      .then(function () {
        return DjCRDView.DataOp.FavColor.clearCache();
      })
      .then(function () {
        return DjCRDView.DataOp.Circle.clearCache();
      })
      .then(function () {
        return DjCRDView.DataOp.CutImage.clearCache();
      })
      .then(function () {
        return DjCRDView.DataOp.MapImage.clearCache();
      })
      .then(function () {
        return DjCRDView.DataOp.FavCondition.clearCache();
      })
      .then(function () {
        return DjCRDView.DataOp.FavCircle.clearCache();
      })
      .catch(function (e) {
        if (e) {
          return Promise.reject(e);
        }
      });
  };

  /**
   * 初期化
   */
  DjCRDView.init = function () {

    // ローカル(file://)で動かすときはセキュリティ保護のために動作しないのと、zip処理は終了を待ち合わせるので、web worker は使わない
    zip.useWebWorkers = false;
    //zip.workerScriptsPath = 'js/vendor/zip/';

    $.holdReady(true);

    return Promise.resolve()
      .then(function () {
        return SandboxFS.open(DjCRDView.Const.FsQuota)
          .then(function () {
            return SandboxFS.getQuotaInfo();
          })
          .then(function (info) {
            console.log('fs opened: quota='+info.quota+' , usage='+info.usage);
          });
      })
      .then(function () {
        return SandboxDB.init(DjCRDView.DataOp.Database.DEFS)
          .then(function () {
            return DjCRDView.DataOp.Database.createTables();
          })
          .then(function () {
            console.log('db opened');
          });
      })
      .then(function () {
        return DjCRDView.DataOp.EnvOp.init();
      })
      .then(function () {
        // イベント一覧は別枠
        return DjCRDView.DataOp.EventList.clearCache();
      })
      .then(function () {
        // ここらへんのために holdReady してる
        DjCRDView.GuiParts.init();
      })
      .then(function () {
        DjCRDView.Pages.RomSelect.setRomSelected(DjCRDView.Env.selectedEventId);
      })
      .catch(function (e) {
        if (e)
          console.error(e.stack || e);
      })
      .then(function () {
        // finally
        $.holdReady(false);
      });

  };

  //---

  global.DjCRDView = DjCRDView;

})(this);
