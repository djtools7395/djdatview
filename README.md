# DjdatView #

### What is this repository for? ###

* DVD-ROMカタログ形式の同人誌即売会のサークルリスト＆配置図データをブラウザで閲覧するためのHTML+JavaScriptのコード群です。

### How do I get set up? ###

* index.htmlを開けば起動します。Webサーバ上に置いてもローカルで開いても構いません。
* WebSQLを使っているのでChrome for PC/Androidでしか実行できません。OperaやSafariで動くかどうかは確認していません。

### Contribution guidelines ###

* 開発環境はWebStormです。

### License ###

* このプロダクトはMITライセンスのもとで公開しています。

### Who do I talk to? ###

* お話は [課題](https://bitbucket.org/djtools7395/djdatview/issues) で伺います。
